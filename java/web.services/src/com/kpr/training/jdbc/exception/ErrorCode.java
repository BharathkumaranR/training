/*
Entity:
  1.ErrorCode

Jobs to be done:

1.Create enumurator ErrorCode
2.Create enum constructors with error code and corresponding error message
3.Map error code and message and store it in ERROR_CODE_MESSAGES
4.Create getErrorMessage method to get/return message for specified error code.

*/

package com.kpr.training.jdbc.exception;

public enum ErrorCode {
	
//  Address Failed Exception
	
    ERR01("Failed to create address"),
    ERR02("Failed to read address"),
    ERR03("Failed to read all addresses"),
    ERR04("Failed to update address"),
    ERR05("Failed to delete address"),
//  Person Failed Exception  
    ERR06("Failed to create person "),
    ERR07("Failed to read person "),
    ERR08("Failed to read all persons "),
    ERR09("Failed to update person "),
    ERR10("Failed to delete person "),
//  AddressValidaton Exception
    ERR11("Postal code should not be empty"),
    ERR12("Failed to Search Address Present"),
    ERR13("Failed to delete Unused Address"),
//  PersonValidaton Exception
    ERR14("Duplicate Person's email address found"),
    ERR15("Failed to check unique name"),
	ERR16("Duplicate Person's name found"),
	ERR17("Person's name should not be null or empty"),
	ERR18("Person's birthDate should not be null "),
	ERR19("Email does not exist"),
	ERR20("Failed to Get Email id"),
//	Cascade Operations Exception
	ERR21("Failed to create Person with Address"),
	ERR22("Failed to store Read Person with Address"),
	ERR23("Failed to store Read All Person with Address"),
	ERR24("Failed to store delete Person with Address"),
//	Connection Exception
	ERR25("Failed to Get Connection"),
	ERR26("Failed to SetAutoCommit false Connection"),
	ERR27("Failed to Close Connection"),
	ERR28("Failed to Commit Connection"),
	ERR29("Failed to Rollback Connection"),
	
//	CSV file Person Creation Exception
	ERR31("Failed to get file"),
	ERR32("Failed to Get Exist Address id"),
	ERR33("Date Formate is dd-MM-yyyy"),
	
//	No Of Rows
	ERR34("Required No of Rows Not Correctly Changed"),
	ERR35("Email not be Valid"),
	ERR36("Failed to delete not used address"),
	ERR37("Search address should contain value");
	
	String message;

	ErrorCode(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return this.message;
	}
}
