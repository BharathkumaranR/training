/*
Requirements:
    - AppException class
    
Entities:
    - AppException
    
Function Declaration:
    - public AppException()
    - public AppException(ErrorCode error, String stackTrace)
    - public AppException(ErrorCode error, Exception e)
    
Job to be done:
    - Create a class ApException extends RuntimeException
    - Declare a constructor with empty parameters and invoke super() method
    
    - Declare another constructor with parameter - ErrorCode error and String stackTrace
    - Get the detail of the error with getDetail() method of enum class
    - Invoke super() method to invoke the properties of Exception
    
    - Declare another constructor with parameter - ErrorCode error and Exceptioon e
    - Get the detail of the error with getDetail() method of enum class
    - Invoke super() method to invoke the properties of Exception
    
Pseudocode:
public class AppException extends RuntimeException {
	
	public AppException() {
		super();
	}

    public AppException(ErrorCode error, String stackTrace) {
    	super(stackTrace);
    	System.out.println(error.getDetail());
	}

	public AppException(ErrorCode error, Exception e) {
		super(error.getDetail(), e);
		e.printStackTrace();
	}
}
 
*/


package com.kpr.training.jdbc.exception;

public class AppException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public AppException(ErrorCode error, Exception e) {
		super(error.getMessage(), e);
	}
	
	public AppException(ErrorCode error) {
		super(error.getMessage());
	}
}
