sspackage com.kpr.training.jdbc.servlet;

import com.google.gson.Gson;
import com.kpr.training.jdbc.model.Person;

public class JsonUtil {
	  
	  public Person deSerialize(String personJson) {
	    
	    Gson gson = new Gson();
	    
	    Person person = gson.fromJson(personJson, Person.class);
	    
	    return person;
	  }
	  
	  public String serialize(Person personGot) {
	    
	    Gson gson = new Gson();
	    
	    String json = gson.toJson(personGot, Person.class);
	    
	    return json;
	  }
	  
	  public static void main(String[] args) {
	    
	    String personJson = "{ 'firstName' : 'Santheesh',"
	              + " 'lastName' : 'A',"
	              + " 'email' : 'santheesh62@gmail.com',";
	    
	    JsonUtil jsonUtil = new JsonUtil();
	    
	    Person personGot = jsonUtil.deSerialize(personJson);
	    
	    System.out.println(personGot.getFirstName());
	    System.out.println(personGot.getLastName());
	    System.out.println(personGot.getEmail());
	    
	    
	    String personString = jsonUtil.serialize(personGot);
	    
	    System.out.println(personString);
	     
	  }
	  
}
