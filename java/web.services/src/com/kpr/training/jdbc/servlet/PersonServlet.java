package com.kpr.training.jdbc.servlet;

import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kpr.training.jdbc.service.PersonService;

public class PersonServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest req, HttpServletResponse res) {

		try {
		      JsonUtil jsonUtil = new JsonUtil();
		      PersonService personService = new PersonService();
		      
		      String personJson = req.getParameter("person");
		      
		      long personId = personService.create(jsonUtil.deSerialize(personJson));
		      
		      PrintWriter writer = res.getWriter();
		      
		      writer.append("Person Creation Success : " + personId);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}

}
