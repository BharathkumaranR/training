package com.kpr.training.jdbc.service;

import java.io.InputStream;
import java.sql.Connection;
import java.util.Properties;

import javax.sql.DataSource;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class ConnectionService {

	private static ThreadLocal<Connection> threadLocal = new ThreadLocal<>();
	private static HikariConfig config = new HikariConfig();
	private static HikariDataSource datasource;
	
	public static HikariDataSource getDataSource() 
	{
		try {
			Properties db = new Properties();
			InputStream resourceAsStream = ConnectionService.class.getClassLoader()
					.getResourceAsStream("db.properties");
			db.load(resourceAsStream);

			config.setMaximumPoolSize(10);
			config.setDriverClassName(db.getProperty("DB_DRIVER_CLASS"));
			config.setJdbcUrl(db.getProperty("URL"));
			config.setUsername(db.getProperty("USERNAME"));
			config.setPassword(db.getProperty("PASSWORD"));
			config.setAutoCommit(false);
			datasource = new HikariDataSource(config);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return datasource;

	}

	public static void init() throws Exception {
		threadLocal.set(getDataSource().getConnection());
	}

	public static Connection get() throws Exception {
		if (threadLocal.get() == null) {
			init();
		}
		return threadLocal.get();
	}

	public static void release() {
		try {
			threadLocal.get().close();
			threadLocal.remove();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void commitRollback(boolean flag) throws Exception {

		try {
			if (flag) {
				threadLocal.get().commit();
			}
			threadLocal.get().rollback();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
}
