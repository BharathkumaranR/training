/*
 Problem Statement
    1.Perform CRUD Operations for Address Service
  
1.Requirement
   - Perform CRUD Operations for Address Service
  
2.Entity
    1.Address
    2.AddressService
    3.AppException
    4.ErrorCode
    5.ConnectionService
*
*/



package com.kpr.training.jdbc.service;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.kpr.training.jdbc.constant.Constant;
import com.kpr.training.jdbc.constant.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;
import com.kpr.training.jdbc.validation.AddressValidator;

public class AddressService {

	Address address;
	Person person;
	long addressId = 0;

	public long checkAddressOrCreate(Address address) {
		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.GET_ADDRESS_ID_QUERY)) {

			setPs(ps, address);

			ResultSet resultSet = ps.executeQuery();
			if (resultSet.next()) {
				
				return resultSet.getLong(Constant.ID);
			}
			return create(address);

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR32, e);
		}
	}

	public long create(Address address) {

		AddressValidator.isPostalCodeEmpty(address);

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.CREATE_ADDRESS_QUERY,
						Statement.RETURN_GENERATED_KEYS)) {

			setPs(ps, address);
			ResultSet resultSet;

			if (ps.executeUpdate() == 1 && (resultSet = ps.getGeneratedKeys()).next()) {
				return resultSet.getLong(Constant.GENERATED_ID);
			}
			return 0;

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR01, e);
		}
	}

	public Address read(long id) {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.READ_ADDRESS_QUERY)) {

			ps.setLong(1, id);
			ResultSet resultSet = ps.executeQuery();

			if (!resultSet.next()) {
				return null;
			}
			return getRs(resultSet);

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR02, e);
		}

	}

	public ArrayList<Address> readAll() {

		ArrayList<Address> addresses = new ArrayList<Address>();
		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.READ_ALL_ADDRESS_QUERY)) {

			ResultSet resultSet = ps.executeQuery();

			while (resultSet.next()) {
				addresses.add(getRs(resultSet));
			}
			return addresses;

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR03, e);
		}

	}

	public void update(Address address, long id) {

		AddressValidator.isPostalCodeEmpty(address);
		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.UPDATE_ADDRESS_QUERY)) {

			setPs(ps, address);
			ps.setLong(4, id);

			if (ps.executeUpdate() != 1) {
				throw new AppException(ErrorCode.ERR34);
			}

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR04, e);
		}
	}

	public void delete(long id) {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.DELETE_ADDRESS_QUERY)) {
			ps.setLong(1, id);

			if (ps.executeUpdate() != 1) {
				throw new AppException(ErrorCode.ERR34);
			}

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR05, e);
		}
	}

	public void deleteNotUsedAddress() {

		try (PreparedStatement ps = ConnectionService.get()
				.prepareStatement(QueryStatement.DELETE_NOT_USED_ADDRESS_QUERY)) {

			ResultSet resultSet = ps.executeQuery();

			if (resultSet.next()) {
				delete(resultSet.getLong(Constant.ID));
			}
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR36, e);
		}
	}

	public List<Address> search(Address address) {

		List<Address> addresses = new ArrayList<>();

		if (address.getStreet() == null && address.getCity() == null && address.getPostalCode() == 0) {
			throw new AppException(ErrorCode.ERR37);
		}

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.ADDRESS_SEARCH_QUERY)) {

			setPs(ps, address);
			ResultSet resultSet = ps.executeQuery();

			if (address.getStreet() != null || address.getCity() != null || address.getPostalCode() != 0) {
				while (resultSet.next()) {
					addresses.add(getRs(resultSet));
				}
			}
			return addresses;
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR12);
		}
	}

	public Address getRs(ResultSet resultSet) throws SQLException {
		return new Address(resultSet.getLong(Constant.ID), resultSet.getString(Constant.STREET),
				resultSet.getString(Constant.CITY), resultSet.getInt(Constant.POSTAL_CODE));
	}

	public void setPs(PreparedStatement ps, Address address) throws SQLException {
		ps.setString(1, address.getStreet());
		ps.setString(2, address.getCity());
		ps.setInt(3, address.getPostalCode());
	}

	public static void main(String[] args) throws Exception {
		AddressService addressService = new AddressService();
		Connection connection = ConnectionService.get(); 
		long id = addressService.create(new Address("T Nagar", "Chennai", 600028));
		System.out.println(id);
		
		if(id > 0) {
			connection.commit();
		} else {
			connection.rollback();
		}
	}
	
}	