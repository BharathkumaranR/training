package com.kpr.training.jdbc.validation;

import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.Address;

public class AddressValidator {
	public static void isPostalCodeEmpty(Address address) throws AppException {
		
		if (address.getPostalCode() == 0) {
			throw new AppException(ErrorCode.ERR11);
		}
	}
}
