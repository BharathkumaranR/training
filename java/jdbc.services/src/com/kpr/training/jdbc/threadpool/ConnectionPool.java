package com.kpr.training.jdbc.threadpool;


import java.sql.Connection;

import com.kpr.training.jdbc.connection.ConnectionService;


public class ConnectionPool extends ThreadLocal<Connection>{
	
	ConnectionService service = new ConnectionService();
	
	public Connection initialValue() {
		return service.init();
	}

	public void remove(Connection connection) {
		super.remove();
		try {
			service.release(connection);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void get(Connection connection) {
		try {
			service.get(connection);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
