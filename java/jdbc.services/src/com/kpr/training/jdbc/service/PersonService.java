/*
 *
 Problem Statement
    1.Perform CRUD Operations for Person Service
  
1.Requirement
   - Perform CRUD Operations for Person Service
  
2.Entity
    1.Person
    2.PersonService
    3.AppException
    4.ErrorCode
    
3.Method Declaration
    1.public static long insertPerson(Person person)
    2.public void CsvPersonInsertion()
    3.public static Address readPerson(long id) 
    4.public List<Address> readAllPerson()
    5.public int updatePerson(Person person, long id)
    6.public Boolean deletePerson(long id)
  

*@author SA
*
*/


package com.kpr.training.jdbc.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.kpr.training.jdbc.constant.Constant;
import com.kpr.training.jdbc.constant.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;
import com.kpr.training.jdbc.validation.PersonValidator;;


public class PersonService {

	Person person;
	Address address;
	long personId = 0;
	long addressId = 0;
	PreparedStatement ps;
	ResultSet rs;
	AddressService addressService = new AddressService();

	/*
	 * Methods for Validate the Condition ------------------------
	 */
	public void isEmailUnique(Person person) throws Exception {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.GET_PERSON_ID_QUERY)) {
			ps.setString(1, person.getEmail());
			ps.setLong(2, person.getId());

			if (ps.executeQuery().next()) {
				throw new AppException(ErrorCode.ERR14);
			}
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR20, e);
		}
	}

	public void isNameUnique(Person person) throws Exception {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.CHECK_UNIQUE_NAME_QUERY)) {

			ps.setString(1, person.getFirstName());
			ps.setString(2, person.getLastName());
			ps.setLong(3, person.getId());

			if (ps.executeQuery().next()) {
				throw new AppException(ErrorCode.ERR16);
			}

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR15, e);
		}
	}

	public java.sql.Date convertDate(String stringDate) throws ParseException {

		try {

			java.util.Date date = new SimpleDateFormat("dd-MM-yyyy").parse(stringDate);
			java.sql.Date sqlDate1 = new java.sql.Date(date.getTime());
			return sqlDate1;
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR33, e);
		}
	}

	/*
	 * Cascade Operations: ------------------------
	 */
	public long create(Person person) {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.CREATE_PERSON_QUERY,
				Statement.RETURN_GENERATED_KEYS)) {

			PersonValidator.isNameNotNull(person);
			isEmailUnique(person);
			isNameUnique(person);
			setPs(ps, addressService.checkAddressOrCreate(person.getAddress()), person);
			
			if (ps.executeUpdate() == 1 && (rs = ps.getGeneratedKeys()).next()) {
				
				return rs.getLong(Constant.GENERATED_ID);
			}
			
			return 0;
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR21, e);
		}
	}

	public Person read(long id, boolean includeAddress) {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.READ_PERSON_QUERY)) {

			ps.setLong(1, id);
			rs = ps.executeQuery();

			if (!rs.next()) {
				return null;
			}
			person = getRs(rs);
			
			if (includeAddress) {
				person.setAddress(addressService.read(person.getAddressId()));
			}
			return person;
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR22, e);
		}
	}

	public List<Person> readAll() {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.READ_ALL_PERSONS_QUERY)) {

			List<Person> persons = new ArrayList<>();

			rs = ps.executeQuery();

			while (rs.next()) {
				person = getRs(rs);
				person.setAddress(addressService.read(person.getAddressId()));
				persons.add(person);
			}
			return persons;

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR23, e);
		}
	}

	public void update(Person person) {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.UPDATE_PERSON_QUERY)) {

			PersonValidator.isNameNotNull(person);
			isNameUnique(person);
			isEmailUnique(person);
			setPs(ps, addressService.checkAddressOrCreate(person.getAddress()), person);
			ps.setLong(6, person.getId());

			System.out.println(ps.executeUpdate());
			if (ps.executeUpdate() != 1) {
				throw new AppException(ErrorCode.ERR34);
			}
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR09, e);
		}
	}

	public void delete(long id) {

		int count = 0;
		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.GET_ADDRESS_ID_COUNT)) {
			ps.setLong(1, id);

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				count = rs.getInt(Constant.ID_COUNT);
				addressId = rs.getInt(Constant.ADDRESS_ID);
			}

			PreparedStatement preparedStatement = ConnectionService.get()
					.prepareStatement(QueryStatement.DELETE_PERSON_QUERY);
			preparedStatement.setLong(1, id);
			
			if (preparedStatement.executeUpdate() != 1) {
				throw new AppException(ErrorCode.ERR34);
			}
			if (count == 1) {
				addressService.delete(addressId);
			}

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR24, e);
		}
	}

	/*
	 * CSV file -------------------------
	 */
	public ArrayList<Person> csvPersonInsertion(String csvFilePath) {

		ArrayList<Person> persons = new ArrayList<Person>();
		try (BufferedReader lineReader = new BufferedReader(new FileReader(csvFilePath))) {

			CSVParser records = CSVParser.parse(lineReader,
					CSVFormat.EXCEL.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());

			for (CSVRecord record : records) {

				Address address = new Address(record.get(5), record.get(6), Integer.parseInt(record.get(7)));
				Person person = new Person(record.get(0), record.get(1), record.get(2), address,
						convertDate(record.get(4)));

				create(person);
				persons.add(person);
			}
			return persons;

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR31, e);
		}
	}

	public static Person getRs(ResultSet rs) throws SQLException {
		return new Person(rs.getLong(Constant.ID), rs.getString(Constant.FIRST_NAME), rs.getString(Constant.LAST_NAME),
				rs.getString(Constant.EMAIL), new Address(rs.getLong(Constant.ADDRESS_ID)),
				rs.getDate(Constant.BIRTH_DATE));
	}

	public static void setPs(PreparedStatement ps, long addressId, Person person) throws SQLException {
		ps.setString(1, person.getFirstName());
		ps.setString(2, person.getLastName());
		ps.setString(3, person.getEmail());
		ps.setLong(4, addressId);
		ps.setDate(5, person.getBirthDate());
	}

	public static void main(String[] args) {
		PersonService personService = new PersonService();
			try {
				Address address = new Address("KumaraNagar", "Tirupur", 641602);
				Person person = new Person("Murugan", "Vel", "murugan@gmail.com", address,
						personService.convertDate("21-09-2000"));
				long personId = personService.create(person);

				if (personId > 0) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}

				ConnectionService.release();

			} catch (Exception e) {
				e.printStackTrace();
			}
	}
	
}
