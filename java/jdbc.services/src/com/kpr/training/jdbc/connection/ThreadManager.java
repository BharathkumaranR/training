/*
Requirements:
    - Create a ThreadPool class (Executer class) that manages runnable executions
    
Entities:
    - ThreadPool
    - ThreadPoolExecuter
    
Function Declaration:
    - public ThreadPool()
    - protected void beforeExecute(Thread t, Runnable r) 
    - protected void afterExecute(Runnable r, Throwable t)
    
Job to be done:
    - Create a class ThreadPool that extends ThreadPoolExecuter
    - Declare a property of type ConnectionService
    
    //public ThreadPool() - Constructor
    - Declare a constructor with no parameters
    - Invoke super() method with parameters for cool thread, maximum thread, availTime, TimeUnit, Queue
    
    //protected void beforeExecute(Thread t, Runnable r)
    - Declare method beforeExecute() with parameters - (Thread t, Runnable r)
    - Invoke super method with Thread and Runnable objects as parameters 
    - Create a new ConnectionService object and store it in connectionService
  
    //protected void afterExecute(Runnable r, Throwable t)
    - Declare method afterExecute() with parameters - (Runnable r, Throwable t)
    - Invoke super() method with parameters - (Runnable r, Throwable t)
    - Invoke commitRelease(true) method
 
Pseudo code:
public class ThreadPool extends ThreadPoolExecutor {
	
	private ConnectionService connectionService;

	public ThreadPool() {
		super(Constant.MAX_THREAD, Constant.MAX_THREAD, 0, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
	}
	
	protected void beforeExecute(Thread t, Runnable r) {
		super.beforeExecute(t, r);
		connectionService = new ConnectionService();
	}
	
	protected void afterExecute(Runnable r, Throwable t) {
		super.afterExecute(r, t);
		connectionService.commitRollback(true);
	}
}
 
 */

package com.kpr.training.jdbc.connection;


import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.kpr.training.jdbc.constant.Constant;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.service.AddressService;

public class ThreadManager extends ThreadPoolExecutor {

	
	public static ConnectionService connectionService;

	public ThreadManager() {
		super(Constant.MAX_THREAD, Constant.MAX_THREAD, 0, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
	}
	
	protected void beforeExecute(Thread t, Runnable r) {
		super.beforeExecute(t, r);
		connectionService = new ConnectionService();
	}
	
	protected void afterExecute(Runnable r, Throwable t) {
		super.afterExecute(r, t);
		connectionService.release();
	}
	
	/*
	public static void main(String[] args) {
		ThreadManager manager = new ThreadManager();
		AddressService addressService = new AddressService();
		manager.submit(new Runnable() {
            @Override
            public void run() {
            	
            	long id = addressService.create(new Address("Bose nagae", "Coimbatore", 641146));
				System.out.println(id);
            }
        });
	}
	*/	
	
}
