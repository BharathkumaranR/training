/*
Requirements:
    - Create a class with methods to connect with database
    
Entities:
    - ConnectionService
    
Function Declaration:
    - public ConnectionService()
    - public void init()
    - public Connection get()
    - public void commitRollback(boolean condition)
    - public void release()

Job to be done:
    1) Create class ConnectionService
    
    //public ConnectionService() method - constructor
    1) Invoke init() method
    
    //init() method - public void init()
    1) Create a Properties object - dbDetails
    2) Invoke getResourceAsStream() method inside load() method to get the values from properties file and 
       load them in Properties Object - dbDetails
    3) Get the values of url, username and password from dbDetails with getProperty(key) method
    4) Invoke DriverManager.getConnection(url, username, password) and store the returned value in ThreadLocal
       object.
    5) Get the connection and invoke setAutoCommit(false) method
 
    
    //get() method - public Connection get()
    1) Invoke ThreadLocal.get() method to get the connection and return it.
    
    
    //commitRelease() method - public void commitRelease(boolean condition)
    1) Check if condition is equal to true
        1.1) Invoke threadLocak.get() method to get connection
        1.2) Invoke commit() method to commit changes in db
        1.2) Else Invoke Invoke threadLocak.get() method to get connection 
            1.2.1) Invoke rollback() method to rollback
    
    
    //release method - public void release(Connection connection)
    1) Invoke connection.close() method
    2) Invoke ThreadLocal.remove() method to remove the Connection object
    
    
Pseudo code:
public class ConnectionService {
	
	ThreadLocal<Connection> threadLocal = new ThreadLocal<Connection>();
	
	public ConnectionService() {
		init();
	}

    public void init() {

        try {
			Properties dbDetails = new Properties();
	        dbDetails.load(ConnectionService.class.getClassLoader().getResourceAsStream("db.properties"));
	        threadLocal.set(DriverManager.getConnection(dbDetails.getProperty(Constant.URL),
                                                        dbDetails.getProperty(Constant.USERNAME),
                                                        dbDetails.getProperty(Constant.PASSWORD)));
	        threadLocal.get().setAutoCommit(false);
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR01, e);
		}
    }
    
    public Connection get() {
        return threadLocal.get();
    }
    
    public void commitRollback(boolean condition) {
    	if (condition == true) {
    		try {
				threadLocal.get().commit();
			} catch (Exception e) {
				throw new AppException(ErrorCode.ERR02, e);
			}
    	} else {
    		try {
    			threadLocal.get().rollback();
			} catch (SQLException e) {
				throw new AppException(ErrorCode.ERR02, e);
			}
    	}
    }
    
    public void release() {
    	try {
			threadLocal.get().close();
			threadLocal.remove();
		} catch (SQLException e) {
			throw new AppException(ErrorCode.ERR03, e);
		}
    }
}
 
 */



package com.kpr.training.jdbc.connection;


import com.kpr.training.jdbc.constant.Constant;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionService {
	
	public static ThreadLocal<Connection> threadLocal = new ThreadLocal<Connection>();
	
	public ConnectionService() {
		init();
	}

    public void init() {

        try {
			Properties dbDetails = new Properties();
	        dbDetails.load(ConnectionService.class.getClassLoader().getResourceAsStream("db.properties"));
	        threadLocal.set(DriverManager.getConnection(dbDetails.getProperty(Constant.URL),
                                                        dbDetails.getProperty(Constant.USERNAME),
                                                        dbDetails.getProperty(Constant.PASSWORD)));
	        threadLocal.get().setAutoCommit(false);
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR01, e);
		}
    }
    
    public static Connection get() {
        return threadLocal.get();
    }
    
    public static void commitRollback(boolean condition) {
    	if (condition == true) {
    		try {
				threadLocal.get().commit();
			} catch (Exception e) {
				throw new AppException(ErrorCode.ERR02, e);
			}
    	} else {
    		try {
    			threadLocal.get().rollback();
			} catch (SQLException e) {
				throw new AppException(ErrorCode.ERR02, e);
			}
    	}
    }
    
    public void release() {
    	try {
			threadLocal.get().close();
			threadLocal.remove();
		} catch (SQLException e) {
			throw new AppException(ErrorCode.ERR03, e);
		}
    }
    
    
}	

