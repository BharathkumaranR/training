/*
Entity
   1.Connections

Method declaration
   1.public static Connection getConnection()

Jobs To be Done:
   1.Create and store url, username and password to connect with MYSQL servers
   2.In each INSERT, DELETE, READ and UPDATE operations(CRUD). 
         2.1)Invoke the static getConnection method.
   3.Return the connections.

Pseudo Code:

public class Connections {
	public static final String url = "jdbc:mysql://localhost:3306/database";
	public static final String username = "root";
	public static final String password = "Bharath29@";
	public static Connection connection;
	public static Connection getConnection() {
		try {
			connection = DriverManager.getConnection(url, username, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}
}
 */

package com.kpr.training.jdbc.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connections {
    
    
    public static final String url = "jdbc:mysql://localhost:3306/database";
    public static final String username = "root";
    public static final String password = "Bharath29@";

    public static Connection connection;

    public static Connection getConnection() {
        try {
             connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
             e.printStackTrace();
        }
             return connection;
    }

}
