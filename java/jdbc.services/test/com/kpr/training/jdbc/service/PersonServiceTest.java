/*
  Problem Statement
  1.To Test All possible conditions for Person Service 
  
  Requirement
  1.To Test All possible conditions for Person Service
  
  Entity
  1.ServicesTest
  2.PersonService
  3.AppException
  4.Connection
  5.Person
  6.Address
  
  Method Signature
  1.public void setUp()
  2.public void insertPersonWithAddressId() 
  3.public void insertPersonWithAddress()
  4.public void readPersonWithAddress()
  5.public void readPersonWithOutAddress()
  6.public void insertPerson();
  7.public void readPerson();
  8.public void readAllPerson();
  9.public void updatePerson()
  10.public void deletePerson()
  
  Jobs to be done:
  ->public void setUp()
   1.Create Person and Address class with values.
   2.Prepare personService object of type PersonService.
  
   @Test insertPersonWithAddress(Person Insertion with Already Created Address Id) 
   1.Invoke the PersonServices class insertPeronAddress method to insert
Person and Address values.
   2.AssertJUnit assertTrue method (pass Check insertPersonStatus is greater than 0 ).
   
 @Test insertPersonWithAddress(Person Insertion with Address) 
   1.Invoke the PersonServices class insertPeronAddress method to insert
Person and Address values.
   2.AssertJUnit assertTrue method (pass Check insertPersonStatus is greater than 0 ).
  
  @Test for readPerson
  1.Invoke personService readPerson method (pass personId and boolean value to add address 'true")
and store in List<Person> objectList.
     
  @Test for readPerson
  2.Invoke personService readPerson method (pass personId and boolean value for without address 'false")
and store in List<Person> objectList.
     
  @Test for insertPerson
  1.Invoke personService insertPerson method (pass person object) 
and store in insertPersonStatus.
  2.AssertJUnit assertTrue method (pass Check insertPersonStatus is greater than 0 ).
  
  @Test for readPerson
  1.Invoke personService readPerson method (pass person id) and
store in List<Person> objectList .
  2.AssertJUnit assertTrue method (pass Check objectList is not equal to null ).
  
  @Test for readPerson
  1.Invoke personService readAllPerson method and
store in List<Person> objectList.
  2.AssertJUnit assertTrue method (pass Check objectList is not equal to null ).
  
  @Test for updatePerson
  1.Invoke personService updatePerson method (pass person object) 
and store in udpateStatus.
  2.AssertJUnit assertTrue method (pass Check udpateStatus is not equal to false ).
  
  @Test for deletePerson
  1.Invoke personService deletePerson method (pass person id) 
and store in deleteStatus.
  2.AssertJUnit assertTrue method (pass Check deleteStatus is not equal to false ).

Psudeo Code:
public class PersonSerivceTest {
	long id;
	long personWithAddressId;
	long personWithOutAddressId;
	long personUpdateId = 1;
	PersonService personService;
	Person person1;
	Person person2;
	Person person3;
	Address address1;
	Address address2;
	Person personUpdate;
	Person personGetUpdated;
	Person personInsert;
	Person personCreateWithAddressId;
	Person personAddressCreate;
	Connection connection;
	ConnectionService connectionService;

	@BeforeClass
	public void setUp() throws Exception {
		personService = new PersonService();
		connectionService = new ConnectionService();
		connection = connectionService.initConnection();
		personCreateWithAddressId = new Person("Varun", "Varun@gmail.com", 0, Date.valueOf("2001-01-18"));
		personAddressCreate = new Person("Praveen", "Praveen@gmail.com", 0, Date.valueOf("2000-09-20"));
		person1 = new Person("Karun", "Karun@gmail.com", 2, Date.valueOf("2000-11-18"));
		personInsert = new Person("Kumar", "Kumar@gmail.com", 2, Date.valueOf("2000-09-16"));
		personUpdate = new Person(personUpdateId, "Sajith", "Sajith@gmail.com", 2, Date.valueOf("2000-01-11"));
		address1 = new Address("Perumal", "Kanchipuram", 601200);
		address2 = new Address("RajivGandhi", "Tirupur", 641602);
	}

	@Test(priority = 1, description = "Person Insertion with Address Id")
	public void insertPersonWithAddressId() throws AppException, SQLException {
		personWithAddressId = personService.insertPeronAddress(personCreateWithAddressId, address1, connection);
		Assert.assertTrue(personWithAddressId > 0);
	}

	@Test(priority = 2, description = "Person Insertion with Address")
	public void insertPersonWithAddress() throws AppException, SQLException {
		personWithOutAddressId = personService.insertPeronAddress(personAddressCreate, address2, connection);
		Assert.assertTrue(personWithOutAddressId > 0);
	}

	@Test(priority = 3, description = "Try Creating person with duplicate email", expectedExceptions = {
			AppException.class }, expectedExceptionsMessageRegExp = "Duplicate email address found")
	public void insertPersonSecondTest() throws SQLException {
		personService.insertPeronAddress(personCreateWithAddressId, address1, connection);

	}

	@Test(priority = 4, description = "Person Insertion with Address")
	public void readPersonWithAddress() throws AppException, SQLException {
		Assert.assertTrue(personService.readPerson(personWithAddressId, true, connection).toString() != null);
	}

	@Test(priority = 5, description = "Person Insertion with out Address")
	public void readPersonWithOutAddress() throws AppException, SQLException {
		Assert.assertTrue(personService.readPerson(personWithOutAddressId, false, connection) != null);
	}

	@Test(priority = 6, description = "Person Creation with unique email")
	public void insertPerson() throws AppException, SQLException {
		this.id = personService.createPerson(person1, connection);
		Assert.assertTrue(this.id > 0);
	}

	@Test(priority = 7, description = "Read Only One Person")
	public void readPerson() throws AppException, SQLException {
		System.out.println(this.id);
		person2 = personService.readPerson(this.id, connection);
		person1.id = this.id;
		Assert.assertEquals(person2.toString(), person1.toString());
	}

	@Test(priority = 8, description = "Read All Person With Address")
	public void readAllPersonWithAddress() throws AppException, SQLException {
		Assert.assertTrue(personService.readAllPerson(connection).toString() != null);
	}

	@Test(priority = 9, description = "Update Only One Person")
	public void updatePerson() throws AppException, SQLException {
		personService.updatePerson(personUpdate, personUpdateId, connection);
		personGetUpdated = personService.readPerson(this.personUpdateId, connection);
		Assert.assertEquals(personGetUpdated.toString(), personUpdate.toString());
	}

	@Test(priority = 10, description = "Delete Only One Person")
	public void deletePerson() throws AppException, SQLException {
		personService.deletePerson(this.id, connection);
	}

	@AfterClass
	public void connectionClose() {
		try {
			connectionService.releaseConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}


@author SA
*
*/

package com.kpr.training.jdbc.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;
import com.kpr.training.jdbc.service.ConnectionService;
import com.kpr.training.jdbc.service.PersonService;

public class PersonServiceTest {

	PersonService personService = new PersonService();
	int expectedReadAllPersons ;
	List<Person> actualReadAllPersons = new ArrayList<Person>();
	CountDownLatch latch = new CountDownLatch(1);
	Person person;
	Person personEmailDuplicate;
	Person personNameDuplicate;
	Person personNameNull;
	long personCreatedId = 2;
	Person expectedCreated;
	long personReadId = 1;
	Person actualReadPerson;
	Person exceptedReadPerson;
	Person exceptedUpdatePerson;
	Person actualUpdatePerson;
	Person exceptedDeletePerson;

	@BeforeClass
	public void setUp() throws Exception {
		person = new Person("Bala", "Kumaran", "kumaran@gmail.com", new Address("OMR", "Chennai", 600028),
				personService.convertDate("21-09-2000"));
		personNameNull = new Person("", " ", "Bala@gmail.com", new Address("KumaraNagar", "Tirupur", 641602),
				personService.convertDate("01-10-2000"));
		personNameDuplicate = new Person("Murugan", "Vel", "murugan@gmail.com",
				new Address("KumaraNagar", "Tirupur", 641602), personService.convertDate("21-09-2000"));
		personEmailDuplicate = new Person("Bala", "Murugan", "murugan@gmail.com",
				new Address("KumaraNagar", "Tirupur", 641602), personService.convertDate("21-09-2000"));
		exceptedReadPerson = new Person(1, "Rajith", "Raja", "Rajith@gmail.com", new Address("AnnaNagar", "Tirupr", 600000),
				personService.convertDate("10-10-2001")); 
		expectedReadAllPersons = 2;
		actualUpdatePerson = new Person(personCreatedId,"Suresh", "Kumar", "kumar@gmail.com",
				new Address("KumaraNagar", "Tirupur", 641602), personService.convertDate("21-09-2000"));
	}

	@Test(priority = 1, description = "Insert Person With Address")
	public void createPersonWithAddress() throws Exception {
		personCreatedId = personService.create(person);
		Assert.assertTrue(personCreatedId != 0 );
		
		if(personCreatedId != 0 ) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
		ConnectionService.release();
	}


	@Test(priority = 2, description = "Insert Person With Name Null", expectedExceptions = {
			AppException.class }, expectedExceptionsMessageRegExp = "Person's name should not be null or empty")
	public void createPersonWithNameNull() throws Exception {
		personCreatedId = personService.create(personNameNull);
		ConnectionService.commitRollback(false);
		ConnectionService.release();
	}
	
	@Test(priority = 3, description = "Insert Person With Name duplicate", expectedExceptions = {
			AppException.class }, expectedExceptionsMessageRegExp = "Duplicate Person's name address found")
	public void createPersonWithNameDuplicate() throws Exception {
		personCreatedId = personService.create(personNameDuplicate);
		ConnectionService.commitRollback(false);
		ConnectionService.release();
	}
	
	@Test(priority = 4, description = "Insert Person With Email duplicate", expectedExceptions = {
			AppException.class }, expectedExceptionsMessageRegExp = "Duplicate Person's email address found")
	public void createPersonWithEmailDuplicate() throws Exception {
		personCreatedId = personService.create(personEmailDuplicate);
		ConnectionService.commitRollback(false);
		ConnectionService.release();
	}
	
	@Test(priority = 5, description = "Read Person With Address")
	public void readPersonWithAddress() throws Exception {
		actualReadPerson = personService.read(personReadId, true);
		Assert.assertEquals(actualReadPerson.toString(), exceptedReadPerson.toString());
		
		if (actualReadPerson.toString() == exceptedReadPerson.toString()) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
		ConnectionService.release();
	}
	
	@Test(priority = 6, description = "Read All Person With Address")
	public void readAllPersonWithAddress() throws Exception {
		actualReadAllPersons = personService.readAll();
		Assert.assertEquals(actualReadAllPersons.size(), expectedReadAllPersons);
		System.out.println(actualReadAllPersons);
		if (actualReadAllPersons.size() == expectedReadAllPersons) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
		ConnectionService.release();
	}
	
	@Test(priority = 7, description = "Update Person With Address")
	public void updatePersonWithAddress() throws Exception {
		personService.update(actualUpdatePerson);
		exceptedUpdatePerson = personService.read(actualUpdatePerson.getId(), true);
		actualUpdatePerson.setAddressId(exceptedUpdatePerson.getAddressId());
		System.out.println(exceptedUpdatePerson);
		Assert.assertEquals(actualUpdatePerson.toString(), exceptedUpdatePerson.toString());

		if (actualUpdatePerson.toString() == exceptedUpdatePerson.toString()) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
		ConnectionService.release();
	}
	@Test(priority = 8, description = "Delete Person With out Address")
	public void deletePersonWithOutAddress() throws Exception {
		personService.delete(person.getId());
		exceptedUpdatePerson = personService.read(person.getId(), true);
		Assert.assertEquals(null, exceptedDeletePerson);

		if (null == exceptedDeletePerson) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
		ConnectionService.release();
	}
	
}
