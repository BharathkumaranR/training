/*Convert the following anonymous class into lambda expression.
interface CheckNumber {
    public boolean isEven(int value);
}

public class EvenOrOdd {
    public static void main(String[] args) {
        CheckNumber number = new CheckNumber() {
            public boolean isEven(int value) {
                if (value % 2 == 0) {
                    return true;
                } else return false;
            }
        };
        System.out.println(number.isEven(11));
    }
}
Word Breakdown Structure(WBS)
1.Requirements
   -To convert the following anonymous class into lambda expression.
2.Entities
   - EvenOrOdd
   - CheckNumber (Interface)
3.Function Declaration
   - int numbers(int number1,int number2);
   - public static void main(String[] args)
4.Jobs to be done
   1.Create a class as EvenOrOdd with interface as CheckNumber
   2.Inside the declaring the single method with integer variable value parameters
   3.In the class main creating interface object and assigning with passing int value of statement if condition to check 
value mod of 2 is equal to 0 . if true return true outside if block return false.
   4.Print statement invoking the interface single method with integer value and finally return the value using assigned
interface object lambda expression statements.

Pseudocode:
interface CheckNumber {
    public boolean isEven(int value);
}
        
        CheckNumber number = (value) -> { // Converted into Java Lambda Expression 
            if (value % 2 == 0) {
                return true;
            } else return false;
        };

Program:*/

interface CheckNumber {
    public boolean isEven(int value);
}

public class EvenOrOdd {

	public static void main(String[] args) {
        CheckNumber number = (value) -> { // Converted into Java Lambda Expression 
            if (value % 2 == 0) {
                return true;
            } else return false;
        };
        System.out.println(number.isEven(11));
        
	}
	
}	