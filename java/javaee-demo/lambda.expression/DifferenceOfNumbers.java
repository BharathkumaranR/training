/*Write a program to print difference of two numbers using lambda expression and the single method interface

Word Breakdown Structure(WBS)
1.Requirements
   -To write a Program to print difference of two numbers using lambda expression and the single method interface
2.Entities
   - DifferenceOfNumbers
   - Difference (Interface)
3.Function Declaration
   - int numbers(int number1,int number2);
   - public static void main(String[] args)
4.Jobs to be done
   1.Create a class as DifferenceOfNumbers with interface as Difference
   2.Inside the declaring the single abstract method with two integer parameters.
   3.In the main class creating interface object and assigning the lambda expression to return two integers difference.
   4.Print statement invoking the interface single method with integer values and finally return the value using assigned
interface object lambda expression 

Pseudocode:
interface Difference {
	int numbers(int number1,int number2);
}

public class DifferenceOfNumbers {

	public static void main(String[] args) {
		Difference difference = (number1,number2) -> number1 - number2;
		System.out.println(difference.numbers(150,74));
	}
}		
Program:

Program to print difference of two numbers using lambda expression and the single method interface */


interface Difference {
	int numbers(int number1,int number2);
}

public class DifferenceOfNumbers {

	public static void main(String[] args) {

        //Assigning the lambda expression to return the difference of numbers
		Difference difference = (number1,number2) -> number1 - number2;
        //invoking the interface single abstract method with integer values 
		System.out.println(difference.numbers(150,74));
	}

}
