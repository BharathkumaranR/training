/*  Write a program to print the volume of a Rectangle using lambda expression.

Word Breakdown Structure(WBS)
1.Requirements
   -To write a program to print the volume of a Rectangle using lambda expression.
2.Entities
   - VolumeOfRectangle
   - PrismValues (Interface)
3.Function Declaration
   - int Values(int base,int length,int height);
   - public static void main(String[] args)
4.Jobs to be done
   1.Create a class as VolumeOfRectangle with interface as DimensionValues.
   2.Inside that declare the single abstract method with integer parameters.
   3.In the class main creating interface object and assigning with passing prism values returning prism value.
   4.Print statement invoking the interface single method with multiple integer value and finally return value using assigned
interface object lambda expression.

pseudocode:
interface DimensionValues 
	int Values(int base,int length,int height);

public class VolumeOfRectangle {
		DimensionValues dimensionValues = (base,length,height) -> base * length * height;
		System.out.println(dimensionValues.Values(5,8,16));
}		

Program:*/

interface DimensionValues {
	int Values(int base,int length,int height);
}

public class VolumeOfRectangle {

	public static void main(String[] args) {
		DimensionValues dimensionValues = (base,length,height) -> base * length * height;
		System.out.println(dimensionValues.Values(5,8,16));
	}

}
