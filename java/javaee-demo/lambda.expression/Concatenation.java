/*Write a Lambda expression program with a single method interface to concatenate two strings
  
Word Breakdown Structure(WBS)
1.Requirements
   -To write a Lambda expression Program with a single method interface to concatenate two strings
2.Entities
   - Concatenation
   - StringConcat (Interface)
3.Function Declaration
   - String stringconcat(String string1, String string2);
   - public static void main(String[] args)
4.Jobs to be done
   1.Inside the main method declare the single method with two string parameters
   3.In the main class create interface object and assigning the lambda expression to return two strings concatenate 
   4.Print statement invoking the interface single method with string values and finally return the value using assigned
interface object lambda expression 

Pseudocode:interface StringConcat {
	String stringconcat(String string1, String string2);
}
public class Concatenation {
    StringConcat string = (string1,string2) -> string1 + string2;
    System.out.println(string.stringconcat("Hello ", "World"));
    }
}
    
Program

Lambda expression program with a single method interface to concatenate two strings  */
interface StringConcat {
	String stringconcat(String string1, String string2);
}
public class Concatenation {

	public static void main(String[] args) {
		//Assigning the lambda expression to return two strings concatenate
		StringConcat string = (string1,string2) -> string1 + string2;
		//invoking the interface single abstract method with string values 
		System.out.println(string.stringconcat("Hello ", "World"));

	}

}
