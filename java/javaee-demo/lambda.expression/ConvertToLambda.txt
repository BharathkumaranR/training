Convert this following code into a simple lambda expression
        int getValue(){
            return 5;
        }

Word Breakdown Structure(WBS)
1.Requirements
   -To write a code to convert the given code into a lambda expression.
2.Function Declaration
   - int getValue()
3.Jobs to be done
   1.Convert given method into lambda expression.

Answer:

  Lambda Expression:
     
     getValue = () -> 5 
     
     Assigning and returning value to the variable get value using lambda expression.