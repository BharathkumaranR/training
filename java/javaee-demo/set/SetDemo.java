/*Create a list

    => Add 10 values in the list
    => Create another list and perform addAll() method with it
    => Find the index of some value with indexOf() and lastIndexOf()
    => Print the values in the list using 
        - For loop
        - For Each
        - Iterator
        - Stream API
    => Convert the list to a set
    => Convert the list to a array
+ Explain about contains(), subList(), retainAll() and give example

Word Breakdown Structure(WBS)

1.Requirements
   -To write a program to create a list and perform the following operations
2.Entities
   - SetDemo
3.Function Declaration
	public static void main(String[] args)
4.Jobs to be done
   1.Create a class named SetDemo and declare main.
   2.Inside the main creating set called bikes and adding 10 bikes using add() method and printing the added set.
   3.Creating another set of newBikes and adding 4 newBikes using addAll() method adding bikes and newBikes sets and after using 
removeAll() method removing added newBikes from bikes set.
   4.Display the set using while loop with hasNext() and forEach.
   5.Checking the set with contain() method to set containing or not and to check set is empty or not using isEmpty() method.

Psudeo Code:

public class SetDemo {
	public static void main(String[] args) {
	    Set<String> cars = new HashSet<>();
	    ADD 10 ELEMETS
	    Set<String> newCars = new HashSet<>();
	    ADD ELEMENTS TO ANOTHER SET FROM SET
	    Iterator<String> car = cars.iterator();
        while (car.hasNext()) {
            System.out.println(car.next());
        }
        cars.forEach(System.out::println)
        System.out.println("Checks car set contains Tata Motors " + cars.contains("Tata Motors"));
        System.out.println("Checks newCars is empty or not? " + newCars.isEmpty());
	}
}

Program:
*/


import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetDemo {

	public static void main(String[] args) {
		
		//Creating a set and adding 10 elements to it.
		Set<String> bikes = new HashSet<>();
        bikes.add("TVS");
        bikes.add("Apache");
        bikes.add("Pulsar");
        bikes.add("Splendor");
        bikes.add("Honda");
        bikes.add("Jupiter");
        bikes.add("Ray");
        bikes.add("Enfield");
        bikes.add("Duke");
        bikes.add("Fassino");
        System.out.println("Creating a set and adding 10 elements to it:\n" + bikes );
        
        //Creating another set with some elements.
        Set<String> newbikes = new HashSet<>();
        newbikes.add("Yamaha");
        newbikes.add("Thunder");
        newbikes.add("Classic");
        newbikes.add("Deluxe");
        
        //Perform addAll() method and removeAll() to the set.
        bikes.addAll(newbikes);
        System.out.println("\taddAll() method ");
        System.out.println(bikes);
        System.out.println("\tremoveAll() method ");
        bikes.removeAll(newbikes);
        System.out.println(bikes);
        newbikes.clear();
        
        
      //Displaying all the set elements using Iterator interface
        Iterator<String> bike = bikes.iterator();
        System.out.println("\tUsing While loop");
        while (bike.hasNext()) {
            System.out.println(bike.next());
        }

        //Displaying all the set elements using ForEach
        System.out.println("\tUsing ForEach");
        bikes.forEach(System.out::println);

        //contains()
        System.out.println("\tcontains() method");
        System.out.println("Checks car set contains Tata Motors " + bikes.contains("Thunder"));

        //isEmpty()
        System.out.println("\tisEmpty() method");
        System.out.println("Checks newbikes is empty or not? " + newbikes.isEmpty());

	}

}
