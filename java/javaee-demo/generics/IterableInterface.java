/*Generics-Implementing Iterable interface:
1. Write a program to print employees name list by implementing iterable interface.

Word Breakdown Structure(WBS):
 1.Requirements:
  		To write a program to print employees name list by implementing iterable interface.
 2.Entity:
  	  - MyIterable
  	  - IterableInterface
  
 3.Function Declaration:
  	  - public MyIterable(T[] t),
  	  -	public Iterator<T> iterator().
  
 4.Jobs To Be Done:
  		1.Creating the MyIterable class that implements the Iterable.
  		2.Creating the local variable list as generic type.
  		3.Creating a method MyIterable of generic type  and assigning already defined String values from the main class.
  	    4.Creating the main class as IterableInterface .
  		5.Declaring the String values in the name variable.
  		6.Creating the object myList for the variables to be iterated.
  		7.After assigning the variables in the list by iterator implement, the final result is printed.

Pseudocode:
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class MyIterable<T> implements Iterable<T> {
	private List<T> list;
	public MyIterable(T[] t) {
		list = Arrays.asList(t);
	}
	public Iterator<T> iterator() {
		return list.iterator();
	}
}
		String[] games = {"dream11", "mpl", "RummyCircle", "TempleRun", "CandyCrush"};
		MyIterable<String> game = new MyIterable<>(games);
		for (String string : game) {
			System.out.println(string);
		}

	}

}
Program:
 */

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class MyIterable<T> implements Iterable<T> {
	private List<T> list;
	public MyIterable(T[] t) {
		list = Arrays.asList(t);
	}
	public Iterator<T> iterator() {
		return list.iterator();
	}
}
public class IterableInterface {

	public static void main(String[] args) {
		String[] games = {"dream11", "mpl", "RummyCircle", "TempleRun", "CandyCrush"};
		MyIterable<String> game = new MyIterable<>(games);
		for (String string : game) {
			System.out.println(string);
		}

	}

}
