/*Generics-Implementing Iterable interface:
2. Why except iterator() method in iterable interface, are not neccessary to define in the implemented class?
  
 Work Breeakdown Structure(WBS):
 1.Requirements:
 	 ->To illustrate Why except iterator() method in iterable interface, are not neccessary to define in the implemented class?
        
Answer:
   The Java Iterable interface has three methods of which only one needs to be implemented and
the other two have default implementations.
 */
