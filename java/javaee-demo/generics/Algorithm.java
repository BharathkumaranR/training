/*Generics-Methods and Class:

 2.Will the following class compile? If not, why?
   	public final class Algorithm {
           public static <T> T max(T x, T y) {
               return x > y ? x : y;
           }
       }
Word Breakdown Structure(WBS):
  1.Requirements : 
  		To find that will the following class compile? If not, why?
  2.Entities :
  		public class Algorithm
  3.Function Declaration :
  		public static <T> T max(T x, T y)
  4.Jobs To Be Done:
     1.Finding the code compiles or not.
Answer:
The code does not compile, because the greater than (>) operator applies only to primitive numeric types.


public class Algorithm {

	public static <T> T max(T x, T y) {
        return (x > y) ? x : y;  //The operator > is undefined for the argument type(s) T, T
    }

}
*/