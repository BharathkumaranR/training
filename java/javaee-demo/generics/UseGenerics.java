/*5. What will be the output of the following program?
public class UseGenerics {
    public static void main(String args[]){  
        MyGen<Integer> m = new MyGen<Integer>();  
        m.set("merit");
        System.out.println(m.get());
    }
}
class MyGen<T>
{
    T var;
    void  set(T var)
    {
        this.var = var;
    }
    T get()
    {
        return var;
    }
}
Word Breakdown Structure(WBS):
 1.Requirements : 
     -To illustrate What will be the output of the following program
 2.Entities :
 	 - public class UseGenerics.
 3.Function Declaration :
 	 - public static void main(String[] args)
 4.Jobs To Be Done:
 		1.Create the UseGenerics class
 		2.Create the count method which returns the count of odd numbers present in a list.
 		3.Create the main method and create a list reference.
 		4.Add elements inside a list.
 		5.Call a count method and printing the number of odd numbers.
 		
Pseudocode:
	public static void main(String[] args) {
        MyGen<Integer> print = new MyGen<Integer>();  
        print.set("merit");
        System.out.println(print.get());
class MyGen<T>
{
    T var;
    void  set(String var)
    {
        //this.var =var;
        this.var =(T) var;
    }
    T get()
    {
        return var;		

	}

}
Program:
 */
public class UseGenerics {

	public static void main(String[] args) {
        MyGen<Integer> print = new MyGen<Integer>();  
        print.set("merit");
        System.out.println(print.get());
    }
}
class MyGen<T>
{
    T var;
    void  set(String var)
    {
        //this.var =var;
        this.var =(T) var;
    }
    T get()
    {
        return var;		

	}

}
/*
Output:
Exception in thread "main" java.lang.Error: Unresolved compilation problem: 
	Type mismatch: cannot convert from String to T

	at MyGen.set(UseGenerics.java:47)
	at UseGenerics.main(UseGenerics.java:38)
*/ 