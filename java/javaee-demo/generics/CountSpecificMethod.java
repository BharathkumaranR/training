/*1. Write a generic method to count the number of elements in a collection that have a specific
   property (for example, odd integers, prime numbers, palindromes).

Work Breakdown Structure(WBS):
 1.Requirements : 
 		To write a generic method to count the number of elements in a collection that have a specific
 property (for example, odd integers, prime numbers, palindromes).
 
 2.Entities :
 	->public class CountSpecificMethod
 3.Function Declaration :
 	- public static void main(String[] args)
 4.Jobs To Be Done:
 		1.Create the CountSpecificProperty class
 		2.Create the count method which returns the count of odd numbers present in a list.
 		3.Create the main method and create a list reference.
 		4.Add elements inside a list.
 		5.Invoking a count method and printing the number of odd numbers.
 		
Pseudocode:
public class CountSpecificMethod {
	
	
	public static int count(ArrayList<Integer> list) {
		int c = 0;
		for(int elements : list) {
			if(elements % 2 != 0) {
				c++;
			}
		}
		return c;
	}
	

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		Add  5 elements in a list;
		System.out.println("Number of odd integers : " + count(list));

	}

}			 
Program
 */

import java.util.ArrayList;

public class CountSpecificMethod {
	
	
	public static int count(ArrayList<Integer> list) {
		int c = 0;
		for(int elements : list) {
			if(elements % 2 != 0) {
				c++;
			}
		}
		return c;
	}
	

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(20);
		list.add(41);
		list.add(13);
		list.add(12);
		list.add(91);
		System.out.println("Number of odd integers : " + count(list));

	}

}
