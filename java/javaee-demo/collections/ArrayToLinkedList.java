/* 1.Create an array list with 7 elements, and create an empty linked 
list add all elements of the array list to linked list ,traverse the elements and display the result
Work Breakdown Structure(WBS):
1.Requirements:
    -To create ArrayList with elements and add all elements in linked list.
2.Entities
    -public class ArrayToLinkedList
3.Function Declaration
    - public static void main(String [] args)
4.Jobs to be done
    1. Create a class named as ArrayToLinkedList and declare main method
    2. Create a array list as players
    3. Adding 7 values in the ArrayList
    4. Create linked list name player
    5. Add all values from array list to linked list
    6. Using iterator printing all values
    
Pseudocode:
public class ArrayToLinkedList {

	public static void main(String[] args) {
		List<String> players = new ArrayList<String>();
		Add elements in a list;
		System.out.println("Elements in the array List : "+players);
		List<String> player = new LinkedList<String>(); //Creating LinkedList
        while(player.size() > 0) {
            players.add(player.remove(0)); //Removing Arraylist elements adding to LinkedList
            }
		System.out.println("Elements in the Linked List : " + player);
        Iterator<String> iterator = player.listIterator(4); 
        System.out.println("After Traverse the elements using Iterator:"); 
        while(iterator.hasNext()) { //Printing the linked list elements 
           System.out.println(iterator.next()); 
        } 

	}

}
		
Program:
*/

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ArrayToLinkedList {

	public static void main(String[] args) {
		List<String> players = new ArrayList<String>();
		players.add("Andrew"); 	//Add 7 values in the list
		players.add("Kohli");
		players.add("Dhoni");
		players.add("Rashid");
		players.add("Bahir");
		players.add("Raina");
		players.add("Dennie");
		System.out.println("Elements in the array List : "+players);
		List<String> player = new LinkedList<String>(); //Creating LinkedList
        while(player.size() > 0) {
            players.add(player.remove(0)); //Removing Arraylist elements adding to LinkedList
            }
		System.out.println("Elements in the Linked List : " + player);
        Iterator<String> iterator = player.listIterator(4); 
        System.out.println("After Traverse the elements using Iterator:"); 
        while(iterator.hasNext()) { //Printing the linked list elements 
           System.out.println(iterator.next()); 
        } 

	}

}
