/*8 Districts are shown below
    Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy  
     to be converted to UPPERCASE.
Work Breakdown Structure(WBS):
1.Requirements:
   * To convert the given into uppercase.
2.Entities
   * public class ConvertIntoUpperCase
3.Function Declaration
   -none-
4.Jobs to be done
    1.Create a class name ConvertIntoUpperCase and declare main method
    2.Create a list as players.
    3.Add 8 values in the list
    4.Converted array list to UPPERCASE using toUpperCase() method
    5.Print the array list
    
Pseudocode:
import java.util.ArrayList;
import java.util.List;

public class ConvertToUppercase {

	public static void main(String[] args) {
		List<String> districts = new ArrayList<String>();
		Add elements to the list;
		for(String district : districts) {
		     System.out.println(district.toUpperCase() ); //Converting the districts name to UpperCase
		    }		

	}

}		
Program:
     */
import java.util.ArrayList;
import java.util.List;

public class ConvertToUppercase {

	public static void main(String[] args) {
		List<String> districts = new ArrayList<String>();
		districts.add("Madurai"); 	//Add 8 values in the list
		districts.add("Coimbatore");
		districts.add("Theni");
		districts.add("Chennai");
		districts.add("Karur");
		districts.add("Salem");
		districts.add("Erode");
	    districts.add("Trichy");
		for(String district : districts) {
		     System.out.println(district.toUpperCase() ); //Converting the districts name to UpperCase
		    }		

	}

}
