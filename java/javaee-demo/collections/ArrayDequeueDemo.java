/*
 4.Use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),pollFirst(),poolLast() 
methods to store and retrieve elements in ArrayDequeue.
Work Breakdown Structure(WBS):
1.Requirements:
   -To illustrate Dequeue with elements.
2.Entities
   -public class ArrayDequeueDemo
3.Function Declaration
   - public static void main(String [] args)
4.Jobs to be done
    1.Create a class name ArrayDequeueDemo and declare main method
    2.Create a array dequeue as player
    3.Add 7 values using addFirst(),addLast(),add methods in the dequeue
    4.Print peek values using peekFirst(),peekLast() methods in the dequeue
    5.Remove element using pollFirst(),pollLast(),removeFirst(),removeLast() methods in
the dequeue
    6.Print the dequeue 
    
Pseudocode:
import java.util.Deque;
import java.util.LinkedList;
public class ArrayDequeueDemo {

	public static void main(String[] args) {
		Deque<String> players = new LinkedList<String>(); 
		Perform Add
		       ,Peek
		       ,Poll
		       ,Remove from dequeue
		System.out.println(players);

	}

}		       
		            
Program:
 */

import java.util.Deque;
import java.util.LinkedList;
public class ArrayDequeueDemo {

	public static void main(String[] args) {
		Deque<String> players = new LinkedList<String>(); 
		players.addFirst("Andrew");  //Add at first
		players.add("Kohli");
		players.add("Dhoni");
		players.add("Rashid");
		players.add("Bahir");
		players.addLast("Raina");   //add at last
		System.out.println("Peek First Value: "+players.peekFirst()); //Peek first value
		System.out.println("Peek Last Value: "+players.peekLast()); //Peek last value
		players.pollFirst();  //poll first value
		players.pollLast();   //poll last value
		players.removeFirst();  //remove first value
		players.removeLast();   //remove last value
		System.out.println(players);

	}

}
