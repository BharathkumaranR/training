/*5.Add and remove the elements in stack.

Work Breakdown Structure(WBS):
1.Requirements:
   -To add and remove Stack with elements
2.Entities
    - AddRemoveStack
3.Function Declaration
    - public static void main(String [] args)
4.Jobs to be done
     1.Create a class name AddRemoveStack and declare main method
     2.Create a stack players using generic type
     3.Add 4 values in the stack using push() method.
     4.Using pop() method removing stack top element
     5.Print the stack elements.
     
Pseudocode:
import java.util.Stack;
public class AddRemoveStack {

	public static void main(String[] args) {
        Stack<String> players = new Stack<String>(); 
        Add elements to stack;
        Remove element from stack;
        System.out.println(players); //Printing the stack elements

	}

}        
        
Program: 
 */

import java.util.Stack;
public class AddRemoveStack {

	public static void main(String[] args) {
        Stack<String> players = new Stack<String>();  //Creating Stack using generic type
        players.push("Andrew"); //Adding a element to stack
        players.push("Kohli");
        players.push("Dhoni");
        players.push("Raina"); 
        players.pop();  //Removing a element to stack
        System.out.println(players); //Printing the stack elements

	}

}
