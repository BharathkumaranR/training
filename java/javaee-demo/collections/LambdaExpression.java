/*8.Addition,Substraction,Multiplication and Division concepts are achieved 
using Lambda expression and functional interface.

Work Breakdown Structure(WBS):
1.Requirements:
   - Lambda with single method interface 
2.Entities
   -public class LambdaExpression
   - Arthmetic (Interface)
3.Function Declaration
    - int operation(int a, int b)
4.Jobs to be done
    1.Create a class named as LambdaExpression and declare main method.
    2.Create a interface as Arithmetic and operation(int a, int b) method.
    3.Create Lambda initialising the Addition,Substraction,Multiplication and Division the two variables. 
    4.Print the each Arithmetic operations.
    
Pseudocode:
interface Arithmetic {
	int operation(int a, int b);
}

public class LambdaExpression {

	public static void main(String[] args) {
    perform every operations;
    print the result;
    
    }
    
}    

Program:
  */

interface Arithmetic {
	int operation(int a, int b);
}

public class LambdaExpression {

	public static void main(String[] args) {
		Arithmetic addition = (int a, int b) -> (a + b); //Adding two elements
		System.out.println("Addition = " + addition.operation(81, 96));
		Arithmetic subtraction = (int a, int b) -> (a - b);//Substract two elements
		System.out.println("Subtraction = " + subtraction.operation(76, 33));
		Arithmetic multiplication = (int a, int b) -> (a * b);//Multiply two elements
		System.out.println("Multiplication = " + multiplication.operation(17, 6));
		Arithmetic division = (int a, int b) -> (a / b);//Dividing two elements
		System.out.println("Division = " + division.operation(222, 11));

	}

}
