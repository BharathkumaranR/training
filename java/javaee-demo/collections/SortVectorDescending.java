/* 3.Code for sorting vetor list in descending order.
Work Breakdown Structure(WBS):
1.Requirements:
   -To code for sorting vetor list in descending order.
2.Entities
   -public class SortVectorDesending.
3.Function Declaration
   - public static void main (String [] args)
4.Jobs to be done
    1.Create a class named as SortVectorDesending and declare main() method.
    2.Create a vector as Animals. 
    3.Add 6 values in the vector and Print the vector elements.
    4.Reversing vetor elements using reverseOrder() method. 
    5.Print the reversed vector.
    
Pseudocode:
public class SortVectorDescending {

	public static void main(String[] args) {
		Vector<String> 	Animals = new Vector<String>();
		Add vector elements;
		System.out.println(Animals); //Printing Vector elements
		Collections.sort(Animals, Collections.reverseOrder()); //Reversing Vector elements		 
		System.out.println(Animals);

	}

}		
Program:
 */
import java.util.Collections;
import java.util.Vector;

public class SortVectorDescending {

	public static void main(String[] args) {
		Vector<String> 	Animals = new Vector<String>();
		Animals.add("dog");
		Animals.add("cat");
		Animals.add("giraffee");
		Animals.add("zebra");
		Animals.add("lion");
		Animals.add("tiger");
		System.out.println(Animals); //Printing Vector elements
		Collections.sort(Animals, Collections.reverseOrder()); //Reversing Vector elements		 
		System.out.println(Animals);

	}

}
