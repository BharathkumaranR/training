/*
3. Compare the checked and unchecked exception.

Answer
Checked exceptions :
    
    1.Checked exceptions are the exceptions which are known to compiler. 
    2.These exceptions are checked at compile time only. 
	3.These exceptions are also called compile time exceptions and because, these exceptions will be known during compile time.
	4.java.lang.Exception is checked exception.

Example:
	SQLException, IOException, ClassNotFoundException.

Unchecked exceptions :

    1.Unchecked exceptions are those exceptions which are not at all known to compiler.
	2.These exceptions occur only at run time. 
	3.These exceptions are also called as run time exceptions.
	4.All sub classes of java.lang.RunTimeException and java.lang.Error are unchecked exceptions.

Example:
    NullPointerException, ArrayIndexOutOfBoundsException, ArithmeticException, IllegalArgumentException, NumberFormatException
*/