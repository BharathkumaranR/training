/*8)Explain the below program.
         try{
          dao.readPerson();
        } catch (SQLException sqlException) {
        throw new MyException("wrong", sqlException);
      }
      
      
Explanation:
Exception wrapping
     The method dao.readPerson() can throw an SQLException. 
     If it does, the SQLException is caught and wrapped in a MyException.*/
