/*6)Handle and give the reason for the exception in the following code: 
        Program:
             public class Exception {  
             public static void main(String[] args)
             {
  	       int arr[] ={1,2,3,4,5};
	      System.out.println(arr[7]);
            }
          }
//Display the output.
Work Breakdown Structure(WBS):
Requirement:
   -TO handle and give the reason for the exception in the following code      
    and display the output.
Entity:
  - public class ExceptionDemo
Functon declaration:
  - public static void main(String[] args)
Jobs to be done:
   1.Declare and Initialize the array value in try block.
   2.Execute the try block.
   3.When an exception occurs in try block ,then go to the catch block.
   4.Check the exception, which are known to run time.
   5.Display the output.

Correct Program:*/

public class ExceptionDemo {

	public static void main(String[] args) {
		try{
			   int arr[] ={1,2,3,4,5};
			   System.out.println(arr[7]);
			}
		        catch(ArrayIndexOutOfBoundsException e){
			   System.out.println("The specified index does not exist " +
				"in array");
			}

	}

}


/*
Output:
The specified index does not exist in array

Reason:
Array has only five elements but we are trying to display the value of 8th element.
It should throw ArrayIndexOutOfBoundsException which is a unchecked exception.
*/