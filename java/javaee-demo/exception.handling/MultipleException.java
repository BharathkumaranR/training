/*Demonstrate the catching multiple exception with example.
Work Breakdown Structure(WBS): 
 Requirements:
   -To demonstrate the catching multiple exception with example. 
 Entity:
   -public class MultipleException
 Function Declaration:
   - public static void main(String[] args)
 Jobs to be done:
   1. Declare and Initialize the array value in try block.
   2. Execute the try block.
   3. When an exception occurs in try block ,then go to the catch block.
   4. Check the exception, which are known to compile time.
   5. If two or more exception in this catch block ,then it's separated by pipe character |.
   6. Display the Exception message.  
Answer:
  * In Java 7 it was made possible to catch multiple different exceptions in the same catch block. 
  * This is also known as multi catch.
  * The two exception class names in the first catch block are separated by the pipe character |. 
  * The pipe character between exception class names are how you declare multiple exceptions to be caught by 
the same catch clause.


public class MultipleException {    
    public static void main(String[] args) {    
        try {    
            int array[] = new int[15];    
            array[7] = 22/0;    
        } catch(ArithmeticException e) {  
            System.out.println(e.getMessage());  
        } catch(ArrayIndexOutOfBoundsException e) {  
            System.out.println(e.getMessage());  
        } catch(Exception e) {  
            System.out.println(e.getMessage());  
        }    
    }    
}  
Program:
*/


public class MultipleException {

	public static void main(String[] args) {
        try {    
            int array[] = new int[15];    
            array[7] = 22/0;    
        } catch(ArithmeticException | ArrayIndexOutOfBoundsException e) {  
            System.out.println(e.getMessage());  
        } catch(Exception e) { 
            System.out.println(e.getMessage());  
        }

	}

}
