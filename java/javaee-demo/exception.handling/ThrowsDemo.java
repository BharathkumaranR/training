/*THROWS
 Work Breakdown Structure(WBS):
 1.Requirements:
   -To illustrate Throws with example. 
 2.Entity:
   -public class ThrowsDemo
 3.Function Declaration:
   - public static void main(String[] args)
 4.Jobs to be done:
    1. Creating a class ThrowsDemo and execute the try block.
    2. When an exception occurs in try block ,then go to the catch block.
    3. Check the exception, which are known to run time.
    4. Display the output.
Program
*/


public class ThrowsDemo {
	int division(int a, int b) throws ArithmeticException{  
		int c = a/b;
		return c;
	}
	
	public static void main(String[] args) {
	    ThrowsDemo obj = new ThrowsDemo();
		try{
		System.out.println(obj.division(15,0));  
		}
		catch(ArithmeticException e){
   	    System.out.println("You shouldn't divide number by zero");
		}

	}

}
