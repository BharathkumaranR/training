/*
 9. Given a path, check if path is file or directory
Work Breakdown Structure(WBS):

1.Requirements:
    - Program to check if path is file or directory
    
2.Entities:
    - FileOrDiretory
    
3.Methodsignature:
   - public static void main(String[] args)
  
4.Jobs to be done:
    1.Get the file using File class passing parameter in File class.
    2.Invoke isFile method to check path is file or directory 
       2.1)Check and Print file or directory.
    
    
Pseudo Code:

public class FileOrDiretory {
	public static void main(String[] args) {
		File file = new File("C:/Users/SUBASH R/eclipse-workspace/Javaee-demo/advance.nio.stream/samplenio.txt");
		if (file.isFile()) {
			System.out.println("The Given path is file");
		} else {
			System.out.println("The Given path is directory");
		}
		
	}
}
Program:
*/

import java.io.File;

public class FileOrDirectory {

	public static void main(String[] args) {
		File file = new File("C:/Users/SUBASH R/eclipse-workspace/Javaee-demo/advance.nio.stream/samplenio.txt");
		if (file.isFile()) {
			System.out.println("The Given path is file");
		} else {
			System.out.println("The Given path is directory");
		}

	}

}
