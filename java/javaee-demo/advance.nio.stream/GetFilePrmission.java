
/*
13. Get the permission allowed for a file
Work Breakdown Structure(WBS):

1.Requirements:
    - Program to get the permission allowed for a file.
    
2.Entities:
    - GetFilePermission
    
3.Methodsignature:
   - public static void main(String[] args)
  
4.Jobs to be done:
    1.Pass the file path to File class constructor.
    2.Check file exists or not using exists method.
    3.File exists 
           3.1)Invoke canExecute method file allowed to execute or not.
           3.2)Invoke canRead method file allowed to read or not.
           3.3)Invoke canWrite method file allowed to write or not.
    
Pseudo Code:
''''''''''''

public class GetFilePermission {
	public static void main(String[] args) {
		File file = new File(""C:/Users/SUBASH R/eclipse-workspace/Javaee-demo/advance.nio.stream/samplenio.txt");
		boolean exists = file.exists();
		if (exists == true) {
			System.out.println("Executable: " + file.canExecute());
			System.out.println("Readable: " + file.canRead());
			System.out.println("Writable: " + file.canWrite());
		} else {
			System.out.println("File not found.");
		}
	}
}
Program:
*/
import java.io.File;

public class GetFilePrmission {

	public static void main(String[] args) {
		// creating a file instance
		File file = new File("C:/Users/SUBASH R/eclipse-workspace/Javaee-demo/advance.nio.stream/samplenio.txt");

		// check if the file exists
		boolean exists = file.exists();
		if (exists == true) {
			// printing the permissions associated with the file
			System.out.println("Executable: " + file.canExecute());
			System.out.println("Readable: " + file.canRead());
			System.out.println("Writable: " + file.canWrite());
		} else {
			System.out.println("File not found.");
		}

	}

}
