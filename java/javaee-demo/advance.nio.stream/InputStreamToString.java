/*
10. InputStream to String and vice versa
Work Breakdown Structure(WBS):

1.Requirements:
    - Program to InputStream to String and vice versa.
    
2.Entities:
    - InputStreamToString
    
3.Methodsignature:
   - public static void main(String[] args)
  
4.Jobs to be done:
    1.Create FileInputStream class to read the file 
      1.1)Specify file path in argument of FileInputStream class.
    2.Create InputStreamReader class and read the file using BufferedReader class.
    3.Read file string using StringBuffer class.
    4.Invoke readLine method and read line in file.
    5.Print the string in file.
    
Pseudo Code:

public class InputStreamToString{
   public static void main(String args[]) throws IOException {
      InputStream inputStream = new FileInputStream("C:/Users/SUBASH R/eclipse-workspace/Javaee-demo/advance.nio.stream/samplenio.txt");
      InputStreamReader isReader = new InputStreamReader(inputStream);
      BufferedReader reader = new BufferedReader(isReader);
      StringBuffer sb = new StringBuffer();
      String string;
      while((string = reader.readLine())!= null){
         sb.append(string);
      }
      System.out.println(sb.toString());
   }
}
Program Code:
*/
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class InputStreamToString {

	public static void main(String[] args) throws IOException {
	      //Creating an InputStream object
	      InputStream inputStream = new FileInputStream("C:/Users/SUBASH R/eclipse-workspace/Javaee-demo/advance.nio.stream/samplenio.txt");
	      //creating an InputStreamReader object
	      InputStreamReader isReader = new InputStreamReader(inputStream);
	      //Creating a BufferedReader object
	      BufferedReader reader = new BufferedReader(isReader);
	      StringBuffer sb = new StringBuffer();
	      String string;
	      while((string = reader.readLine())!= null){
	         sb.append(string);
	      }
	      System.out.println(sb.toString());

	}

}
