/*  Create a list

    => Add 10 values in the list
    => Create another list and perform addAll() method with it
    => Find the index of some value with indexOf() and lastIndexOf()
    => Print the values in the list using 
        - For loop
        - For Each
        - Iterator
        - Stream API
    => Convert the list to a set
    => Convert the list to a array
+ Explain about contains(), subList(), retainAll() and give example
 
Word Breakdown Structure(WBS)

1.Requirements
   -To write a program to print difference of two numbers using lambda expression and the single method interface
2.Entities
   - ListDemo
3.Function Declaration
	public static void main(String[] args)
4.Jobs to be done
   1.Create a class and declare main.
   2.Inside the main creating list called employees and adding 10 employees using add() method and printing the added employees list.
   3.Creating another list of newEmployees and adding 4 newEmployees using addAll() method adding employees and newEmployees employees.
   4.Finding the index of given topic using indexOf() and lastIndexOf().
   5.Print the values in the list using
             1.In For loop getting index value and printing the value
             2.In Foreach printing the value assigning to variable topic
             3.In While loop with hasNext printing the value.
             4.Stream API using :: method reference printing the value.
   6.Converting the list into array and set.
   7.Checking the set with contain() method to set containing or not.
   8.Creating new list adding values from already created list employees using sublist(stratIndex, lastIndex) method.
   9.Finding the added values newly created list and employees list using retainAll() method.

Pseudo Code:
public class ListDemo{
    public static void main(String[] args) {
        List<String> topics = new ArrayList<String>();
        ADD 10 ELEMENTS
        List<String> newTopics = new ArrayList<String>();
        CREATE ANOTHER LIST TO ADD ELEMENTS IN LIST
        System.out.println("Index of JavaCoreTopic Classes \t" + topics.indexOf("Classes"));
        for (String topic : topics) {
            System.out.println(topic);
         }
         Iterator<String> topicsIterator = topics.iterator();
         while(topicsIterator.hasNext()) {
            System.out.println(topicsIterator.next());
         }
         topics.forEach(System.out::println);
        Set<String> topicsSet = new HashSet<>(topics); 
        String[] topicsArray = new String[topics.size()];
        System.out.println(topics.contains("Method Reference"));
        List<String> newAddedTopics = topics.subList(10, topics.size());
        System.out.println(newAddedTopics);
        System.out.println("After using retainAll method in topics list: " + 
        topics.retainAll(newAddedTopics) + "\n" + topics);
            
    }
}

Program:
*/

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
public class ListDemo {

	public static void main(String[] args) {
		
	//Create a list
	List<String> employees = new ArrayList<String>();
	//Add 10 values in the list
	employees.add("Arun");
	employees.add("Aravind");
	employees.add("Ashwin");
	employees.add("Arul");
	employees.add("Bhuvi");
	employees.add("Bhuvanesh");
	employees.add("Clement");
	employees.add("Manoj");
	employees.add("Mathew");
	employees.add("Naveen");
	
	System.out.println("Java Core employees:\n" + employees);
	
	//Create another list and perform addAll() method with it
	List<String> newemployees = new ArrayList<String>();
	newemployees.add("Saran");
	newemployees.add("Tharun");
	newemployees.add("Yogesh");
	employees.addAll(newemployees);
	System.out.println("Java Core employees:\n"+employees);
	
	//Find the index of some value with indexOf() and lastIndexOf()
	System.out.println("Index of JavaCoreEmployee Classes \t" + employees.indexOf("Manoj"));
	System.out.println("Last index of JavaCoreEmployee Variables\t" + employees.lastIndexOf("Naveen"));
	    
	//Print the values in the list using 
	//Using For loop
	System.out.println("For loop");
    for (int index = 0; index < employees.size(); index++) {
        System.out.println(employees.get(index));
    }

    //Using For each
    System.out.println("Foreach");
    for (String topic : employees) {
        System.out.println(topic);
    }

    //Using While loop
    System.out.println("while loop");
    Iterator<String> employeesIterator = employees.iterator();
    while(employeesIterator.hasNext()) {
        System.out.println(employeesIterator.next());
    }

    //Using Stream API's forEach
    System.out.println("Stream API");
    employees.forEach(System.out::println);

    //Convert the list to a set
    Set<String> employeesSet = new HashSet<>(employees);
    System.out.println(employeesSet);

    //Convert the list to a array
    String[] employeesArray = new String[employees.size()];
    System.out.println(employeesArray);
    
    //contains()
    System.out.println(employees.contains("Method Reference"));

    //subList(startIndex, lastIndex)
    List<String> newAddedemployees = employees.subList(10, employees.size());
    System.out.println(newAddedemployees);

    //retainAll()
    System.out.println("After using retainAll method in employees list: " + 
    employees.retainAll(newAddedemployees) + "\n" + employees);

	}

}
