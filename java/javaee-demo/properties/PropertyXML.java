/*
Properties:

2) Write a program to add 5 elements to a xml file and print the elements in the xml file 
using list. Remove the 3rd element from the xml file and print the xml file.
      
Work Breakdown Structure(WBS):

1.Requirement :
    Program to add 5 elements to a xml file and print the elements in the xml file 
using list. Remove the 3rd element from the xml file and print the xml file.
2.Entity :
    PropertyXml
3.Function Declaration :
    public static void main(String[] args)
    public static List<String> toList(Set<Object> set) 
4.Jobs to be done :
    1)Create a object for Properties class and Set some properties to that object.
    2)Create a new XML file
    3)Store the properties to XML file
    4)Create a new object for Properties class
    5)Load the data from XML file to object
    6)Remove a specified property from properties
    7)Display all the properties in new object
5.Pseudocode :

public class PropertyXml {

	public static List<String> toList(Set<Object> set) {
        List<String> list = new ArrayList<>();
        for (Object element : set) {
            list.add((String) element);
        } return list;
    }
	
	public static void main(String[] args) throws IOException {
		
		Properties properties = new Properties();
        properties.setProperty("path", "Javaee-demo\\properties");
        properties.setProperty("project", "Javaee-demo");
        properties.setProperty("password", "5555");
        properties.setProperty("resource", "https://jenkov.com");
        properties.setProperty("username", "Jenkov");

        //create a XML file
        FileOutputStream newFile = new FileOutputStream(
        		"C:\\Users\\SUBASH R\\eclipse-workspace\\javaee-demo\\properties\\Info.xml");

        //store properties to xml file
        properties.storeToXML(newFile, "Information about the project path");

        //import a XML file
        FileInputStream existingFile = new FileInputStream(
        		"C:\\\\Users\\\\SUBASH R\\\\eclipse-workspace\\\\javaee-demo\\\\properties\\\\Info.xml");

        //creating a newProperties object of Property class
        Properties newProperties = new Properties();

        //loading properties from XML file to newProperties object
        newProperties.loadFromXML(existingFile);

        //converting set to list
        List<String> keysList = toList(newProperties.keySet());

        for (String key : keysList) {
            System.out.println(key + " -> " + newProperties.getProperty(key));
        }

        //removing a key from keysList
        keysList.remove("password");

        System.out.println();

        for (String key : keysList) {
            System.out.println(key + " -> " + newProperties.getProperty(key));
        }

	}

}

*/

import java.util.List;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Set;

public class PropertyXML {
	public static List<String> toList(Set<Object> set) {
        List<String> list = new ArrayList<>();
        for (Object element : set) {
            list.add((String) element);
        } return list;
    }
	
	public static void main(String[] args) throws IOException {
		
		Properties properties = new Properties();
        properties.setProperty("path", "Javaee-demo\\properties");
        properties.setProperty("project", "Javaee-demo");
        properties.setProperty("password", "5555");
        properties.setProperty("resource", "https://jenkov.com");
        properties.setProperty("username", "Jenkov");

        //create a XML file
        FileOutputStream newFile = new FileOutputStream(
        		"C:\\Users\\SUBASH R\\eclipse-workspace\\javaee-demo\\properties\\Info.xml");

        //store properties to xml file
        properties.storeToXML(newFile, "Information about the project path");

        //import a XML file
        FileInputStream existingFile = new FileInputStream(
        		"C:\\\\Users\\\\SUBASH R\\\\eclipse-workspace\\\\javaee-demo\\\\properties\\\\Info.xml");

        //creating a newProperties object of Property class
        Properties newProperties = new Properties();

        //loading properties from XML file to newProperties object
        newProperties.loadFromXML(existingFile);

        //converting set to list
        List<String> keysList = toList(newProperties.keySet());

        for (String key : keysList) {
            System.out.println(key + " -> " + newProperties.getProperty(key));
        }

        //removing a key from keysList
        keysList.remove("password");

        System.out.println();

        for (String key : keysList) {
            System.out.println(key + " -> " + newProperties.getProperty(key));
        }

	}

}
