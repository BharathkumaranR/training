/*
Properties:

1) Write a program to perform the following operations in Properties.
        i) add some elements to the properties file.
        ii) print all the elements in the properties file using iterator.
        iii) print all the elements in the properties file using list method.
      
Work Breakdown Structure(WBS):

1.Requirement :
    Program to perform the following operations in Properties.
        i) add some elements to the properties file.
        ii) print all the elements in the properties file using iterator.
        iii) print all the elements in the properties file using list method.
2.Entity :
   PropertyDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Create a object for Properties class.
    2)Set some properties to object.
    3)Display all property using Iterator object with keySet.
    4)Display all property in entrySet using Stream.

Pseudocode :

public class PropertyDemo {

	public static void main(String[] args) {
		
		Properties properties = new Properties();

        properties.setProperty("user-name", "Bharath Kumaran");
        properties.setProperty("email", "19ect01@kpriet.ac.in");
        properties.setProperty("password", "96776");

        //Using Iterator to display key and values on properties
        Iterator<?> propertiesIterator = properties.keySet().iterator();
        while (propertiesIterator.hasNext()) {
            String key = (String) propertiesIterator.next();
            System.out.println(key + " : " + properties.get(key));
        }

        //Using entrySet method of HashTable to get the key value pair
        //using streams forEach to display key and values on properties
        properties.entrySet().stream().forEach(System.out::println);

	}

}
*/

import java.util.Iterator;
import java.util.Properties;

public class PropertyDemo {

	public static void main(String[] args) {
		
		Properties properties = new Properties();

        properties.setProperty("user-name", "Bharath Kumaran");
        properties.setProperty("email", "19ect01@gmail.com");
        properties.setProperty("password", "96776");

        //Using Iterator to display key and values on properties
        Iterator<?> propertiesIterator = properties.keySet().iterator();
        while (propertiesIterator.hasNext()) {
            String key = (String) propertiesIterator.next();
            System.out.println(key + " : " + properties.get(key));
        }

        //Using entrySet method of HashTable to get the key value pair
        //using streams forEach to display key and values on properties
        properties.entrySet().stream().forEach(System.out::println);

	}

}
