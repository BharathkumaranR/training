/*
6.Create a program using HttpUrlconnection in networking.
Work Breakdown Structure(WBS):

1.Requirements:
    ->To create a program using HttpURLConnection in networking.
    
2.Entities:
    ->public class HttpUrlConnection
    
3.Methodsignature:
    ->public static void main(String[] args)
  
4.Jobs to be done:
    1.Create a class calledHttpUrlConnection.
    2.Try catch 
        2.1)Create a url path and set the  path name.
        2.2)Create a object http and openConnection method is used to open the connection.
        2.2)Using forloop to iterate the values and print.
        2.4)Disconnect method to disconnect the connection.
    4.Print the result.

Pseudo Code:
''''''''''''
public class HttpUrlConnection {
	public static void main(String[] args) {
		try {
			URL url = new URL("https://matlab.mathworks.com/");
			HttpURLConnection http = (HttpURLConnection) url.openConnection();
			for (int i = 1; i <= 8; i++) {
				System.out.println(http.getHeaderFieldKey(i) + " = " + http.getHeaderField(i));
			}
			huc.disconnect();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
Program:
*/

import java.net.HttpURLConnection;
import java.net.URL;

public class HttpUrlConnection {

	public static void main(String[] args) {
		try {
			URL url = new URL("https://matlab.mathworks.com/");
			HttpURLConnection http = (HttpURLConnection) url.openConnection();
			for (int i = 1; i <= 8; i++) {
				System.out.println(http.getHeaderFieldKey(i) + " = " + http.getHeaderField(i));
			}
			http.disconnect();
		} catch (Exception e) {
			System.out.println(e);
		}
		
	}

}
