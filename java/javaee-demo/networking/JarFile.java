/*
3.Create an jar file inside eclipse.
 
1.Requirement:
     ->To create a jar file  
2.Steps to follow to create:
     1.Either from the context menu or from the menu bar's File menu, select Export
     2.Expand the Java node and select JAR file. Click Next.
     3.In the JAR File Specification page, select the resources that you want to export 
in the Select the resources to export field.
     4.Select the appropriate checkbox to specify whether you want to Export generated 
class files and resources or Export Java source files and resources.
     5.In the Select the export destination field, either type or click Browse to select 
a location for the JAR file.
     6.Select or clear the Compress the contents of the JAR file checkbox.
     7.Select or clear the Overwrite existing files without warning checkbox. 
If you clear this checkbox, then you will be prompted to confirm the replacement of each
file that will be overwritten.
     8.You have two options:
	  Click Finish to create the JAR file immediately.
	  Click Next to use the JAR Packaging Options page to set advanced options, create a
JAR description, or change the default manifest.
     9.Go ahead and click finish.
     10.Now, navigate to the location you specified for the jar. The icon you see and the 
behavior you get if you double click it will vary depending on how your computer is set up.
*/