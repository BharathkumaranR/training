/*
2.Write a code to rounding the value to maximum of 3 digits by setting maximum and minimum Fraction digits.
Work Breakdown Structure(WBS):
 
1.Requirement:
     ->to write a program for rounding the value to maximum of 3 digits by setting maximum and minimum Fraction digits.
     
2.Entity:
     ->public class NumberFraction
     
3.Function Declaration:
  	 ->public static void main(String[] args)
  	 
4.Jobs To Be Done:
  	1.Invoke NumberFormat class getInstance method and store it in numberFormat.
  	2.Set Minimun and Maximun fraction digits using
              *) setMinimumFractionDigits and
              *) setMaximumFractionDigits method.
    3.Invoke format method with float value to print. 
    
PseudoCode:

import java.net.*;

public class NumberFraction {
	public static void main(String[] args) {
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setMinimumFractionDigits(1);
		numberFormat.setMaximumFractionDigits(3);
		System.out.println(numberFormat.format(21.3243f));
	}
}
Program:
*/ 

import java.text.NumberFormat;

public class NumberFraction {

	public static void main(String[] args) {
		NumberFormat numberFormat = NumberFormat.getInstance();
		//Set Minimum fraction digits
		numberFormat.setMinimumFractionDigits(1);
		//Set Maximum fraction digits
		numberFormat.setMaximumFractionDigits(3);
		System.out.println(numberFormat.format(21.2134f));

	}

}
