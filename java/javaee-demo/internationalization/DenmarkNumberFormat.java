/*
1.Write a code to change the number format to Denmark number format .
Work Breakdown Structure(WBS):
 
1.Requirement:
     ->Towrite a program to change the number format to Denmark number format.
     
2.Entity:
     ->public class DenmarkNumberFormat
     
3.Function Declaration:
  	 ->public static void main(String[] args)
  	 
4.Jobs To Be Done:
  	1.Invoke the NumberFormat class getInstance method parameter as 
create Locale class with argument Denmark number format.
    2.Print the Denmark number format.
    
PseudoCode:

import java.net.*;
public class DenmarkNumberFormat {
	public static void main(String[] args) {
		System.out.println(NumberFormat.getInstance(new Locale("Denmark"))
				                       .format(32124));
	}
}
Program:
*/ 
 
import java.text.NumberFormat;
import java.util.Locale;

public class DenmarkNumberFormat {

	public static void main(String[] args) {
		System.out.println("Denmark Number Foramt:-");
		System.out.println(NumberFormat.getInstance(new Locale("Denmark"))
				                       .format(32124));

	}

}
