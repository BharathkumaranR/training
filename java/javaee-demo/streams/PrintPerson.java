/*
8. Print all the persons in the roster using java.util.Stream<T>#forEach 

Work Breakdown Structure(WBS):

1.Requirement:
	-To print all the persons in the roster using java.util.Stream<T>#forEach

2.Entity:
    -public class PrintPerson

3.Function declaration
	- public static void main(String[] args)

4.Jobs to be done:
    1.Create a class named PrintPerson.
    2.Declare main method and the method returns a list.
    3.Store the returned list to a new list.
    4.Stream API's forEach used to access the printPerson method of all objects.

Pseudocode:
public class PrintPerson {

	public static void main(String[] args) {
        List<Person> personList = Person.createRoster();
        personList.forEach(Person::printPerson);

	}

}
Program:
*/

import java.util.List;

public class PrintPerson {

	public static void main(String[] args) {
    	// forEach accepts the printPerson method of Person class 
        //as Method reference as parameter 
        List<Person> personList = Person.createRoster();
        personList.forEach(Person::printPerson);

	}

}
