/*
4. Consider a following code snippet:
    -> List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
    -> Find the sum of all the numbers in the list using java.util.Stream API
    -> Find the maximum of all the numbers in the list using java.util.Stream API
    -> Find the minimum of all the numbers in the list using java.util.Stream API

Work Breakdown Structure(WBS):

1.Requirement:
    -> To illustrate considering a following code snippet:
    -> List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
    -> Find the sum of all the numbers in the list using java.util.Stream API
    -> Find the maximum of all the numbers in the list using java.util.Stream API
    -> Find the minimum of all the numbers in the list using java.util.Stream API

2.Entity:
	-> public class MaxMinSum

3.Function declaration
	-> public static void main(String[] args)

4.Jobs to be done:
     1.Create a class as MaxMinSum and delcare main method.
     2.Create a double ended queue and add a element at first.
     3.Add a element at last and remove element present at first.
     4.Remove element present at last and get the peek first element.
     5.Get the peek last element.
     6.Poll the first element in dequeue and poll the last element in dequeue.

Pseudocode: .
	public static void main(String[] args) {
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 25, 78);
        int sum = randomNumbers.stream().mapToInt(Integer::intValue).sum();
        int maxInList = randomNumbers.stream()
                .mapToInt(Integer::intValue).max().getAsInt();
        int minInList = randomNumbers.stream()
                .mapToInt(Integer::intValue).min().getAsInt();

        System.out.println("Sum: " + sum);
        System.out.println("Minimum in list: " + minInList);
        System.out.println("Maximum in list: " + maxInList);

	}

}
Program:
*/


import java.util.Arrays;
import java.util.List;

public class MaxMinSum {

	public static void main(String[] args) {
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 25, 78);
        // list of numbers

        int sum = randomNumbers.stream().mapToInt(Integer::intValue).sum();
        /* stream method used to get the desired result with the pipelined methods
         * mapToInt method helps to convert into integer values
         * sum method sums the integer values and return int value.
         */

        int maxInList = randomNumbers.stream()
                .mapToInt(Integer::intValue).max().getAsInt();
        // max method returns maximum element from stream
        // getAsInt method used to change the type to int from optionalInt

        int minInList = randomNumbers.stream()
                .mapToInt(Integer::intValue).min().getAsInt();
        // max method returns maximum element from stream
        // getAsInt method used to change the type to int from optionalInt

        System.out.println("Sum: " + sum);
        System.out.println("Minimum in list: " + minInList);
        System.out.println("Maximum in list: " + maxInList);

	}

}
