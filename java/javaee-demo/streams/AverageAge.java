/*
6. Write a program to find the average age of all the Person in the person List

Work Breakdown Structure(WBS):

1.Requirement:
   -To write a program to find the average age of all the Person in the person List

2.Entity:
   -public class AverageAge

3.Function declaration
   - public static void main(String[] args)

4.Jobs to be done:
   1.Create a class named as AverageAgePerson.
   2.Declare main method and the method returns a list.
   3.Store the returned list to a new list.
   4.Stream method to get sequential elements in List.
   5.Using average() method used to find the average of elements.

Pseudocode:
import java.util.List;
public class AverageAge {

	public static void main(String[] args) {
        List<Person> personList = Person.createRoster();
        double age = personList.stream()
        		               .mapToDouble(Person::getAge)
        		               .average() //average() method calculates the average for sequential elements
        		               .getAsDouble();
        System.out.println("Average age of all persons: " + age);

	}

}
Program:
*/

import java.util.List;
public class AverageAge {

	public static void main(String[] args) {
    	//Stream method used to pipeline various methods
        //With the help of getAge gets age of all objects
        List<Person> personList = Person.createRoster();
        double age = personList.stream()
        		               .mapToDouble(Person::getAge)
        		               .average() //average() method calculates the average for sequential elements
        		               .getAsDouble(); //getAsDouble() method converts and return Optional double to double
     
       
        System.out.println("Average age of all persons: " + age);

	}

}
