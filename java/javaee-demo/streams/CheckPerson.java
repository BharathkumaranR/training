/*
13. Consider the following Person:
            new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
       - Check if the above person is in the roster list obtained 
    from Person class.

Work Breakdown Structure(WBS):

1.Requirement:
    -> To check if the above person is in the roster list obtained from Person class.

2.Entity:
    -> public class CheckPersonInList

3.Function declaration
	-> public static void main(String[] args)
	-> public static boolean contains(List<Person> list, Person o)

4.Jobs to be done:
    1).Create createRoster static method using the class, the method returns a list.
    2).Store the returned list to a new list.
    3).Store a object of Person class to a variable of that type and declare main method.
    4).Check the object stored on a variable is present in list.

Pseudocode:
public class CheckPerson {

    public static boolean contains(List<Person> list, Person o) {
        for (Person person : list) {
            if (person.name.equals(o.name) &&
                    person.gender.equals(o.gender) &&
                    person.emailAddress.equals(o.emailAddress) &&
                    person.birthday.equals(o.birthday)) {
                return true;
            }
        }
        return false;
    }
	public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        Person aPerson = new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com");

        System.out.println("Check person is in the roster: " + contains(roster, aPerson));

	}

}    
Program:
*/


import java.time.chrono.IsoChronology;
import java.util.List;

public class CheckPerson {

    public static boolean contains(List<Person> list, Person o) {
        for (Person person : list) {
            if (person.name.equals(o.name) &&
                    person.gender.equals(o.gender) &&
                    person.emailAddress.equals(o.emailAddress) &&
                    person.birthday.equals(o.birthday)) {
                return true;
            }
        }
        return false;
    }
    
	public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        Person aPerson = new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com");

        System.out.println("Check person is in the roster: " + contains(roster, aPerson));

	}

}
