/*
9) Sort the roster list based on the person's age in descending order
using comparator

Work Breakdown Structure(WBS):

1.Requirement:
    -To sort the roster list based on the person's age in descending order using comparator

2.Entity:
    -public class SortOrderUsingComparator

3.Function declaration
    - public static void main(String[] args)

4.Jobs to be done:
    1.Create a class named SortOrderUsingComparator.
    2.Declare main method the method returns a list.
    3.Store the returned list to a new list.
    4.Sort the list based on the person age in descending order.
    5.Iterating the list to print person details.

Pseudocode:
public class SortOrderUsingComparotor {

	public static void main(String[] args) {
        List<Person> personList = Person.createRoster();
        personList.sort(Comparator.comparing(Person::getAge).reversed());
        for (Person person : personList) {
            person.printPerson();
        }

	}

}

Program:
*/


import java.util.Comparator;
import java.util.List;

public class SortOrderUsingComparotor {

	public static void main(String[] args) {
        List<Person> personList = Person.createRoster();
        personList.sort(Comparator.comparing(Person::getAge).reversed());
        // sorting the list using Comparator methods
        for (Person person : personList) {
            person.printPerson();
        }

	}

}
