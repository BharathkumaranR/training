/*
12. Consider the following code snippet:
            List<Person> newRoster = new ArrayList<>();
            newRoster.add(
                new Person(
                "John",
                IsoChronology.INSTANCE.date(1980, 6, 20),
                Person.Sex.MALE,
                "john@example.com"));
            newRoster.add(
                new Person(
                "Jade",
                IsoChronology.INSTANCE.date(1990, 7, 15),
                Person.Sex.FEMALE, "jade@example.com"));
            newRoster.add(
                new Person(
                "Donald",
                IsoChronology.INSTANCE.date(1991, 8, 13),
                Person.Sex.MALE, "donald@example.com"));
            newRoster.add(
                new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
    - Create the roster from the Person class and add each person in the
        newRoster to the existing list and print the new roster List.
    - Print the number of persons in roster List after the above addition.
    - Remove the all the person in the roster list

Work Breakdown Structure(WBS):

1.Requirement:
	-> Create the roster from the Person class and add each person in the newRoster to the existing list and print the new roster List.
	-> Print the number of persons in roster List after the above addition.
	-> Remove the all the person in the roster list

2.Entity:
	-> public class NewRosterList

3.Function declaration
	- public static void main(String[] args)

4.Jobs to be done:
     1.Create a class as NewRosterList and declare main method.
     2.Create the createRoster static method using the class, the method returns a list
     3.Store the returned list to a new list 'roster' and creating a new roster list 
     4.Adding new objects to it and add the objects of new roster list to roster list.
     5.Print the elements of new roster list and display the size of the roster list
     6.To clear all the elements in roster list.
Pseudocode:
public class NewRosterList {

	public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        List<Person> newRoster = new ArrayList<>();
        newRoster.forEach(roster::add);
        // adding the objects(elements) in newRoster to roster list
        newRoster.forEach(Person::printPerson);
        // printing the persons in newRoster list
        System.out.println("Number of persons in roster list: " + roster.size());
        // printing the number of persons in list
        roster.clear();
        // removing all the elements of roster list
        System.out.println("Number of persons in roster list: " + roster.size());

	}

}
Program:
*/

import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;

public class NewRosterList {

	public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        List<Person> newRoster = new ArrayList<>();
        newRoster.add(
            new Person(
            "John",
            IsoChronology.INSTANCE.date(1980, 6, 20),
            Person.Sex.MALE,
            "john@example.com"));
        newRoster.add(
            new Person(
            "Jade",
            IsoChronology.INSTANCE.date(1990, 7, 15),
            Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add(
            new Person(
            "Donald",
            IsoChronology.INSTANCE.date(1991, 8, 13),
            Person.Sex.MALE, "donald@example.com"));
        newRoster.add(
            new Person(
            "Bob",
            IsoChronology.INSTANCE.date(2000, 9, 12),
            Person.Sex.MALE, "bob@example.com"));

        newRoster.forEach(roster::add);
        // adding the objects(elements) in newRoster to roster list
        newRoster.forEach(Person::printPerson);
        // printing the persons in newRoster list
        System.out.println("Number of persons in roster list: " + roster.size());
        // printing the number of persons in list
        roster.clear();
        // removing all the elements of roster list
        System.out.println("Number of persons in roster list: " + roster.size());

	}

}
