    /*
7. Consider a following code snippet:
     List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
   - Get the non-duplicate values from the above list using java.util.Stream API

Work Breakdown Structure(WBS):

1.Requirement:
    Consider a following code snippet:
      - List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
      - Get the non-duplicate values from the above list using java.util.Stream API

2.Entity:
    - public class NonDuplicate

3.Function declaration
    - public static void main(String[] args)

4.Jobs to be done:
     1.Create a class as NonDuplicate and declare main method.
     2.Get the list of random numbers.
     3.Using filter to filter the elements only which have no repeated.
     4.Display the filtered elements.
Pseudocode
public class NonDuplicate {

	public static void main(String[] args) {

	        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
	        randomNumbers.stream()
	        .filter(o -> Collections.frequency(randomNumbers, o) == 1)
	        .collect(Collectors.toList())
	        .forEach(System.out::println);

	}

}    
Program:
*/


import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class NonDuplicate {

	public static void main(String[] args) {

	        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
	        randomNumbers.stream()
	        .filter(o -> Collections.frequency(randomNumbers, o) == 1)
	        .collect(Collectors.toList())
	        .forEach(System.out::println);

	}

}
