/*
3. Write a program to filter the Person, who are male and
        - find the first person from the filtered persons
        - find the last person from the filtered persons
        - find random person from the filtered persons

Work Breakdown Structure(WBS):

1.Requirement:

   -To write a program to filter the Person, who are male and
        - find the first person from the filtered persons
        - find the last person from the filtered persons
        - find random person from the filtered persons

2.Entity:
   -public class StreamFilterDemo

3.Function declaration
   - public static void main(String[] args)

4.Jobs to be done:
    1.Create a class named StreamFilterDemo
    2.Declare main method and the method returns a list
    3.Store the returned list to a new list
    4.Filter the likkst using stream API's filter method
    5.Get first person from the filtered persons
    6.Get last person from the filtered persons and get random person from the filtered persons

Pseudocode:
public class StreamFilterDemo {

	public static void main(String[] args) {
        List<Person> rosterList = Person.createRoster();
        List<Person> maleRosterList = rosterList.stream()
                .filter(aPerson -> aPerson.gender == Person.Sex.MALE)
                .collect(Collectors.toList());
        maleRosterList.get(0).printPerson();
        //getting the first person in the male list

        maleRosterList.get(maleRosterList.size() - 1).printPerson();
        //getting the last person in the male list

        Random random = new Random();
        maleRosterList.get(random.nextInt(maleRosterList.size())).printPerson();
    }
}        
Program:

*/



import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class StreamFilterDemo {

	public static void main(String[] args) {
        List<Person> rosterList = Person.createRoster();
        //creating a roster  of persons

        List<Person> maleRosterList = rosterList.stream()
                .filter(aPerson -> aPerson.gender == Person.Sex.MALE)
                .collect(Collectors.toList());
        // Stream API filter getting the persons who are male.

        maleRosterList.get(0).printPerson();
        //getting the first person in the male list

        maleRosterList.get(maleRosterList.size() - 1).printPerson();
        //getting the last person in the male list

        Random random = new Random();
        maleRosterList.get(random.nextInt(maleRosterList.size())).printPerson();
        //using the object of Random class getting a random number given the range
        //as list size
        //the printPerson() method prints the person name along with his age.

	}

}
