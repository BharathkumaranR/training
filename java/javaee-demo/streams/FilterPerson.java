/*
1) Write a program to filter the Person, who are male and age greater than 21
Work Breakdown Structure(WBS):

1.Requirement:
   -To write a program to filter the Person, who are male and age greater than 21
2.Entity:
   - public class FilterPerson
3.Function declaration
   - public static void main(String[] args)
4.Jobs to be done:
    1.Create a class named as FilterPerson.
    2.Declare createRoster static method using the class, the method returns a list
    3.Store the returned list and iterating the rosterList using for loop
    4.Condition to check the person male and his age greater than 21
        4.i)If satisfied print the person name and age.
Pseudocode:
public class FilterPerson {

	public static void main(String[] args) {
        
		List<Person> rosterList = Person.createRoster();
        //Storing the returned list into rosterList
        for (Person aPerson : rosterList) {        
            if ((aPerson.getGender() == Person.Sex.MALE) && (aPerson.getAge() > 21)) {
                aPerson.printPerson();
            
            }
            
        }

	}

}
Program:
*/

import java.util.List;

public class FilterPerson {

	public static void main(String[] args) {
        
		List<Person> rosterList = Person.createRoster();
        //Storing the returned list into rosterList
        for (Person aPerson : rosterList) {
        	// Iterating all persons in list and accessing age and gender to check
            if ((aPerson.getGender() == Person.Sex.MALE) && (aPerson.getAge() > 21)) {
                aPerson.printPerson();
            
            }
            
        }

	}

}
