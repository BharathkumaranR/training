/*
10.Iterate the roster list in Persons class and and print the person
without using forLoop/Stream 

Work Breakdown Structure(WBS):

1.Requirement:
   -To iterate the roster list in Persons class and and print the person without using forLoop/Stream 
2.Entity:
  -public class PersonIterator
3.Function declaration
  - public static void main(String[] args)

4.Jobs to be done:
   1.Create a class named as PersonIterator.
   2.Declare main method and the method returns a list
   3.Store the returned list to a new list
   4.Using iterator to print person details

Pseudocode:
public class PersonIterator {

	public static void main(String[] args) {
        List<Person> personList = Person.createRoster();
        Iterator<Person> iterator = personList.iterator();
        // creating a object of iterator
        while (iterator.hasNext()) {
            iterator.next().printPerson();
            // next returns object of person
            //printPerson() prints the person name and age
        }

	}

}
Program:
*/


import java.util.Iterator;
import java.util.List;

public class PersonIterator {

	public static void main(String[] args) {
        List<Person> personList = Person.createRoster();
        Iterator<Person> iterator = personList.iterator();
        // creating a object of iterator
        while (iterator.hasNext()) {
            iterator.next().printPerson();
            // next returns object of person
            //printPerson() prints the person name and age
        }

	}

}
