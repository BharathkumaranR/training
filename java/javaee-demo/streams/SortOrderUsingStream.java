/*
11. Sort the roster list based on the person's age in descending order using
java.util.Stream

Work Breakdown Structure(WBS):
1.Requirement:
    -To sort the roster list based on the person's age in descending order using java.util.Stream

2.Entity:
	-public class SortOrderUsingStream

3.Function declaration
    - public static void main(String[] args)

4.Jobs to be done:
    1.Create a class named SortOrderUsingStream.
    2.Declare main method and the method returns a list.
    2.Store the returned list to a new list.
    3.Sort the list based on the person age in descending order.
    4.Iterating the list to print person details.

Pseudocode:
public class SortOrderUsingStream {

	public static void main(String[] args) {
        List<Person> personList = Person.createRoster();
        personList.stream().sorted(Person::compareByAge)
            .forEach(Person::printPerson);
	}

}        
Program:
*/
import java.util.List;

public class SortOrderUsingStream {

	public static void main(String[] args) {
    	// stream() method can pipeline many different methods to get desired
        // results in single line of code
        List<Person> personList = Person.createRoster();
        personList.stream().sorted(Person::compareByAge)
            .forEach(Person::printPerson);
        
        // sorted() method accepts comparator here compareByAge method of person
        // class is passed
        // finally forEach to print person details

	}

}
