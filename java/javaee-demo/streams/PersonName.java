/*
2. Write a program to print minimal person with name and email address from the
Person class using java.util.Stream<T>#map API
Work Breakdown Structure(WBS):
1.Requirement:
  -To write a program to print minimal person with name and email address from the
   Person class using java.util.Stream<T>#map API

2.Entity:
  -public class PersonName

3.Function declaration
  - public static void main(String[] args)

4.Jobs to be done:
   1.Create class named as PersonName.
   2.Call the createRoster static method using the class, the method returns a list
   3.Store the returned list to a new list
   4.Iterate the list and get the name and mail
   5.Put email as key and name as value to map
   6.Displaying the map elements using forEash method in stream

Pseudocode:   
public class PersonName {
	

	public static void main(String[] args) {
        List<Person> rosterList = Person.createRoster();

        Map<String, String> mapDetails = new HashMap<>();

        for (Person roster : rosterList) {
            mapDetails.put(roster.emailAddress, roster.name);
        }
        Map<String, String> rosterMap = rosterList.stream()
                .collect(Collectors.toMap(Person::getEmailAddress, Person::getName));
        rosterMap.entrySet().forEach(System.out::println);
	}

}
Program:

*/

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PersonName {
	

	public static void main(String[] args) {
        List<Person> rosterList = Person.createRoster();

        Map<String, String> mapDetails = new HashMap<>();

        for (Person roster : rosterList) {
            mapDetails.put(roster.emailAddress, roster.name);
        }

        //Another way to do the same
        Map<String, String> rosterMap = rosterList.stream()
                .collect(Collectors.toMap(Person::getEmailAddress, Person::getName));
        // Using stream() method the collections of objects parallely processed
        // The collect method act to each and every object.
       
        rosterMap.entrySet().forEach(System.out::println);
        // the toMap method of Collectors class maps the two functions which 
        // return string values.

	}

}
