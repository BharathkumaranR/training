/*
 5.Demonstrate java program to working of map interface using( put(), remove(), booleanContainsValue(), replace() ) .

Work Breakdown Structure(WBS):
1.Requirements:
   -To demonstrate a Java program to working of map interface using( put(), remove(), booleanContainsValue(), replace()).
2.Entities
   - MapInterface
3.Function Declaration
   - public static void main(String[] args)
4.Jobs to be done
   1.Create a Class name TreeSetDemo and declare main method.
   2.Create a TreeMap as players.
   3.Use put() to add elements, remove() to remove element, booleanContainsValue() to check element, replace() methods to replace element 
   4.Print the players. 
   
Pseudocode:
import java.util.TreeMap;
public class MapInterface {

	public static void main(String[] args) {
	      TreeMap<Integer, String> players = new TreeMap<Integer, String>();   
          Add players
          Remove and replace players
	      System.out.println(players.containsValue("Bahir")); //Using containsValue() method
		  System.out.println(players);

	}

}

Program:
*/


import java.util.TreeMap;
public class MapInterface {

	public static void main(String[] args) {
	      TreeMap<Integer, String> players = new TreeMap<Integer, String>();  //Creating TreeMap and SortedMap
	      players.put(2, "Kohli");   //Using put() method
	      players.put(3, "Dhoni");
	      players.put(5, "Bahir");
	      players.put(1,  "Andrew");
		  System.out.println(players);
	      players.remove(3, "Dhoni");  //Using remove() method
	      players.replace(1,"Raina");  //Using replace() method
	      System.out.println(players.containsValue("Bahir")); //Using containsValue() method
		  System.out.println(players);

	}

}
