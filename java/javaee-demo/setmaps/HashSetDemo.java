/*1.Java program to demonstrate adding elements, displaying, removing, and iterating in hash set

Work Breakdown Structure(WBS):
1.Requirements:
    -To write a Java program to demonstrate adding elements, displaying, removing, and iterating in hash set
2.Entities
   - public class HashSetDemo
3.Function Declaration
   - public static void main(String [] args)
4.Jobs to be done
 1.Create a Class named HashSetDemo and declare main method.
 2.Create a set as players and add 5 values and remove on value from set.
 3.Display all the set elements using Iterator 

Pseudocode:
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
public class HashSetDemo {

	public static void main(String[] args) {
		Set<String> players = new HashSet<>();
		Add 5 players
		Remove a player
        Iterator<String> player = players.iterator();  //Printing all the set elements using Iterator 
        System.out.println("Printing all the set elements using Iterator :");
        while (player.hasNext()) {
            System.out.println(player.next());
        } 

	}

}		
Program:
*/

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
public class HashSetDemo {

	public static void main(String[] args) {
		Set<String> players = new HashSet<>();
        players.add("Andrew"); 	//Add 5 values in the set
	    players.add("Kohli");
	    players.add("Dhoni");
	    players.add("Rashid");
	    players.add("Bahir");
	    players.remove("Andrew");//Remove a value in set
        Iterator<String> player = players.iterator();  //Printing all the set elements using Iterator 
        System.out.println("Printing all the set elements using Iterator :");
        while (player.hasNext()) {
            System.out.println(player.next());
        } 

	}

}
