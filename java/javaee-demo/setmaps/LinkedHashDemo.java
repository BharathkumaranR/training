/*2.Demonstrate program explaining basic add and traversal operation of linked hash set

Work Breakdown Structure(WBS):
1.Requirements:
   -To demonstrate a program explaining basic add and traversal operation of linked hash set
2.Entities
    -public class LinkedHashSetDemo
3.Function Declaration
   - public static void main(String [] args)
4.Jobs to be done
 1.Create a Class named LinkedHashSetDemo and declare main method.
 2.Create a set as cars and add 6 values and remove on value from set.
 3.Create iterator to traverse over cars.
 4.Print all the set elements using Iterator with hasnext() method.
 
Pseudocode:
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
public class LinkedHashDemo {

	public static void main(String[] args) {
        Set<String> players = new LinkedHashSet<>();
        Add 6 players
	    Iterator<String> setIterator = players.iterator();
        while(setIterator.hasNext()){

             System.out.println(setIterator.next());

          }  		

	}

}        
 
 Program:
 */
 
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
public class LinkedHashDemo {

	public static void main(String[] args) {
        Set<String> players = new LinkedHashSet<>();	//Creating a LinkedHashSet 
	    players.add("Andrew");     //Adding 6 Values
	    players.add("Kohli");
	    players.add("Dhoni");
	    players.add("Rashid");   
	    players.add("Bahir");
	    players.add("Raina");  
        //creating iterator to traverse over players
	    Iterator<String> setIterator = players.iterator();
        while(setIterator.hasNext()){

             System.out.println(setIterator.next());

          }  		

	}

}
