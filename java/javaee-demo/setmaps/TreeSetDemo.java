/*
 4.Java program to demonstrate insertions and string buffer in tree set.

Work Breakdown Structure(WBS):
1.Requirements:
   -To write a Java program to demonstrate insertions and string buffer in tree set
Entities
   -public class TreeSetDemo
Function Declaration
   - public int compare(StringBuffer s1, StringBuffer s2)
   - public static void main(String[] args)
Jobs to be done
 1.Create a Class as TreeSetDemo.
 2.Create public int compare with 2 StringBuffers as parameter and return compare two Strings.
 3.Declare main method and create a set as player as String and players as StringBuffer.
 4.Add 2 values in player and Add 2 values in players. 
 5.Print the player and players.

Pseudocode:
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
public class TreeSetDemo implements Comparator<StringBuffer> {
		
	public int compare(StringBuffer string1, StringBuffer string2) { 
	    return string1.toString().compareTo(string2.toString()); //Compare  
	} 

	public static void main(String[] args) {
    	Set<String> player = new TreeSet<String>();  //Creating set as String
        Set<StringBuffer> players = new TreeSet<>(new TreeSetDemo());
        Add players
        System.out.println(players); 

	}

}        
 
Program:
*/

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
public class TreeSetDemo implements Comparator<StringBuffer> {
		
	public int compare(StringBuffer string1, StringBuffer string2) { 
	    return string1.toString().compareTo(string2.toString()); //Compare  
	} 

	public static void main(String[] args) {
    	Set<String> player = new TreeSet<String>();  //Creating set as String
        Set<StringBuffer> players = new TreeSet<>(new TreeSetDemo()); //Creating set as StringBuffer
        player.add("Kohli"); 
        player.add("Dhoni"); 
        System.out.println(player); 
        players.add(new StringBuffer("Rashid")); 
        players.add(new StringBuffer("Raina")); 
        System.out.println(players); 

	}

}
