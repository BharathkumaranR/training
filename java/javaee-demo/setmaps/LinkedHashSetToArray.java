/*3.Demonstrate linked hash set to array() method in java
 
Work Breakdown Structure(WBS):
1.Requirements:
   -To demonstrate a linked hash set to array() method in java
2.Entities
   -public class LinkedHashSetToArray
3.Function Declaration
   - public static void main(String [] args)
4.Jobs to be done
   1.Create a Class name LinkedHashSetToArray and declare main method.
   2.Create a set as players and add 6 values and remove on value from set using remove() method.
   3.Using .toArray() method covert to array.
   4.Print the Array using for loop.
   
Pseudocode:
import java.util.LinkedHashSet;
import java.util.Set;
public class LinkedHashSetToArray {

	public static void main(String[] args) {
        Set<String> players = new LinkedHashSet<>();
        Add 6 players
        Object[] player = players.toArray();  
        System.out.println("The Array elements:");
        for (int i = 0; i < player.length; i++) { 
            System.out.println(player[i]); 
        }

	}

}           
 
 Program:
 */
 
import java.util.LinkedHashSet;
import java.util.Set;
public class LinkedHashSetToArray {

	public static void main(String[] args) {
        Set<String> players = new LinkedHashSet<>();
	    players.add("Andrew");     //Adding 6 Values
	    players.add("Kohli");
	    players.add("Dhoni");
	    players.add("Rashid");   
	    players.add("Bahir");
	    players.add("Raina"); 
	    System.out.println("The LinkedHashSet: \n"+players); //Print the set values
	    
	    System.out.println(" ");
	    //Create array convert to elements using .toArray() method
        Object[] player = players.toArray();  
        System.out.println("The Array elements:");
        for (int i = 0; i < player.length; i++) { 
            System.out.println(player[i]); 
        }

	}

}
