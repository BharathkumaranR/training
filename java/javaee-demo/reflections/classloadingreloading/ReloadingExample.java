package classloadingreloading;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class ReloadingExample extends ClassLoader {
    public ReloadingExample(ClassLoader parent) {
        super(parent);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	public Class loadClass(String name) throws ClassNotFoundException {
        if(!"reflection.MyObject".equals(name))
                return super.loadClass(name);

        try {
            String url = "file:C:/data/projects/tutorials/web/WEB-INF/" +
                            "classes/reflection/MyObject.class";
            URL myUrl = new URL(url);
            URLConnection connection = myUrl.openConnection();
            java.io.InputStream input = connection.getInputStream();
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            int data = input.read();

            while(data != -1){
                buffer.write(data);
                data = input.read();
            }

            input.close();

            byte[] classData = buffer.toByteArray();

            return defineClass("reflection.MyObject",
                    classData, 0, classData.length);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

	public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {

	ClassLoader parentClassLoader = ReloadingExample.class.getClassLoader();
	ReloadingExample classLoader = new ReloadingExample(parentClassLoader);
	@SuppressWarnings("rawtypes")
	Class myObjectClass = classLoader.loadClass("reflection.ReloadExample");

	

	@SuppressWarnings("unused")
	ReloadingExample object2 = (ReloadingExample) myObjectClass.newInstance();

	// create new class loader so classes can be reloaded.
	classLoader = new ReloadingExample(parentClassLoader);
	myObjectClass = classLoader.loadClass("reflection.MyObject");

	object2 = (ReloadingExample) myObjectClass.newInstance();

	}

}
