/* Consider a following code snippet
        Queue bike = new PriorityQueue();    
        bike.poll();
        System.out.println(bike.peek());    

  what will be output and complete the code.
Word Breakdown Structure(WBS):
1.Requirements :
  -To illustrate what will be output and complete the code.
2.Entities:
  -public class  QueueSnippet 
3.Function Declaration:
  -public static void main(String [] args)
4.Job to be done :  
   1.Create a class named as QueueSnippet and declare main method
   2.In the main class creating the Queue with generic String type and adding values using add() method.
   3.To print the peek values poll() method to get values and using peek() printing the peek value.
   
Pseudocode:
import java.util.PriorityQueue;
import java.util.Queue;
public class QueueSnippet {

	public static void main(String[] args) {
        Queue<String> countries = new PriorityQueue<String>();
        Add 6 countries
        countries.poll();
        System.out.println(countries.peek());  

	}

}        
        
 
 Program Code:
*/
import java.util.PriorityQueue;
import java.util.Queue;
public class QueueSnippet {

	public static void main(String[] args) {
        Queue<String> countries = new PriorityQueue<String>();
        countries.add("India");
        countries.add("America");
        countries.add("Iran");
        countries.add("Japan");
        countries.add("Pakistan");
        countries.add("Sri lanka");
        countries.poll();
        System.out.println(countries.peek());  

	}

}
