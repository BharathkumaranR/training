/* Create a queue using generic type and in both implementation Priority Queue,Linked list and complete following  
  -> add atleast 5 elements
  -> remove the front element
  -> search a element in stack using contains key word and print boolean value value
  -> print the size of queue
  -> print the elements using Stream 
Word Breakdown Structure(WBS):
1.Requirements :
  ->To create a stack using generic type and implement
  ->To push atleast 5 elements
  ->To pop the peek element
  ->To search a element in stack and print index value
  ->To print the size of stack
  ->To print the elements using Stream
  Reverse List Using Stack with minimum 7 elements in list.

2.Entities:
  - QueueDemo 
3.Function Declaration:
  - public void linkedList()
  - public void priority
  - public static void main(String [] args)
4.Job to be done :  
   1.In the main class create object for class and invoking the two methods.
   2.In the linkedList() method creating Queue with generic String type with initailse Linkedlist 
and creating stream for displaying the Stack elements.
   3.Add 5 values to Queue using add() method and using remove() method removing value specified value.
   4.i).Search the specified value using search() method and find size of stack using size() method.
    ii).Using contains() method finding the given value is in the Queue or not and using size() method finding size of queue.
   5.Print the queue values using stream and for each value.
   6.In the priority() method creating queue with initailse PriorityQueue and also creating stream to print the queue values.
   7.Add the values to queue using add() method and finding specified value using contains() method.
   8.Find the size using size() method and printing the PriorityQueue values using stream and for each

 Pseudocode:
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Stream;
public class QueueDemo{
	
	public void linkedList() {
		 Queue<String> languages = new LinkedList<String>();
	        Stream<String> stream = languages.stream();
            ADD elements in linkedlist()
            Remove element
	        stream.forEach((element) -> {
	           System.out.println(element);  
	        }); 
	}
	public void priority() {
		 Queue<String> languages = new PriorityQueue<String>();
	        Stream<String> stream = languages.stream();             
	        System.out.println(languages.contains("Tamil"));
	        System.out.println(languages.size());   
	        stream.forEach((element) -> {
	           System.out.println(element); 
	        });    //Process Stack Using Stream
	    }

	public static void main(String[] args) {
    	QueueDemo queue = new QueueDemo();
    	System.out.println("Linked List");
    	queue.linkedList();
    	System.out.println("Priority");
    	queue.priority();		

	}

}
Program Code:
*/
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Stream;
public class QueueDemo{
	
	public void linkedList() {
		 Queue<String> languages = new LinkedList<String>();
	        Stream<String> stream = languages.stream();
	        languages.add("Tamil");
	        languages.add("English");
	        languages.add("Hindi");
	        languages.add("Telugu");
	        languages.add("Malayalam");
	        languages.remove();
	        
	        System.out.println(languages.contains("Japanese"));
	        System.out.println(languages.size());       
	        stream.forEach((element) -> {
	           System.out.println(element);  
	        }); 
	}
	public void priority() {
		 Queue<String> languages = new PriorityQueue<String>();
	        Stream<String> stream = languages.stream();
	        languages.add("Tamil");        
	        languages.add("English");
	        languages.add("Hindi");
	        languages.add("Telugu");
	        languages.add("Malayalam");
	        languages.remove();
	        System.out.println(languages.contains("Tamil"));
	        System.out.println(languages.size());   
	        stream.forEach((element) -> {
	           System.out.println(element); 
	        });    //Process Stack Using Stream
	    }

	public static void main(String[] args) {
    	QueueDemo queue = new QueueDemo();
    	System.out.println("Linked List");
    	queue.linkedList();
    	System.out.println("Priority");
    	queue.priority();		

	}

}

/*
Output:
Linked List
false
4
English
Hindi
Telugu
Malayalam
Priority
true
4
Hindi
Malayalam
Tamil
Telugu
*/
