/*  Create a stack using generic type and implement
  -> Push atleast 5 elements
  -> Pop the peek element
  -> search a element in stack and print index value
  -> print the size of stack
  -> print the elements using Stream
  Reverse List Using Stack with minimum 7 elements in list.

Word Breakdown System(WBS)
1.Requirements :
  ->To create a stack using generic type and implement
  ->To push atleast 5 elements
  ->To pop the peek element
  ->To search a element in stack and print index value
  ->To print the size of stack
  ->To print the elements using Stream
  Reverse List Using Stack with minimum 7 elements in list.
2.Entities:
  - StackDemo 
3.Function Declaration:
  - public static void main(String [] args)
4.Job to be done :  
   1.In the main class create the Stack with generic String type and creating stream for displaying the Stack elements.
   2.i).Push 5 values to stack using push() method and using pop() method displaying top value in stack.
    ii).Search the specified value using search() method and find size of stack using size() method.
   3.i).Print the index value in stack using specifying index in println.
    ii).Print the stack values using stream and for each.
   4.Reverse a stack creating another stack removing the values using remove() method and pushing to new created 
stack using push() method 
   5.Remove elements from stack adding to list using add() method.
 
Pseudocode:

import java.util.Stack;
import java.util.stream.Stream;
public class StackDemo {

	public static void main(String[] args) {
        
        Stack<String> languages = new Stack<String>();  
        Stream<String> stream = languages.stream();
        Add languages
        System.out.println("Create a stack using generic type and implement");
        int index = languages.search("Punjabi");  
        int size = languages.size();        
        System.out.println(size);        
        System.out.println(index);      
        stream.forEach((element) -> {
           System.out.println(element); 
        });    //Process Stack Using Stream
        
        System.out.println("Reverse List Using Stack with minimum 7 elements in list");
        Stack<String> stack = new Stack<String>();
        while(languages.size() > 0) {
                  stack.push(languages.remove(0)); //Element is removed from list and push to stack
        }

        while(stack.size() > 0){
                languages.add(stack.pop()); //Element is pop from stack and add to list
        } 

        System.out.println(languages);

	}

}        
Program:
*/
import java.util.Stack;
import java.util.stream.Stream;
public class StackDemo {

	public static void main(String[] args) {
        
        Stack<String> languages = new Stack<String>();  //Cretaing Stack using generic type
        Stream<String> stream = languages.stream();
        languages.push("Tamil");
        languages.push("English");
        languages.push("Hindi");
        languages.push("Telugu"); 
        languages.add("Malayalam");        
        languages.pop();   //pop top element in stack
        System.out.println("Create a stack using generic type and implement");
        int index = languages.search("Punjabi");  //Initialise search variable
        int size = languages.size();        //Initialise size variable
        System.out.println(size);       //Printing the stack size 
        System.out.println(index);      //Printing the index of the element
        stream.forEach((element) -> {
           System.out.println(element);  // print element
        });    //Process Stack Using Stream
        
        System.out.println("Reverse List Using Stack with minimum 7 elements in list");
        Stack<String> stack = new Stack<String>();
        while(languages.size() > 0) {
                  stack.push(languages.remove(0)); //Element is removed from list and push to stack
        }

        while(stack.size() > 0){
                languages.add(stack.pop()); //Element is pop from stack and add to list
        } 

        System.out.println(languages);

	}

}

/*
Output:
	Create a stack using generic type and implement
	4
	-1
	Tamil
	English
	Hindi
	Telugu
	Reverse List Using Stack with minimum 7 elements in list
	[Telugu, Hindi, English, Tamil]	
*/