/*
4. Write some String content using OutputStream

Work Breakdown Structure(WBS):
1.Requirement:
    ->To write some String content using OutputStream

2.Entity:
    ->public class WriteStringUsingOutputStream

3.Method Signature:
    ->public static void main(String[] args)

4.Jobs to be done:

    1.Get the file using file path and store it in source String.
      1.1)Check file exist using FileOutputStream class.
    2.Write the file using OutputStream.
    3.Invoke the write of byte array to write content to file and close an outStream .
    4.OutputStream decodes the array and store as String content.

Pseudo Code:

public class WriteStringUsingOutputStream {
    public static void main(String[] args) {
        OutputStream outStream = new FileOutputStream(source);
        String data = "This statement written by Output Stream";
        byte[] byteArray = data.getBytes();
        outStream.write(byteArray);
        outStream.close();
        System.out.println("File writed successfully");
    }

}
Program:
*/

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class WriteStringUsingOutputStream {

	public static void main(String[] args) {
        // file path
        String source = "C:/Users/SUBASH R/eclipse-workspace/Javaee-demo/advance.io.stream/Content.txt";
        try {
            OutputStream outStream = new FileOutputStream(source);
            String text = "This Content writtern using OutputStream";

            // encodes the string to byte array
            byte[] byteArray = text.getBytes();

            // decodes and store as string
            outStream.write(byteArray);
            outStream.close();
            System.out.println("File writed successfully");
        } catch (FileNotFoundException fileNotFound) {
            fileNotFound.getMessage();
        } catch (IOException ioe) {
            ioe.getMessage();
        }

	}

}
