/*
3. Read a any text file using BufferedReader and print the content of the file

Work Breakdown Structure(WBS):

1.Requirement:
   ->To read a any text file using BufferedReader and print the content of the file

2.Entity:
   ->public class ReadContentOfFile

3.Method Signature:
   ->public static void main(String[] args)

4.Jobs to be done:
     1.Create a file to write content.
     2.Write content to the file created using write method.
     3.Close the file after written using close method.
     4.Using FileReader to read the content of file.
     5.Buffered reader takes the Reader file.
     6.Read each line
         6.1)If a line exists on file show the line of data.
         6.2)If no more line exists, stop reading the file.
     7.Close the BufferedReader properly using close method.

Pseudo Code:

public class ReadFContentOfFile {
    public static void main(String[] args) throws IOException {
        FileWriter file = new FileWriter(source);
        file.write("I'm Bharath Kumaran");
        file.write("\nI'm studying ECE in KPRIET");
        file.write("\nI'm underoging Full Stack Development Training");
        file.close();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(source));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            System.out.println(line);
        }
        bufferedReader.close();
    }
}
Program:
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ReadContentOfFile {

	public static void main(String[] args) throws IOException {
        // Create a file on specified location
        String source = "C:/Users/SUBASH R/eclipse-workspace/Javaee-demo/advance.io.stream/Content.txt";
        FileWriter file = new FileWriter(source);

        // Writing data to created file and closing the file
        file.write("I'm Bharath Kumaran");
        file.write("\nI'm studying ECE in KPRIET");
        file.write("\nI'm underoging Full Stack Development Training");
        file.close();

        // reader to read the data in file
        BufferedReader bufferedReader = new BufferedReader(new FileReader(source));
        String line;
        // readLine reads data line by line
        while ((line = bufferedReader.readLine()) != null) {
            System.out.println(line);
        }
        // closing the reader
        bufferedReader.close();       

	}

}
