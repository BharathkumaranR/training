/*
Create a File, get the Following fields as input from the user 
    1.Name
    2.studentId
    3.Address
    4.Phone Number
now store the input in the File and serialize it, and again de serialize the File and print the 
content.
Work Breakdown Structure(WBS):

1.Requirement:
   ->To write a program to store the input in the File and serialize it, and again de serialize the File and print the 
content.

2.Entity:
   ->public class DeserializeTheFile 

3.Method Signature:
   ->public static void main(String [] args)

4.Jobs to be done:
   1.Create a Object for Student student it is equals to NULL.
   2.Use try block
      2.1)From the path to store ser file and store it in fileStream
      2.2)Create a object for ObjectInputStream pass file path as argument.
      2.3)Reads in fileStream using readObject method.
   3.Catch IOException and ClassNotFoundException error.
   4.Print name ,studentId,Address,Phone Number.        

Psudocode:

public class DeserializeTheFile {
	   public static void main(String [] args) {
		      Student student = null;
		      TRY {
		         FileInputStream fileIn = new FileInputStream("C:/Users/SUBASH R/eclipse-workspace/Javaee-demo/serialization/student.ser");
		         ObjectInputStream fileStream = new ObjectInputStream(fileIn);
		         student = (Student) fileStream.readObject();
		         fileStream.close();
		         fileIn.close();
		      } CATCH IOException i {
		         i.printStackTrace();
		         return;
		      } CATCH ClassNotFoundException c {
		         PRINT "Student class not found";
		         c.printStackTrace();
		         return;
		      }
		      System.out.println("Deserialized Employee...");
		      System.out.println("Name: " + s.name);
		      System.out.println("StudentId: " + s.studentId);
		      System.out.println("Address: " + s.address);
		      System.out.println("Phone Number: " + s.phonenumber);

	   }
}	
Program:
*/
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializeTheFile {

	public static void main(String[] args) {
	      Student student = null;
	      try {
	         FileInputStream fileIn = new FileInputStream("C:/Users/SUBASH R/eclipse-workspace/Javaee-demo/serialization/student.ser");
	         ObjectInputStream fileStream = new ObjectInputStream(fileIn);
	         student = (Student) fileStream.readObject();
	         fileStream.close();
	         fileIn.close();
	      } catch (IOException i) {
	         i.printStackTrace();
	         return;
	      } catch (ClassNotFoundException c) {
	         System.out.println("Student class not found");
	         c.printStackTrace();
	         return;
	      }
	      
	      System.out.println("Deserialized Student-");
	      System.out.println("Name: " + student.name);
	      System.out.println("StudentId: " + student.studentId);
	      System.out.println("Address: " + student.address);
	      System.out.println("Phone Number: " + student.phonenumber);

	}

}
