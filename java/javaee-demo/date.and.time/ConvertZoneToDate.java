/*
12. Write a Java program to convert ZonedDateTime to Date.

Work Breakdown Structure(WBS):

1.Requirement :
    ->Towrite a program to convert ZonedDateTime to Date.

2.Entity:
    ->public class ConvertZoneToDate

3.Method Declaration:
    ->public static void main(String[] args)

4.Jobs to be done :
     1.Invoke ZoneId class systemDefault method and store default zone time in defaultTimeZone.
     2.Invoke LocalDateTime class now method and store local date time in localDateTime variable.
     3.Convert the zone time to date using LocalDateTime class atZone method store it in zonedDateTime.
          3.1)Print the zonedDateTime.
     4.Get the current date and time using Date class from method store it in date.
          4.1)Print the date.
     
Pseudo Code:

public class ConvertZoneToDate {
	public static void main(String[] args) {
		ZoneId defaultTimeZone = ZoneId.systemDefault();
		LocalDateTime localDateTime = LocalDateTime.now();
		ZonedDateTime zonedDateTime = localDateTime.atZone(defaultTimeZone);
		System.out.println("ZonedDateTime : " + zonedDateTime);
		Instant instant = zonedDateTime.toInstant();
		Date date = Date.from(instant);
		System.out.println("Date : " + date);
	}
}
Program:
*/

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

public class ConvertZoneToDate {

	public static void main(String[] args) {
		// Get default time zone
		ZoneId defaultTimeZone = ZoneId.systemDefault();
		// Get current date and time.
		LocalDateTime localDateTime = LocalDateTime.now();
		ZonedDateTime zonedDateTime = localDateTime.atZone(defaultTimeZone);
		System.out.println("ZonedDateTime : " + zonedDateTime);
		// ZonedDateTime to Instant
		Instant instant = zonedDateTime.toInstant();
		// Convert Instant to Date.
		Date date = Date.from(instant);
		System.out.println("Date : " + date);

	}

}
