/*
Problem Statement:

4.Write an example that tests whether a given date occurs on Tuesday the 11th.

Work Breakdown Structure(WBS):
1.Requirement :
    ->To write an example that tests whether a given date occurs on Tuesday the 11th.
    
2.Entity:
    ->hursdayEleventhDemo
    ->IsTuesdayDemo
    ->public class TuesdayDemo
    
3.Method declaration:
    ->public static void main(String[] args)
    
4.Jobs To be Done:
    1.Get the input date from user
    2.Invoke the LocateDate class and pass the Parameter which you get from user 
      2.1)Store it in object of LocalDate's 
      2.2)Use  LocalDate's object and invoke the Class IsTuesdayDemo which implements TemporalQuery<Boolean> 
    3.Check the user input month is eleven and day is Tuesday
      3.1)if the condition satisfied return true.
      3.2)if the condition not satisfied return false.
      
Pseudo code:

class IsTuesdayDemo implements TemporalQuery<Boolean> {
    public Boolean queryFrom(TemporalAccessor date) {
        return ((date.get(ChronoField.DAY_OF_MONTH) == 11) && (date.get(ChronoField.DAY_OF_WEEK) == 2));
    }
}
public class ThursdayEleventhDemo {
    public static void main(String[] args) { 
        LocalDate thisDate = LocalDate.parse(date);
        System.out.println(thisDate.query(new IsTuesdayDemo()));
    }

}
Program Code:
*/
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalQuery;
import java.util.Scanner;

class IsTuesdayDemo implements TemporalQuery<Boolean> {

// Returns TRUE if the date occurs on Tuesday the 11th.
    public Boolean queryFrom(TemporalAccessor date) {
        return ((date.get(ChronoField.DAY_OF_MONTH) == 11) && (date.get(ChronoField.DAY_OF_WEEK) == 2));
    }
}

public class TuesdayDemo {

	public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the year,month and date as(YYYY-MM-DD)");
        String date = scanner.next();
        LocalDate thisDate = LocalDate.parse(date);
        System.out.println(thisDate.query(new IsTuesdayDemo()));
        scanner.close();

	}

}
