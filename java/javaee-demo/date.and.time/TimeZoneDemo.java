/*
Problem Statement:

9.Display the default time zone and Any three particular time zone as mentioned below.
     Sample output: 
        Displaying current of the particular TimeZones
        GMT-27-09-2020 Sunday 08:36:28 AM
        Europe/Copenhagen-27-09-2020 Sunday 10:36:28 AM
        Australia/Perth-27-09-2020 Sunday 04:36:28 PM
Work Breakdown Structure(WBS):

1.Requirement :
    ->To write a program to Display the default time zone and Any three particular time 
zone as mentioned below. 
2.Entity:
    ->public class TimeZoneDemo

3.Method declaration:
    ->public static void main(String[] args)
    
4.Jobs To be Done:
    1.Invoke differentTZTimings static method.
    2.Invoke TimeZone class getTimeZone method to get time zone.
        2.1)Set Date Format using SimpleDateFormat class setTimeZone method.
        2.2)Using date using format method
        2.3)Print current time zone.
    3.Get the 
       3.1)Asia/Kolkata
       3.2)Australia/Perth Zone
       3.3)Using getTimeZone method
    4.Set the time zone using setTimeZone method.
    5.Set the date format using format method.
        
    
Pseudo code:

public class TimeZoneDemo {
	public static void differentTZTimings() {
		TimeZone timeZone = TimeZone.getTimeZone("GMT");
		SimpleDateFormat date_format = new SimpleDateFormat("dd-MM-yyyy EEEE hh:mm:ss a");
		date_format.setTimeZone(timeZone);
		Date date = new Date();
		String current_date_time = date_format.format(date);
		System.out.println("GMT-" + current_date_time);

		timeZone = TimeZone.getTimeZone("Asia/Kolkata");
		date_format.setTimeZone(timeZone);
		current_date_time = date_format.format(date);
		System.out.println("Asia/Kolkata-" + current_date_time);

		timeZone = TimeZone.getTimeZone("Australia/Perth");
		date_format.setTimeZone(timeZone);
		current_date_time = date_format.format(date);
		System.out.println("Australia/Perth-" + current_date_time);
	}

	public static void main(String[] args) {
		System.out.println("Displaying current of the particular TimeZones");
		differentTZTimings();
	}
}
 
*/

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TimeZoneDemo {
	public static void differentTZTimings() {
		TimeZone timeZone = TimeZone.getTimeZone("GMT");
		SimpleDateFormat date_format = new SimpleDateFormat("dd-MM-yyyy EEEE hh:mm:ss a");
		date_format.setTimeZone(timeZone);
		Date date = new Date();
		String current_date_time = date_format.format(date);
		System.out.println("GMT-" + current_date_time);

		timeZone = TimeZone.getTimeZone("Asia/Kolkata");
		date_format.setTimeZone(timeZone);
		current_date_time = date_format.format(date);
		System.out.println("Asia/Kolkata-" + current_date_time);

		timeZone = TimeZone.getTimeZone("Australia/Perth");
		date_format.setTimeZone(timeZone);
		current_date_time = date_format.format(date);
		System.out.println("Australia/Perth-" + current_date_time);

	}
	
	public static void main(String[] args) {
		System.out.println("Displaying current of the particular TimeZones");
		differentTZTimings();

	}

}
