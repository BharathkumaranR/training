/*
 Date and Time API
 
Problem Statement:

2. Which class would you use to store your birthday in years, months, days, seconds, and nanoseconds?

Answer:

    Classes are used to store your birthday
       *LocalDateTime class
       *ZonedDateTime class
       Both classes track date and time to nanosecond precision and both classes 
when used in conjunction with Period result in such as years, months, days, seconds, and nanoseconds.
*/