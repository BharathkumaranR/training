/*
Problem Statement
1. How do you convert a Calendar to Date and vice-versa with example? 
Work Breakdown Structure(WBS):
1.Requirement :
    - Convert a Calendar to Date and vice-versa.

2.Entity:
    - CalculateDateandTime

3.Method Declaration:
    - public static void main(String[] args)

4.Jobs to be done :
    1.Invoke the DateToCalendar method.
    2.Invoke the CalendarToDate method.
    3.Get the instant time for converting it to Date format.
    4.Invoke the simpleDateFormat constructor and convert  the time to Date.
    5.Print the resultant date and time.
      
Pseudo Code:
public class CalenderAndDate {
	private Calendar dateToCalendar(Date date) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar;

	} 
	private Date calendarToDate(Calendar calendar) {
		return calendar.getTime();
	}

	public static void main(String[] argv) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
		String dateInString = "22-01-2015 10:20:56";
		Date date = sdf.parse(dateInString);
		CalenderAndDate convertor = new CalenderAndDate();

		// 2. Test - Convert Date to Calendar
		Calendar calendar = convertor.dateToCalendar(date);
		System.out.println(calendar.getTime());

		// 3. Test - Convert Calendar to Date
		Date newDate = convertor.calendarToDate(calendar);
		System.out.println(newDate);

	}

Program:
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CalculateDateAndTime {
	// Convert Date to Calendar
	private Calendar dateToCalendar(Date date) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar;

	}

	// Convert Calendar to Date
	private Date calendarToDate(Calendar calendar) {
		return calendar.getTime();
	}
	
	public static void main(String[] args) throws ParseException {

		// 1. Create a Date from String
		SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
		String dateInString = "22-01-2015 10:20:56";
		Date date = sdf.parse(dateInString);
		CalculateDateAndTime convertor = new CalculateDateAndTime();

		// 2. Test - Convert Date to Calendar
		Calendar calendar = convertor.dateToCalendar(date);
		System.out.println(calendar.getTime());

		// 3. Test - Convert Calendar to Date
		Date newDate = convertor.calendarToDate(calendar);
		System.out.println(newDate);

	}

}
