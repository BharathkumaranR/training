/*
1. How do you convert a Calendar to Date and vice-versa with example?

Work Breakdown Structure(WBS):
1.Requirement :
    ->To write a program to convert a Calendar to Date 

2.Entity:
    ->public class CalendarToDate

3.Method Declaration:
    ->public static void main(String[] args)

4.Jobs to be done :
     1.Invoke Calendar class getInstance method and store in calendar.
     3.Using add method add one to Calender date.
          3.1)Get the add one date using Calender class getTime method and store it in date.
     4.Using SimpleDateFormat class set date and time that format.
          3.1)Set the date format using format method. 
     5.Print the date.
     
Pseudo Code:


public class CalendarToDate {
	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 1);
		java.util.Date date = calendar.getTime();             
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String dateFormat = format.format(date); 
		System.out.println(dateFormat);
		java.util.Date inActiveDate = null;
		try {
		    inActiveDate = format.parse(dateFormat);
		} catch (ParseException e1) {
		    e1.printStackTrace();
		}
	}
}
Program:
*/



import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CalendarToDate {

	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 1);
		java.util.Date date = calendar.getTime();             
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String dateFormat = format.format(date); 
		System.out.println(dateFormat);
		@SuppressWarnings("unused")
		java.util.Date inActiveDate = null;
		try {
		    inActiveDate = format.parse(dateFormat);
		} catch (ParseException e1) {
		    // TODO Auto-generated catch block
		    e1.printStackTrace();
		}

	}

}
