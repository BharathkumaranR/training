/* 
2. Find the time difference using timer class ?
Work Breakdown Structure(WBS):
1.Requirement :
    ->To Find the time difference using timer class

2.Entity:
    ->public class TimerClass
    ->TimerClass
    ->RemindTask

3.Method Declaration:
    ->public static void main(String[] args)
    ->public void run()
    ->public TimerClass(int seconds)

4.Jobs to be done :
     1.Create new TimerClass constructor with passing argument value to schedule display.
           1.1)Print the "Task scheduled".
     2.Invoke TimerClass class constructor 
           2.1)Store new Timer in timer.
           2.1)Schedule to execute RemindTask class
     3.RemindTask class extends the inbuild TimerTask class
          3.1)Method runs the schedule time comes.
           
Pseudo Code:

public class TimerClass {
    Timer timer;
    public TimerClass(int seconds) {
        timer = new Timer();
        timer.schedule(new RemindTask(), seconds*1000);
	}
    class RemindTask extends TimerTask {
        public void run() {
            System.out.println("Time's up!");
            timer.cancel(); 
        }
    }
    public static void main(String args[]) {
        new TimerClass(5);
        System.out.println("Task scheduled.");
    }
}
Program:
*/

import java.util.Timer;
import java.util.TimerTask;

public class TimerClass {
    Timer timer;

    public TimerClass(int seconds) {
        timer = new Timer();
        timer.schedule(new RemindTask(), seconds*1000);
	}

    class RemindTask extends TimerTask {
        public void run() {
            System.out.println("Time's up!");
            timer.cancel(); //Terminate the timer thread
        }
    }
	public static void main(String[] args) {
        new TimerClass(5);
        System.out.println("Task scheduled.");

	}

}
