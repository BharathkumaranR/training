/*
Problem Statement:

5.Write an example that, for a given year, reports the length of each month within that particular year.

Work Breakdown Structure(WBS):
1.Requirement:
    ->To write an example  Java program that, for a given year, reports the length of each month within that particular year.
    
2.Entity:
    ->public class MonthInYear 

3.Method Signature:
    ->public static void main(String[] args);
    ->maxLength();

4.Jobs To be Done;
    1.Get the year input from user.
    2.Invoke the class Year 
           2.1)Pass the user input year as parameter.
    3.Get the month input from user.
    4.Invoke the Month
           4.1)Pass the user input month as parameter. 
    5.Invoke max length method 
           5.1)Find the length of the month.
    6.Print the result.

Pseudo code:


    public class MonthInYear {
        public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int year = scanner.nextInt();
        Year thisYear = Year.of(year);
        int month = scanner.nextInt();
        Month thismonth = Month.of(month);
        System.out.println(thismonth.maxLength());
    }
}

Program:
*/
import java.time.Month;
import java.time.Year;
import java.util.Scanner;

public class MonthInYear {

	public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the year \n");
        int year = scanner.nextInt();
        @SuppressWarnings("unused")
		Year thisYear = Year.of(year);
        System.out.print("Enter the month \n");
        int month = scanner.nextInt();
        Month thismonth = Month.of(month);
        System.out.println(thismonth.maxLength());
        scanner.close();

	}

}
