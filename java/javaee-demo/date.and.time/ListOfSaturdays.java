/*
Problem Statement
3. Write an example that, for a given month of the current year, 
lists all of the Saturdays in that month.
Work Breakdown Structure(WBS):
Requirement :
    ->To write a Java program with an example that, for a given month of the current year, lists all of the Saturdays in that month.

Entity:
    ->public class ListOfSaturdays

Method Declaration:
    - public static void main(String[] args)
    - firstInMonth();

Jobs to be done :
    1.Get the month input from user
    2.Invoke the Month class and pass the user input as parameter.
    3.Invoke the LocalDate class and pass the parameter and store it in its object. 
    4.With that object get the Saturday's date and print it.
    5.Use the while loop 
           5.1)Check the month and print the next saturday's date.
      
Pseudo Code:


public class ListOfSaturdays {
    public static void main(String[] args) {
        Month month1 = Month.valueOf(month.toUpperCase());
        String month = scanner.next();
        System.out.print( month1);
        LocalDate date = Year.now().atMonth(month1).atDay(1)
                         .with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));
        Month newMonth = date.getMonth();
        while (newMonth == month1) {
            System.out.printf("%s%n", date);
            date = date.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
            newMonth = date.getMonth();
        }
    }
}
Program:
 */


import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.temporal.TemporalAdjusters;
import java.util.Scanner;

public class ListOfSaturdays {

	public static void main(String[] args) {
		
        Scanner scanner = new Scanner(System.in);
        System.out.printf("Enter the month to find the Satudays %n");
        String month = scanner.next();
        Month monthString = Month.valueOf(month.toUpperCase());
        System.out.printf("For the month of ", monthString);
        LocalDate date = Year.now().atMonth(monthString).atDay(1)
                         .with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));
        Month newMonth = date.getMonth();
        while (newMonth == monthString) {
            System.out.printf("%s%n", date);
            date = date.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
            newMonth = date.getMonth();
        }
        scanner.close();

	}

}
