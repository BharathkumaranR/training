/*
 2) Write a Java program to test if a map contains a mapping for the specified key?


Word Breakdown Structure(WBS)
1.Requirements
   -To write a java program to copy all of the mappings from the specified map to another map.
   -To count the size of mappings in a map
2.Entities
   - ContainsKeyValuePairs
3.Function Declaration
   - public static void main(String[] args)
4.Jobs to be done
   1.Creating object for HashMap with integer and String called bikes.
   2.Adding the values using put() method and using containsKey() method find the specified key value map is containing or not.
   3.Count the size of mappings in a map using size() method.

Psudeo Code:

public class ContainsKey {
	public static void main(String args[]) {
        HashMap<Integer, String> bikes = new HashMap<Integer, String>();
        ADD ELEMENTS TO MAP
        System.out.println(bikes.containsKey(20));
        System.out.println(bikes.containsKey(8));
        System.out.println(bikes.size());
	 }
}   

Program:
*/


import java.util.HashMap;

public class ContainsKeyValuePairs {

	public static void main(String[] args) {

		  //Creating hashMap 
	      HashMap<Integer, String> patients = new HashMap<Integer, String>();

	      /*Adding elements to HashMap*/
	      patients.put(27, "Ashwin");
	      patients.put(18, "Manohar");
	      patients.put(34, "Hirendar");
	      patients.put(42, "Pravin ");
	      patients.put(7, "Prakash");
	      
	      //Testing if a map contains a mapping for the specified key
	      System.out.println("Checking '27' key value map containing or not");
	      System.out.println(patients.containsKey(27));
	      
	      System.out.println("Checking '10' key value map containing or not");
	      System.out.println(patients.containsKey(10));
	      
	      //Count the size of mappings in a map
	      System.out.println("Count the size of mappings in a map");
	      System.out.println(patients.size());
	}

}
