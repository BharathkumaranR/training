/*
 1)Write a Java program to copy all of the mappings from the specified map to another map?

Word Breakdown Structure(WBS):
1.Requirements
   -To write a java program to copy all of the mappings from the specified map to another map.
2.Entities
   - CopySpecifiedKey
3.Function Declaration
   - public static void main(String[] args)
4.Jobs to be done
   1.Create a class as CopySpecifiedKey and declare the main.
   2.In the main class creating object for HashMap with integer and String called patients.
   3.Adding the values using put() method and creating another hashMap patients2 for copying from patients to patients2 using putAll() method.
   4.Printing the copied hashMap patients2.

Psudeo Code:
public class CopySpecifiedKey {

   public static void main(String args[]) {
        HashMap<Integer, String> bikes = new HashMap<Integer, String>();
        ADD ELEMENTS TO MAP
        HashMap<Integer, String> bikes2 = new HashMap<Integer, String>();
        bikes2.putAll(bikes);
   }
}

Program
*/


import java.util.HashMap;

public class CopySpecifiedKey {

	public static void main(String[] args) {
		
		   
		  //Creating hashMap 
	      HashMap<Integer, String> patients = new HashMap<Integer, String>();

	      /*Adding elements to HashMap*/

	      patients.put(27, "Ashwin");
	      patients.put(18, "Manohar");
	      patients.put(34, "Hirendar");
	      patients.put(42, "Pravin ");
	      patients.put(7, "Prakash");
	      
	      HashMap<Integer, String> patients2 = new HashMap<Integer, String>();
	      
	      
	      //Copy all of the mappings from the specified map to another map
	      patients2.putAll(patients);
	      System.out.println(patients2);

	}

}
