/*
4) Write a Java program to get the portion of a map whose keys range from a given key to another key?


Word Breakdown Structure(WBS)
1.Requirements
   -To write a java program to get the portion of a map whose keys range from a given key to another key.
2.Entities
   - GetPortion
3.Function Declaration
   - public static void main(String[] args)
4.Jobs to be done
   1.Create a class as GetPortion and declare the main.
   2.In the main class creating object for TreeMap with integer and String called patients.
   3.Selecting the portion of the TreeMap using subMap() method.
   4.Printing the Sub map key values.

Psudeo Code:
public class GetPortion {
	public static void main(String[] args) {
		TreeMap<Integer, String> bikes = new TreeMap<Integer, String>();
		ADD ELEMENTS TO MAP
		System.out.println(bikes.subMap(20, 40));
	}
}
Program:
*/


import java.util.TreeMap;

public class GetPortion {

	public static void main(String[] args) {

		//Creating TreeMap and SortedMap
	    TreeMap<Integer, String> patients = new TreeMap<Integer, String>();
	      
	    /*Adding elements to HashMap*/

        patients.put(27, "Ashwin");
        patients.put(18, "Manohar");
        patients.put(34, "Hirendar");
        patients.put(42, "Pravin ");
        patients.put(7, "Prakash");
	     
   	    System.out.println("Sub map key-values\n" + patients.subMap(20, 40));
   	    
	}

}
