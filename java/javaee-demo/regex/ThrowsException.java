/*
 2.Create a program which throws exception and fetch all the details of the exception.
 
Work Breakdown Structure(WBS):
1.Requirements:
   -To create a program which throws exception and fetch all the details of the exception.
2.Entities
   - public class ThrowsException
3.Function Declaration
   - public static void main (String [] args)
4.Jobs to be done
  1.Exception with throws FileNotFoundException and IOException
  2.Try Catch execute 
      2.1)Invoke ThrowsException class testException method with two different passing arguments.
          2.1.1)Check if passing value is less than or zero.
      2.2)Catch block catches FileNotFoundException and invoke Exception class printStackTrace method.
      2.3)Finally block print every time.   

Psudeo Code:
public class ThrowsException {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		try{
			testException(-5);
			testException(-10);
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}finally{
			System.out.println("Releasing resources");			
		}
		testException(15);
	}
	public static void testException(int i) throws FileNotFoundException, IOException{
		if(i < 0){
			FileNotFoundException myException = new FileNotFoundException("Negative Integer "+i);
			throw myException;
		}else if(i > 10){
			throw new IOException("Only supported for index 0 to 10");
		}

	}

}   

 
Program
*/

import java.io.FileNotFoundException;
import java.io.IOException;

public class ThrowsException {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		try{
			testException(-5);
			testException(-10);
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}finally{
			System.out.println("Releasing resources");			
		}
		testException(15);
	}
	
	public static void testException(int i) throws FileNotFoundException, IOException{
		if(i < 0){
			FileNotFoundException myException = new FileNotFoundException("Negative Integer "+i);
			throw myException;
		}else if(i > 10){
			throw new IOException("Only supported for index 0 to 10");
			
		}

	}

}
