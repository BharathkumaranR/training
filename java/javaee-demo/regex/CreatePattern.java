/* Regex:


 1.Create a pattern for password which contains
   8 to 15 characters in length
   Must have at least one uppercase letter
   Must have at least one lower case letter
   Must have at least one digit

Work Breakdown Structure(WBS):
1.Requirements:
   - String should contain length 8 to 15 atleast one digit,lower case letter,uppercase letter
2.Entities
   - public class CreatePattern
3.Function Declaration
   - public static void main (String [] args)
4.Jobs to be done
  1.Create a Class name CreatePatten and declare main method.
  3.Create a string contains length 8 to 15 atleast one digit,lower case letter,uppercase letter
  4.Create a patten and put a patten
     =>^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,15}$
     4.i) Must have at least one uppercase letter
     4.ii)Must have at least one lower case letter
     4.iii)Must have at least one digit   
  5.Create and Using Matcher and also with if condition print the result.   

Psudeo Code:
public class CreatePattern {
	public static void main (String [] args) {
	    String string = "Cre0tePattern";
		Pattern pattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,15}$");
	    Matcher matcher = pattern.matcher(string);
	    boolean matchFound = matcher.find();
	    if(matchFound) {
	      System.out.println("Match found");
	    } else {
	      System.out.println("Match not found");
	    }
	}
}
Program:
*/

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreatePattern {

	public static void main(String[] args) {
		String string = "CreatePattern";
		Pattern pattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,15}$");
	    Matcher matcher = pattern.matcher(string);
	    boolean matchFound = matcher.find();
	    if(matchFound) {
	      System.out.println("Match found");
	    } else {
	      System.out.println("Match not found");
	    }

	}

}
