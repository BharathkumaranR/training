package com.election.training.core; // creating package


public class Candidate extends Election{ //Inherits Election Parent class
    public String electionWardNumber; // Defining electionWardNumber property as public
    protected String electionWardName; //Defining electionWardName property as protected
    private String electionDate;
    public Candidate(String electionWardNumber,String electionWardName,String electionDate) // Parameterized Constructor 
    {
        super(electionWardNumber,electionWardName,electionDate);// super keyword refers to parent objects 
    }
    public void printInfo() { // Method Declaration 
        System.out.println("ElectionWardNumber: "+ this.electionWardNumber +" "+ "ElectionWardName: " + this.electionWardName);
        /*Printing only public and protected properties because public can be accessible in all classes,
        then protected only accessible in same packages and inherited classes*/
    }
}
