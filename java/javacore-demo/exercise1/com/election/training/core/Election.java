package com.election.training.core; //creating package

class Election {// creating a class as Election
    public String electionWardNumber; // Defining electionWardNumber property as public 
    protected String electionWardName; //Defining electionWardName property as protected
    private String electionDate; //Defining electionDate property as private
    public Election(String electionWardNumber,String electionWardName,String electionDate) {  // Parameterized Constructor 
        this.electionWardNumber = electionWardNumber;   // Initailizing property from user inputs
        this.electionWardName = electionWardName; 
        this.electionDate = electionDate;
    }
}