package accessSpecifierWithPackage;
import com.election.training.core.Candidate; //importing packages with creating class Candidate
import java.util.Scanner; // importing Scanner library for getting user inputs

public class Main {
	
       public static void main(String args[]) { 
    
    	   String electionWardNumber,electionWardName;
        Scanner sc = new Scanner(System.in); // creating object for getting user inputs
        System.out.println("Enter Election WardNumber,WardName");
        Candidate candidate = new Candidate("603","KumaranNagar","23/05/2021");//Creating Object for Child Class

        candidate.electionWardNumber = sc.nextLine(); //Getting user input for only employeeId 
        candidate.printInfo(); // calling methods in child class
 
    }
       
}
