package accessSpecifiersWithoutPackage;
public class Student{
    //Access Specifiers
	
    public String studentId;  //Property Declaration
    private String studentName;
    protected String dateOfBirth;
    public Student(String studId,String studName,String studdob) {  //Parameterised Constructor
        
    	studentId = studId;
        studentName = studName;
        dateOfBirth = studdob;
        
    }
    
    public void printInfo() {  // Methods Declaration
    	
        System.out.println("StudentName: " + studentName);  //private studentName not accessible outside the class
        StudentDept studdept = new StudentDept("Electronics And Communication");  //creating object for inner class
        System.out.println("StudentDept: " + studdept.studentDept);  //prints studentDept
        
    }
    
    class StudentDept{  //Inner class
    	
        protected String studentDept;
        /*protected studDept is accessible only in their same packsges and inheritance*/
        public StudentDept(String studDept) {
            studentDept = studDept;
        }
        
        public void display() {
            System.out.println("StudDept: " + studentDept);
        }
        
    }
    
    public static void main(String args[]) {
    	
        Student stud = new Student("19ECT01","KarthickRaja","29-09-2000");  //Passing constructor arguments
        stud.printInfo();  /*displays private properties using methods*/
        System.out.println("StudentId: " + stud.studentId);  //print studentId: "19ECT01" 
        System.out.println("Studentdob: " + stud.dateOfBirth);  //print studentdob: "29-09-2000"
        
    }
    
}
    
        
    