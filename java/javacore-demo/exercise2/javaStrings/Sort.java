/*42. sort and print following String[] alphabetically ignoring case. Also convert and print even indexed Strings into uppercase

       { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" }*/


/* WBS

Requirement:
    Sort and print following array alphabetically ignoring case.
    Also convert and print even indexed Strings into uppercase.

Entity:
    -public class Sort

Function Declaration:
    -public static void main(String[] args) 

Jobs to be done:
    1).Sort the array using sort method.
    2).Sort the even indexed district in the array.
*/     

package javaStrings;
import java.util.Arrays;
class Sort {
	
    public static void main(String[] args) {
  
    	String[] districts = { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" };
        Arrays.sort( districts, 0, districts.length );
        for (String i : districts) {
            System.out.print(i + " ");
        }
        System.out.println("\nEven indexed districts:");
        for(int i = 0; i < districts.length; i++) {
            if(i % 2 == 0) {
                String district = districts[i].toUpperCase();
                System.out.print(district + " ");
                
            }
            
        }
        
    }
    
}


