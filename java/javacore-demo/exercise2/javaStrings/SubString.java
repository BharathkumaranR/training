/* 46. How long is the string returned by the following expression? What is the string?

"Was it a car or a cat I saw?".substring(9, 12) */

/* WBS

Requirement:
    How long is the string
    What is the string

Entity:
   -public classSubString

Function Declaration:
    -public static void main(String[] args)

Jobs to be done
    1).Use the substring method with start index.
    2).Use the length method to find the length of the string.
    3).Print the result.
*/

package javaStrings;
public class SubString {

	public static void main(String[] args) {
    
		String str = "Was it a car or a cat I saw?";
        System.out.println(str);
        System.out.println("String length: " + str.length());
        String substr = str.substring(9, 12);
        System.out.println(substr);
        System.out.println("String length: " + substr.length());
        
    }
	
}

