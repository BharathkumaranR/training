/*
47. In the following program, called ComputeResult, what is the value of result after each numbered line executes?

Word Breakdown Structure

Requirement:
    Result of the program.

Entity:
    -public class ComputeResult

Function Declaration:
     None

Jobs to be done:
    1)Type StringBuilder is declared and is used to modify string. 
         i).setCharAt method is used to set a character value that changes the actual one.
        ii).insert method can be used to set a character or substring at a particular location/index.
       iii).append method is used to add string at the end.
    2)Print the result.     
*/

package javaStrings;
public class ComputeResult {
	
    public static void main(String[] args) {
    	
        String original = "software";
        StringBuilder result = new StringBuilder("hi");
        int index = original.indexOf('a');

/*1*/   result.setCharAt(0, original.charAt(0)); // si
/*2*/   result.setCharAt(1, original.charAt(original.length()-1)); // se
/*3*/   result.insert(1, original.charAt(4)); // swe
/*4*/   result.append(original.substring(1,4)); // sweoft
/*5*/   result.insert(3, (original.substring(index, index + 2) + " ")); // swear oft

        System.out.println(result);
        
    }
    
}