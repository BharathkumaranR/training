/*
45. Consider the following string:
    String hannah = "Did Hannah see bees? Hannah did.";
    - What is the value displayed by the expression hannah.length()?
    - What is the value returned by the method call hannah.charAt(12)?
    - Write an expression that refers to the letter b in the string referred to by hannah.
    
Word Breakdown Structure:

Requirement:
    What is the value displayed by the expression hannah.length()
    What is the value returned by the method call hannah.charAt(12)
    Write an expression that refers to the letter b in the string referred to by hannah

Entity:
    -Hannah

Function Declaration:
    None

Jobs to be done:

    1).Create a static variable hannah that can hold the string.
    2).i).Use the length method to print the length of the hannah.
      ii).Use the charAt method of string to get the particular character.
     iii).Loop the character in a string to get the string with the character 'b'.
*/

package javaStrings;
class Hannah {
    public static String hannah;
    
    public static void main(String[] args) {
    
    	hannah = "Did Hannah see bees? Hannah did.";
        System.out.println(hannah.length());
        System.out.println(hannah.charAt(12));
        String[] words = hannah.split(" ");
        for(String word : words) {
            for(int i = 0; i < word.length(); i++) {
                if(word.charAt(i) == 'b') {
                    System.out.println(word);
                    
                }
                
            }
            
        }
        
    }
    
}
