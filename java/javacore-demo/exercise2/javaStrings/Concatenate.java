/*
48. Show two ways to concatenate the following two strings together to get the string "Hi, mom.":

    String hi = "Hi, ";
    String mom = "mom.";*/

/*Requirement:
    To concatenate the following two strings together to get the string "Hi, mom."

Entity:
    -public class Concatenate

Function Declaration:
    None

Jobs to be done:
    (i)Plus(concatenation) operator can be used to combine two strings. or
    (ii)format method in String class can be used to achieve the result.*/

package javaStrings;
class Concatenate {
  
	public static void main(String[] args) {
    
		String hi = "Hi, ";
        String mom = "mom.";
        System.out.println(hi + mom);
        System.out.println(String.format("%s%s",hi,mom));
        
    }
	
}


    
