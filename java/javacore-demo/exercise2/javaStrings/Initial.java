//49. Write a program that computes your initials from your full name and displays them.//


/* WBS
Requirement:
    Initials from your full name

Entity:
    -public class Initial

Function Declaration:
     public static void main(String[] args) 

Jobs to be done:
    1).import the scanner(in this case)
    2).Get input from user.
    3).Split the names to form an array
    4).i).Use for loop to get word by word and 
       ii)use charAt method to get only one character
    5).Display the initials.
*/
    
package javaStrings;    
import java.util.Scanner;
class Initial {
    
	public static void main(String[] args) {
       
		Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        String words[] = name.split(" ");
        for (String word : words) {
            char letter = word.charAt(0);
            char initial = Character.toUpperCase(letter);
            System.out.print(initial + " ");
            
        }
        
    }
	
}

