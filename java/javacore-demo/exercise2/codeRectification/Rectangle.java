
/*+ What's wrong with the following program? And fix it.
    public class SomethingIsWrong {
        public static void main(String[] args) {
            Rectangle myRect;
            myRect.width = 40;
            myRect.height = 50;
            System.out.println("myRect's area is " + myRect.area());
        }
    }*/

/*Word Breakdown Structure(WBS)
1.Requirments
    Fix the code.
2.Entities
    Rectangle
3.Function Declaration
    public class Rectangle()
    public int area()
4.Jobs to be done
    1).Create two instances as width and height.
    2).Declare a method named as area.
    3).i).Create a myRect object.
      ii).Invoke the area method in the myRect object with the help of two instances.
    5).Prints the returned results.
*/
    
package codeRectification;    
public class Rectangle { //Class name changed as Rectangle
    public int width; //Initialized two instances variables width and height
    public int height;
    public int area(int width,int height) { // Declared method in the given datatype with parameters
        return width*height; // returning rectangle calculated value 
    }
    
    public static void main(String[] args) {
        Rectangle myRect = new Rectangle(); // creating an myRect object 
        myRect.width = 40;
        myRect.height = 50;
        System.out.println("area of myRect is " + myRect.area(myRect.width,myRect.height)); // calling method with two arguments
        
    }
    
}
    
