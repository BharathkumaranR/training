/*Use the Java API documentation for the Box class (in the javax.swing package) to help you answer 

the following questions.
    - What static nested class does Box define?
    - What inner class does Box define?
    - What is the superclass of Box's inner class?
    - Which of Box's nested classes can you use from any class?
    - How do you create an instance of Box's Filler class?

Word Breakdown Structure(WBS)
1.Requirments
    - What static nested class does Box define?
    - What inner class does Box define?
    - What is the superclass of Box's inner class?
    - Which of Box's nested classes can you use from any class?
    - How do you create an instance of Box's Filler class?

2.Entities
    -No Entity

3.Function Declaration
    -No Function


Use the Java API documentation for the Box class (in the javax.swing package) to help you answer 
the following questions.

/1/What static nested class does Box define?
Answer: Box.Filler

/2/What inner class does Box define?
Answer: Box.AccessibleBox

/3/What is the superclass of Box's inner class?
Answer: [java.awt.]Container.AccessibleAWTContainer

/4/Which of Box's nested classes can you use from any class?
Answer: Box.Filler

/5/How do you create an instance of Box's Filler class?
Answer: new Box.Filler(minDimension, prefDimension, maxDimension)*/