/*  What Integer method would you use to convert a string expressed in base 5 into the equivalent 
int?
  For example, how would you convert the string "230" into the integer value 65? Show the code you 
would use to accomplish this task.

Word Breakdown Structure(WBS):

1.Requirments
    -What Integer method can you use to convert an int into a string that expresses the number in 
hexadecimal?

2.Entities
    -No Entity
    
3.Function Declaration
    -No Function


Answer: valueOf.

Example Code:

String base5String = "230";
int result = Integer.valueOf(base5String, 5);*/