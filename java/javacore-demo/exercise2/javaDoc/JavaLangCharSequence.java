/*What methods would a class that implements the java.lang.CharSequence interface have to implement?


Word Breakdown Structure(WBS)

1.Requirement:
    -What methods would a class that implements the java.lang.CharSequence interface have to implement

2.Entity:
    -No Entity.

3.Function Declaration:
    -No Function.

Jobs to be done:
     1.Methods would a class that implements the java.lang.CharSequence interface have to implement.


What methods would a class that implements the java.lang.CharSequence interface have to implement?

Answers : charAt,
          length,
          subSequence, and
          toString.*/