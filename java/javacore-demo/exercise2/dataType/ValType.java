/*
 Question:
 Print the type of the result value of following expressions
    - 100 / 24
    - 100.10 / 10
    - 'Z' / 2
    - 10.5 / 0.5
    - 12.4 % 5.5
    - 100 % 56

 Word Breakdown Structure(WBS):

Requirement:
    -> To write a code to print the type of the result value of following expressions
Entity:
    -> public class ValType
Function Declaration:
    -> public static void main(String[] args)
Jobs to be done:
    1.Declare a,b,c variable in integer and double datatype.
    2.Calculate the given operation
          2.1)Prints some of operations of results using getClass method to gets the class of the object 
          2.2)And getName() method to returns only the name of the class.
*/   

package dataType;
class Type {
    public void type(int i) {
        System.out.println(i + ": Type int");
    }
    public void type(double i) {
        System.out.println(i + ": Type float");
        
    }
    
}

public class ValType {
    public static void main(String[] args) {
        Type t = new Type();
        t.type(100 / 24);
        t.type(100.10 / 10);
        t.type('Z' / 2);
        t.type(10.5 / 0.5);
        t.type(12.4 % 5.5);
        t.type(100 % 56);
        
    }
    
}

