/*
 Question:
 Program to store and retrieve four boolean flags using a single int.
 Word Breakdown Structure(WBS):
1.Requirments
    ->To write a program to store and retrieve four boolean flags using a single int.
2.Entities
    -> public class BooleanFlag
3.Function Declaration
    -> public static void main(String[] args)
4.Jobs to be done
    1.Declare and initialise 5 integer variables.
    2.Collections of declare myWeaponBag weapons with initailise knife or pistol or stick.
    3.Check the myWeaponBag conataining specified knife , pistol , sword , stick and shotgun.
         3.1)Variables contains in myWeaponBag prints found specified variable.
         3.2)Not Conatins prints no specified variables.
                   
*/

package dataType;
public class BooleanFlag {
    public static void main(String[] args) {
        // define weapons
        int knife = 1;
        int pistol = 2;
        int sword = 4;
        int stick = 8;
        int shotgun = 16;
        System.out.println("Weapons defined: knife, pistol, sword, stick, shotgun");
        
        // create weapon bag
        int myWeaponBag;
        System.out.println("Initiating weapon bag...");
        
        // store weapons in bag
        System.out.println("Storing a few weapons in bag (knife, pistol, stick)...");
        myWeaponBag = knife | pistol | stick;
        
        // check for weapons
        System.out.println("Looking for weapons...");
        System.out.println((myWeaponBag & knife) > 0 ? "--> Found knife" : "NO knife");
        System.out.println((myWeaponBag & pistol) > 0 ? "--> Found pistol" : "NO pistol");
        System.out.println((myWeaponBag & sword) > 0 ? "--> Found sword" : "NO sword");
        System.out.println((myWeaponBag & stick) > 0 ? "--> Found stick" : "NO stick");
        System.out.println((myWeaponBag & shotgun) > 0 ? "--> Found shotgun" : "NO shotgun");
        
    }
    
}