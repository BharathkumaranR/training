/*
 Question:
 Program to store and retrieve four boolean flags using a single int.
  
 Word Breakdown Structure(WBS):

1.Requirements
    -> Program to store and retrieve four boolean flags using a single int.
2.Entities
    -> public class BooleanInverter
3.Function Declaration
    -> public static void main(String[] args)
4.Jobs to be done
     1.Declare flag variable as boolean type and initialise as false .
     2.In the String type declare res variable and initialise as "Fail".
     3.Check if the res variable is "Pass" 
          3.1)Prints the flag value else.
          3.2)Prints the invert value of flag value.
*/   

package dataType;
public class BooleanInverter {
    public static void main(String[] args) {
        //To Invert the Value of the Boolean//
        boolean flag = true;
        String res = "Fail";
        if(res == "Pass") {
            System.out.println(flag);
        }
        
        else {
            flag = !flag;
            System.out.println(flag);
            
        }
        
    }
    
}
