/*
Question:
Find the ClassNames of the Primitive Datatype.

Word Breakdown Structure(WBS):
1.Requirement:
    -> The ClassNames of the Primitive Datatype.
    
2.Entity
    -> PrimitiveClassNames Class
    
3.Function Declaration:
    -> public static void main(String[] args)
    
4.Jobs Work Done
    1.Create int, char, double, float primitive datatype in String type getName() method.
    2.Print each primitive variables with getName() method.
*/

package dataType;
public class PrimitiveClassNames {
    
	public static void main(String[] args) {
        //ClassNames of Primitive Datatypes//
        
        String intClassName = int.class.getName();
        System.out.println("ClassName of Int : "+intClassName);
        
        String charClassName = char.class.getName();
        System.out.println("ClassName of Char : "+charClassName);
        
        String doubleClassName = double.class.getName();
        System.out.println("ClassName of double : "+doubleClassName);
        
        String floatClassName = float.class.getName();
        System.out.println("ClassName of float : "+floatClassName);
        
    }
    
}