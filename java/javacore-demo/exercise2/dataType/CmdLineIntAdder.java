/*
 Question:
 
    Create a program that reads an unspecified number of integer arguments from the command line and 
adds them together.
    For example, suppose that you enter the following: java Adder 1 3 2 10
    The program should display 16 and then exit. The program should display an error message if 
the user enters only one argument.

 Word Breakdown Structure(WBS):

Requirement:
    -> To create a program that reads an unspecified number of integer arguments from the command line and 
adds them together.
Entity:
    -> public class CmdLineIntAdder
Function Declaration:
    -> public static void main(String[] args)
Jobs to be done
    1.Initialise numArgs variable for find the length arguments.
    2.Check the number of numArgs is less than two 
        2.1)Prints statements else
        2.2)Initialise sum value and calculate the sum value 
        2.3)Using for loop of array args and doubleValue() method
    3.Printing the output.
 */

package dataType;
public class CmdLineIntAdder {
    public static void main(String[] args) {
        int numArgs = args.length;
        
        //this program requires at least two arguments on the command line
            if (numArgs < 2) {
                System.out.println("This program requires two command-line arguments.");
            } else {
        int sum = 0;
        
        for (int i = 0; i < numArgs; i++) {
            sum += Integer.valueOf(args[i]).intValue();
        }
        
            //print the sum
            System.out.println(sum);
            
        }
               
    }
    
}