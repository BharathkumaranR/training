/*
Question:
   Overloading with Wrapper types
1.Requirement:
    -> The ClassNames of the Primitive Datatype.
    
2.Entity
    -> class WrapperDemo 
    -> public class WrapperOverload
    
3.Function Declaration:
    -> public static void main(String[] args)

    
Jobs to be done
    1.Create a method which gets the Primitive type int, char and double  as the parameter and prints it.
    2.Create WrapperDemo class object declare wrapper variable.
    3.Invoke the WrapperOverload class 
        3.1)wrapIntSetter method to print integer.
        3.2)wrapcharSetter method to print character.
        3.3)wrapDoubleSetter method to print double.
*/

package dataType;
class WrapperDemo {
    
	void wrapIntSetter(int a) {
        System.out.println("int Value changed to Integer " +  a);
    }
    
    void wrapcharSetter(char b) {
        System.out.println("char Value changed to Character " + b);
    }
    
    void wrapDoubleSetter(double c) {
        System.out.println("double Value changed to Double " + c);
        
    }
    
}

public class WrapperOverload {
	
    public static void main(String[] args) {
        //Overloading Wrapper types//
        
        Integer number = 65;
        Character letter = 'S';
        Double decimal = 3.14;
        WrapperDemo wrap = new WrapperDemo();
        wrap.wrapIntSetter(number);
        wrap.wrapcharSetter(letter);
        wrap.wrapDoubleSetter(decimal);
        
    }
    
}