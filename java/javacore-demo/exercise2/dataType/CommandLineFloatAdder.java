/*
 Question:
  Create a program that is similar to the previous one but instead of reading integer arguments, 
it reads floating-point arguments.
    It displays the sum of the arguments, using exactly two digits to the right of the decimal point.
    For example, suppose that you enter the following: java FPAdder 1 1e2 3.0 4.754
    The program would display 108.75. Depending on your locale, the decimal point might be a 
comma (,) instead of a period (.). 
 Word Breakdown Structure(WBS):

1.Requirement:
    -> To  create a program that is similar to the previous one but instead of reading integer arguments, 
it reads floating-point arguments.

2.Entity:
    -> public class CmdLineFloatAdder
    
3.Function Declaration:
    -> public static void main(String[] args)
    
4.Jobs to be done
    1.Initialise numArgs variable for find the length arguments.
    2.Check the number of numArgs is less than two 
        2.1)Prints statements else
        2.2)Initialise sum value and calculate the sum value 
        2.3)Using for loop of array args and doubleValue() method
    3.Create myFormatter as DecimalFormat type
        3.1)Initialise output variable with myFormatter object format() method sum parameter 
        3.2)Printing the output.

*/

package dataType;
import java.text.DecimalFormat;

public class CommandLineFloatAdder {
    public static void main(String[] args) {

    int numArgs = args.length;
    
    //this program requires at least two arguments on the command line
           if (numArgs < 2) {
               System.out.println("This program requires two command-line arguments.");
           } else {
        double sum = 0.0;
    
        for (int i = 0; i < numArgs; i++) {
            sum += Double.valueOf(args[i]).doubleValue();
        }
    
        //format the sum
        DecimalFormat myFormatter = new DecimalFormat("###,###.##");
        String output = myFormatter.format(sum);
    
        //print the sum
            System.out.println(output);
        }
    }
}