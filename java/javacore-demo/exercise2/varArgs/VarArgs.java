/*
24. demonstrate overloading with varArgs


Word Breakdown Structure

Requirement:
    To demonstrate overloading with varArgs

Entity:
    -public class VarArgs

Function Declaration:
    -public void sort(int ... a1)
    -public void sort(boolean pass, int ... a2)

Jobs to be done:
    1).The methods takes varArgs and another method takes boolean and VarArgs as arguments.both has a
    common name 'sort'.
    2).While passing unordered values to the method sort the value get sorted and printed by the method.
    3).Another method takes boolean and values of int,
           i).if true the values get sorted 
          ii).if parameter false it prints a statement.

*/

package varArgs;
import java.util.Arrays;
class VarArgs {
    
	public void sort(int ... a1) {
    
		System.out.println("After sorting: ");
        Arrays.sort(a1, 0, a1.length);
        for(int i: a1) {
            System.out.print(i + " ");
            
        }
        
        System.out.println();
        
    }
	
    public void sort(boolean pass, int ... a2) {
        if(pass) {
            Arrays.sort(a2, 0, a2.length);
            for(int i: a2) {
                System.out.print(i + " ");
            }
        } else {
            System.out.println("No need to sort");
            
        }
        
    }
    
    public static void main(String[] args) {
    
    	VarArgs obj = new VarArgs();
        obj.sort(7,3,6,11,0,2,34);
        obj.sort(false,1,2,3,4,5);
        
    }
    
}
