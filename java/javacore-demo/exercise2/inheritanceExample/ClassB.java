package inheritanceExample;
/*
//23. Consider the following two classes://
    public class ClassA {
        public void methodOne(int i) {
        }
        public void methodTwo(int i) {
        }
        public static void methodThree(int i) {
        }
        public static void methodFour(int i) {
        }
    }

    public class ClassB extends ClassA {
        public static void methodOne(int i) {
        }
        public void methodTwo(int i) {
        }
        public void methodThree(int i) {
        }
        public static void methodFour(int i) {
        }
    }
    //Example
    class ClassA {
    public static int i = 0;
    public int j = 0;
    public void methodTwo() {
        System.out.println(j);
    }

    public static void methodFour() {
        System.out.println(i);
    }
}

class ClassB extends ClassA {
    public void methodTwo() {
        j = 20;
        System.out.println(j);
    }

    public static void methodFour() {
        i = 10;
        System.out.println(i);
    }
    public static void main(String[] args) {
        ClassB obj = new ClassB();
        obj.methodTwo();
        obj.methodFour();
        ClassA obj1 = new ClassA();
        obj1.methodTwo();
        obj1.methodFour();
    }
}

/*questions and answers
    a. Which method overrides a method in the superclass?
    b. Which method hides a method in the superclass?
    c. What do the other methods do?

a: Method2 overrides the method in superclass.
b: method4 in parent class and child class are static. While using method on child class, the
   method4 on the parent class becomes changed.
c: Method1 and method3 produces compilation error. Because the non-static methods cannot
   override the static methods.*/    