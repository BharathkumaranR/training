//22. demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake //
    
/* WBS
Requirement:
    To demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake 
class of objects

Entity:
    -Animal
    -Dog
    -Cat
    -Snake

Function Declaration:
    public void eatfood()
    public void eatfood(int kg)

Jobs to be done:
    1).Create a parent class Animal.
    2).Create a child class Dog, Cat, and Snake.
    3).Perform the methods that gives overriding and overloading.
*/  
    
package inheritanceExample;    
class Animal {
    public void eatfood() {
        System.out.println("Eating food..");
    }
    public void eatfood(int kg) {
        System.out.println(String.format("Eating more food..",kg));
        
    }
    
}

class Dog extends Animal {
    public void eatfood() {
        System.out.println("Eating pedigree..");
    }
    public void eatfood(int kg) {
        System.out.println(String.format("Eating %dkg of pedigree..",kg));
        
    }
    
}
class Cat extends Animal {
    public void eatfood() {
        System.out.println("Eating a fish..");
    }
    public void eatfood(int kg) {
        System.out.println(String.format("Eating %dkg of fish..",kg));
        
    }
    
}
class Snake extends Animal {
    public void eatfood() {
        System.out.println("Eating a frog..");
    }
    public void eatfood(int kg) {
        System.out.println(String.format("Eating %d frogs..",kg));
        
    }
    
}
public class DemonstrateInheritance {
    
	public static void main(String[] args) {
        //method overloading
        Snake cobra = new Snake();
        cobra.eatfood();
        cobra.eatfood(4);
        //method overriding
        Animal cat = new Animal();
        cat.eatfood();
        Cat ragdoll = new Cat();
        ragdoll.eatfood();
        
    }
	
}

  