/*
 Question:
 Print fibinocci using for loop, while loop and recursion

Word Breakdown Structure(WBS):
1.Requirments
    ->To print fibinocci using for loop, while loop and recursion.
2.Entities
    -> public class Fibonacci
3.Function Declaration
    -> public Fibonacci(int count, int number1,int number2)
    -> public void FibonacciFor(int count, int number1,int number2)
    -> public void FibonacciWhile(int count, int number1,int number2)
    -> public void FibonacciRecursion(int count, int number1,int number2)
    -> public static void main(String[] args)
4.Jobs to be done
   1.Create Fibonacci class object and arguments values initailised using constructor.
   2.Invoke the Fibonacci class 
       2.1)FibonacciFor() method to print fibonacci using for loop. 
       2.2)FibonacciWhile() method to print fibonacci using while loop.
       2.3)FibonacciRecursion() method to print fibonacci using recursion.
       2.4)With passing arguments values count , number1 and number2.
   3.Each methods prints the fibonacci values.
   
*/  

package controlFlow;
public class Fibonacci {
    int count;
    int number1;
    int number2;
    
    public Fibonacci(int count, int number1,int number2) {
        this.count = count;
        this.number1 = number1;
        this.number2 = number2;
    }
    public void FibonacciFor(int count, int number1,int number2) {
        for (int i = 1; i <= count; i++)
        {
            System.out.print(" " + number1 + " ");
            int number3 = number1 + number2;
            number1 = number2;
            number2 = number3;
        }
    }
    public void FibonacciWhile(int count, int number1,int number2) {
        int i = 1;
        while(i <= count)
        {
            System.out.print(" "+number1+" ");
            int number3 = number1 + number2;
            number1 = number2;
            number2 = number3;
            i++;
        }
    }
    public void FibonacciRecursion(int count, int number1,int number2) { {
        if(count > 0) {
            System.out.print(" "+ number1 +" ");
            int number3 = number1 + number2;
            number1 = number2;
            number2 = number3;
            FibonacciRecursion(count - 1,number1,number2);
        }
    }
}

    public static void main(String[] args) {
        Fibonacci fibonacci = new Fibonacci(9,0,1);
        System.out.print("Fibonacci Series using \nFor loop");
        fibonacci.FibonacciFor(9,0,1);
        System.out.print("\nWhile loop");
        fibonacci.FibonacciWhile(9,0,1);
        System.out.print("\nRecursion");
        fibonacci.FibonacciRecursion(9,0,1);
    }
}