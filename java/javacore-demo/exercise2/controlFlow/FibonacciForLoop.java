//print fibinocci using for loop, while loop and recursion

/*Work Breakdown Structure

1.Requirments
    print fibinocci using for loop
2.Entities
    Fibonacci
3.Function Declaration
    No Function
4.Jobs to be done
    (i).Create a class.
    (ii).Declare main method inside the class.
    (iii).In the main method declare three instance integer variables and initializing values.
    (iv).Print the count using for loop.
    (v).In the for loop initialize the value ,check the condition and incrementing the variable 
by the number before the current expression is evaluted.
    (vi).Print the incremented variable and adding two variable and initializing added variable to
another new variable.
    (vii).Swap three variables for looping values to prints fibinocci series.*/

package controlFlow;
public class FibonacciForLoop {

    public static void main(String[] args) {

        int count = 7, num1 = 0, num2 = 1;
        System.out.print("Fibonacci Series of " + count + " numbers: ");

        for (int i = 1; i <= count; ++i) {
            System.out.print(num1 + " ");
            int sumOfPrevTwo = num1 + num2;
            num1 = num2;
            num2 = sumOfPrevTwo;
        }
        
    }
    
}
