//Fibonacci Series using Recursion 

/*Word Breakdown Structure

1.Requirments
    To print fibinocci using recursion function
2.Entities
    Fibonacci
3.Function Declaration
    public class Fibonacci()
    static void printFibonacci()
4.Jobs to be done
    (i).Create a new class.
    (ii).Declare two variables and initializing the values.
    (iii).Declaring and assigning static method.
    (iii).In the main method declare three integer variables and initializing values.
    (iv).In the method check the condition and add  two variables and initialize the added value.
print the variable. 
    (vi).Again invoke the same method inside the method for recursion.
    (vii).Declare main method after the static method.
    (viii)In the main printing the values and invoking the method.*/

package controlFlow;
public class FibonacciRecursion {
   static int number1 = 0, number2 = 1, number3 = 0;
   static void Fibonacci(int count) {
      if (count > 0) {
         number3 = number1 + number2;
         number1 = number2;
         number2 = number3;
         System.out.print(" " + number3);
         Fibonacci(count - 1);
         
      }
      
   }
   public static void main(String[] args) {
      int count = 5;
      System.out.print(number1 + " " + number2);
      Fibonacci(count - 2);
      
   }
   
}



 