//print fibinocci using for loop, while loop and recursion

/*Word Breakdown Structure
1.Requirments
    print fibinocci using while loop
2.Entities
    Fibonacci
3.Function Declaration
    public class Fibonacci()
4.Jobs to be done
    (i).Create a new class.
    (ii).Declare main method inside the class.
    (iii).Inside the main method declare three instance integer variables and initializing values.
    (iv).Print the count and using while loop and initializing the value.
    (v).In the while loop check the condition and print the variable. 
    (vi).Add two variables and initialize added variable to another new variable.
    (vii).Swapping three variables while looping values to prints fibinocci series 
    (viii)Incrementing the variable by the number after the current expression is evaluted.*/

package controlFlow;    
public class FibonacciWhile {

    public static void main(String[] args) {

        int count = 7, num1 = 0, num2 = 1;
        System.out.print("Fibonacci Series of "+count+" numbers:");
        int i=1;
        while(i<=count)
        {
            System.out.print(num1+" ");
            int sumOfPrevTwo = num1 + num2;
            num1 = num2;
            num2 = sumOfPrevTwo;
            i++;
            
        }
        
    }
    
}
