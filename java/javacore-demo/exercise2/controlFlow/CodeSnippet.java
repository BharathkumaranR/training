/*
 Question:
 -> Consider the following code snippet.
        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string");

        -> What output do you think the code will produce if aNumber is 3?

        Write a test program containing the previous code snippet
        -> and make aNumber 3. What is the output of the program?
        -> Using only spaces and line breaks, reformat the code snippet to make the control flow easier to understand.
        -> Use braces, { and }, to further clarify the code.
 
 Word Breakdown Structure(WBS):

1.Requirments
    -> To perform a test program containing the previous code snippet.
2.Entities
    -> public class CodeSnippet
3.Function Declaration
    -> public CodeSnippet(int aNumber)
    -> public void SpacesLineBreaks(int aNumber)
    -> public void Brackets(int aNumber)
    -> public static void main(String[] args)
4.Jobs to be done
    1.Initialise aNumber variable is 3.
    2.Invoking the SpacesLineBreaks() methods and passing argument value is 3.
       2.1)To print using only spaces and line breaks.
    3.Invoking the Brackets() methods and passing argument value is 3.
       3.1)To print using braces, { and }

*/

package controlFlow;
public class CodeSnippet {
    int aNumber;
    public CodeSnippet(int aNumber) {
        this.aNumber = aNumber;
    }
    /*
     Using only spaces and line breaks, reformat the code snippet to make the control flow 
easier to understand.
    */
    public void SpacesLineBreaks(int aNumber) {
        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.print("first string");
        else 
            System.out.print("\tsecond string");
        System.out.print("\n\tthird string");
    }
    //Using braces, { and }, to further clarify the code.
    public void Brackets(int aNumber) {
        if (aNumber >= 0) {
            if (aNumber == 0) {
                System.out.println("first string");
            }
        }else { 
            System.out.println("\tsecond string");
         }
        System.out.println("\tthird string");
    }
    public static void main(String[] args) {
        CodeSnippet snippet = new CodeSnippet(3);
        System.out.println("aNumber is 3 Using Spaces Line and Breaks");
        System.out.println(" ");
        snippet.SpacesLineBreaks(3);
        System.out.println("\naNumber is 3 Using { and } brackets");
        System.out.println(" ");
        snippet.Brackets(3);
    }
}
//What output do you think the code will produce if aNumber is 3?
/*
 Output:
 
aNumber is 3 Using Spaces Line and Breaks

	second string
	third string
aNumber is 3 Using { and } brackets

	third string 
*/