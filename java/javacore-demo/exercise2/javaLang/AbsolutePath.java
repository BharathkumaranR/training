/*Print the absolute path of the .class file of the current class


Word Breakdown Structure(WBS)

1.Requirments
    -Print the absolute path of the .class file of the current class.

2.Entities
    -AbsolutePath

3.Function Declaration
    -public AbsolutePath()
    -public static void main( String[] args )

4.Jobs to be done
    1).Create class constructor and create an path object with getClass method, getClassLoader method
       to get the current path class.
    2).Declare a URL current variable initialise using path object getResource methoad with passing 
       current class file and print the current class path.
    4.In the main method create an object for an AbsolutePath class.
*/

package javaLang;
import java.net.*;

class AbsolutePath {
   public AbsolutePath() {
      ClassLoader path = this.getClass().getClassLoader();
      URL current = path.getResource("AbsolutePath.class");
      System.err.println("Url=" + current);
   }
   
   public static void main( String[] args ) {
      AbsolutePath path = new AbsolutePath();
      
   }
   
}