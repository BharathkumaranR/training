/*
 1)Question:
 Demonstrate abstract classes using Shape class.

    - Shape class should have methods to calculate area and perimeter
    - Define two more class Circle and Square by extending Shape class and implement the
      calculation for each class respectively.

Word Breakdown Structure:

Requirement:
  -> To demonstrate abstract classes using shape class.
  -> Shape class should have methods to calculate area and perimeter

Entity:
    -Shape(Abstract class)
    -Circle
    -Square

Function Declaration:
    -> Square() (Constructor)
    -> Circle() (Constructor)
    -> public abstract void calculateArea()
    -> public abstract void calculatePerimeter()
    -> public static void main(String[] args)

Jobs to be done:
    1.Create a scanner object to get input from the user.
    2.Get input to Switch between Square and Circle class 
    3.Invoke the Square and Circle class methods and  that methods declared in abstract class 
            3.1)Returns Square formula Area and Perimeter results.
            3.2)Returns Circle formula Area and Perimeter results.
    4.Prints the returned results.
*/

package abstractClass;
abstract class Shape {
    public double area;
    public double perimeter;
    public abstract void calculateArea();
    public abstract void calculatePerimeter();
}
class Circle extends Shape {
    
    public double radius;
    public final double pi = 3.141592;
    
    public Circle(double radius) {
        this.radius = radius;
    }
    public void calculateArea() {
        area = pi * radius * radius;
        System.out.println(String.format("Area of circle: %.2f", area));
    }
    public void calculatePerimeter() {
        perimeter = 2 * pi * radius;
        System.out.println(String.format("Perimeter of circle: %.2f", perimeter));
        
    }
    
}
class Square extends Shape {
    
    public double side;
    
    public Square(double side) {
        this.side = side;
    }
    public void calculateArea() {
        area = side * side;
        System.out.format("Area of square: %.2f\n", area);
    }
    public void calculatePerimeter() {
        perimeter = 4 * side;
        System.out.format("Perimeter of square: %.2f\n", perimeter);
        
    }
    
}

public class AbstractClass {
	
    public static void main(String[] args) {
        
        Shape circle = new Circle(3.21);
        circle.calculateArea();
        circle.calculatePerimeter();
        
        Shape square = new Square(5.5);
        square.calculateArea();
        square.calculatePerimeter();
        
    }
    
}