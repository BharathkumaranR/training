/*
35. Is the following interface valid?
    public interface Marker {}

Word Breakdown Structure:

Requirement:
    Is the Marker interface valid

Entity:
    None

Function Declaration:
    None

Jobs to be done:
    Creating a interface without any abstract methods.
*/

/*public interface Marker {
}
//Answer:

Yes the interface is valid, the interface without any methods called 
marking class or tagging class.
*/