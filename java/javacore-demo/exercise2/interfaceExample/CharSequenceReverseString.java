/*
36. Write a class that implements the CharSequence interface found in the java.lang package.
  Your implementation should return the string backwards. Write a small main method to test 
  your class; make sure to call all four methods.

Word Breakdown Structure(WBS)

Requirement:
    The class should return the string backwards

Entity:
    -public Test
    -public class CharSequenceReverseString

Function Declaration:

    -public char charAt(int index)
    -public int length()
    -public String subSequence(int start, int end)
    -public String toString()

Jobs to be done:
    1).Create a constructor that stores the string to an instance variable string.
    2).Invoke the following methods 
        i).charAt method gets the character at the particular index.
       ii).length method gets the length of the string.
      iii).subSequence method gets the part of a string by specifing start and end index.
       iv).toString method that overrides the default toString method in java, this reversing the
    string and storing on StringBuilder.
    3).Print the result.
*/
package interfaceExample;
class Test implements CharSequence {
    
    String string;
    
    public Test(String string) {
        this.string = string;
    }
    public char charAt(int index) {
        return string.charAt(index);
    }
    public int length() {
        return string.length();
    }
    public String subSequence(int start, int end) {
        return string.substring(start, end);
    }
    public String toString() {
        int insertIndex = 0;
        StringBuilder reversed = new StringBuilder();
        for(int index = string.length() - 1; index >= 0; index--) {
            reversed.insert(insertIndex, string.charAt(index));
            insertIndex ++;
        }
        return reversed.toString();
        
    }
    
}
public class CharSequenceReverseString {
    
	public static void main(String[] args) {
        Test obj = new Test("MANOJ");
        System.out.println(obj); // this statement equivalent to obj.toString();
        
    }
	
}