/*
26. What methods would a class that implements the java.lang.CharSequence interface have 
to implement?

Word Breakdown Structure

Requirement:
    What methods would a class that implements the java.lang.CharSequence interface have 
    to implement?

Entity:
    None

Function Declaration:
    None

Jobs to be done:
    java.lang.CharSequence interface should be implement to assess those methods
*/

//Answer
package interfaceExample;
/*1) charAt(index)
2) length()
3) subSequence(startValue, endValue)
4) toString()*/