/*
34. What is wrong with the following interface? and fix it.
    public interface SomethingIsWrong {
        void aMethod(int aValue){
            System.out.println("Hi Mom");
        }
    }

Word Breakdown System(WBS)

Requirement:
    Methods for which a class that implements the java.lang.CharSequence interface.

Entity:
    -public class InterfaceDemo

Function Declaration:
    -public static void main(String[] args)

Jobs to be done:
   1).Create an interface with a single method.
   2).Create an instance for the method.
   3).Create an object and invoke the method to perform the action
   4).Print the result.
*/

package interfaceExample;
interface Interface {
    void aMethod (int aValue);
}
class A implements Interface {
    public void aMethod (int aValue) {
        System.out.println("A value from aMethod: " + aValue);
        
    }
    
}
public class InterfaceDemo {
    
	public static void main(String[] args) {
        A obj = new A();
        obj.aMethod(5);
        
    }
	
}