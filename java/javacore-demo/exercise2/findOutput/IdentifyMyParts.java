//+ What is the output from the following code://

/*Word Breakdown Structure(WBS)
1.Requirments
    To check an output of the code.
2.Entities
    IdentifyMyParts
3.Function Declaration
    public class IdentifyMyParts()
4.Jobs to be done
    1).Two variables inside the class named as
            *class variable
            *instance variable is created
    2).Create two object and initializing values in Main method using two object with variables
    3).Invoke and print the initialized values using object and one using class name.
*/
    
package findOutput;    
public class IdentifyMyParts {
	
    public static int x = 7;
    public int y = 3;
    
    public static void main(String args[]){
       
    	IdentifyMyParts a = new IdentifyMyParts();
        IdentifyMyParts b = new IdentifyMyParts();
        a.y = 5;
        b.y = 6;
        a.x = 1;
        b.x = 2;
        System.out.println("a.y = " + a.y);
        System.out.println("b.y = " + b.y);
        System.out.println("a.x = " + a.x);
        System.out.println("b.x = " + b.x);
        System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x);
        
        }
    
}

/*Output
a.y = 5
b.y = 6
a.x = 2
b.x = 2


IdentifyMyParts.x = 2*/