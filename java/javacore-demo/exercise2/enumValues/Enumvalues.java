// + compare the enum values using equal method and == operator //


/*Word Breakdown Structure(WBS)
1.Requirments
    To compare enum value program 
2.Entities
    public class Enumvalues
3.Function Declaration
    public static void main(String[] args)
4.Jobs to be done
    1).Declare two enum initializing values inside the class as Day and Month.
    2).Declare main method inside the class.
    3).i).Print the result by comparing two enum class using == operators.
      ii).In another print statement comparing two different enum values using equals method and print 
the result.*/

package enumValues;
public class Enumvalues { 
    
	public enum Day { 
                    MON, 
                    TUE, 
                    WED, 
                    THU, 
                    FRI, 
                    SAT, 
                    SUN 
          } 
    public enum Month { 
                      JAN, 
                      FEB, 
                      MAR, 
                      APR, 
                      MAY, 
                      JUN, 
                      JULY 
          } 
    public static void main(String[] args) { 
        /*Comparing two enum members which are from different enum type 
         using == operator
        System.out.println(Month.JAN == Day.MON); //Incompatible operand types Enumvalues.Month and Enumvalues.Day*/
    
        /* Comparing two enum members which are from different enum type 
         using .equals() method */
        System.out.println(Month.JAN.equals(Day.MON)); //false
        
    } 
    
} 

