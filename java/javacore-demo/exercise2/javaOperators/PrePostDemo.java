/*+ + In the following program, explain why the value "6" is printed twice in a row:

       class PrePostDemo {
           public static void main(String[] args){
               int i = 3;
               i++;
               System.out.println(i);    // "4"
               ++i;
               System.out.println(i);    // "5"
               System.out.println(++i);  // "6"
               System.out.println(i++);  // "6"
               System.out.println(i);    // "7"
           }
       }

       }*/

/*Word Breakdown Structure(WBS)
1.Requirments
    To show why the value "6" is printed twice in a row
2.Entities
    public class PrePostDemo
3.Function Declaration
    public static void main(String[] args)
4.Jobs to be done
    1).Declare integer instance variable with initializing a value without main method
    2).Performances:
       i).Increments the number before the current expression is evaluted and prints the value.
      ii).Increments the number after the current expression is evaluted and prints the value.
     iii).Increments the number before the current expression is evaluted in the println method.
      iv).Again increments the number after the current expression is evaluted in the println method
    3).Finally prints the value.*/ 

package javaOperators;
class PrePostDemo {

	public static void main(String[] args){
     
		int i = 3;
        i++;
        System.out.println(i);    // "4"
        ++i;
        System.out.println(i);    // "5"
        System.out.println(++i);  // "6"
        System.out.println(i++);  // "6"
        System.out.println(i);    // "7"
        
    }
	
}

