/*+ Change the following program to use compound assignments:
       class ArithmeticDemo {

            public static void main (String[] args){

                 int result = 1 + 2; // result is now 3
                 System.out.println(result);

                 result = result - 1; // result is now 2
                 System.out.println(result);

                 result = result * 2; // result is now 4
                 System.out.println(result);

                 result = result / 2; // result is now 2
                 System.out.println(result);

                 result = result + 8; // result is now 10
                 result = result % 7; // result is now 3
                 System.out.println(result);
            }
       }*/
       
/*Word Breakdown Structure(WBS)
1.Requirements
    To write a program to use compound assignments
2.Entities
    public class ArithmeticDemo
3.Function Declaration
    public static void main(String[] args)
4.Jobs to be done
    1).Declare instance variables and initialize it inside the main method.
    2).Using compound assignment perform all the arithmetic operations with the integer value.
    3).Print the result after each initialization.
*/
    
package javaOperators;    
class ArithmeticDemo {
    
	public static void main (String[] args) {
        
		int result = 1;
        result += 2; // result is now 3
        System.out.println(result);
        
        result -=  1; // result is now 2
        System.out.println(result);
        
        result *= 2; // result is now 4
        System.out.println(result);
        
        result /= 2; // result is now 2
        System.out.println(result);
        
        result += 8; // result is now 10
        System.out.println(result);
        
        result %= 7; // result is now 3
        System.out.println(result);
        
     }
	
}

