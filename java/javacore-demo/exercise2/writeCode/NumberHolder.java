/*+ Given the following class, called NumberHolder,

    write some code that creates an instance of the class and initializes its two member variables with provided values,
    and then displays the value of each member variable.

    public class NumberHolder {
        public int anInt;
        public float aFloat;
    }*/

/*Word Breakdown Structure(WBS)
1.Requirments
    To write some code that creates an instance of the class and initializes its two member variables 
with provided values,and then displays the value of each member variable.
2.Entities
    public class NumberHolder
3.Function Declaration
    public static void main(String[] args)
    public void display()
4.Jobs to be done
    1).Declare the instance variables.
    2).Declare a void method with two parameters in main method.
    3).In main method create an object and initialize values ucing objects in main method.
    4).In print statement the functions are to be called with their two arguments.
*/

package writeCode;
public class NumberHolder {

	public int anInt;
    public float aFloat;
    public void display(int anInt,float aFloat) {
        System.out.println("anInt = " + anInt +" "+ "aFloat = " + aFloat);
    }
    
    public static void main(String args[]) {
    
    	NumberHolder numHold = new NumberHolder();
        numHold.anInt = 25;
        numHold.aFloat = 7.5f;
        numHold.display(numHold.anInt,numHold.aFloat);
        
    }
    
}

