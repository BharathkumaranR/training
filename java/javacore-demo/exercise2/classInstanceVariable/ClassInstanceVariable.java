/*+ Consider the following class:
    public class IdentifyMyParts {
        public static int x = 7;
        public int y = 3;
    }
    - What are the class variables?
    - What are the instance variables?



Word Breakdown Structure(WBS)
1.Requirments
    static int x = 7; //static variable is initialised
    int y = 3; //a variable is initialised
2.Entities
    IdentifyMyParts
3.Function Declaration
    public class IdentifyMyParts()
4.Jobs to be done
    (i).Create a Class name as IdentifyMyParts
    (ii).Two variables named as 
            *class variable
            *instance variable is declared inside the class.


-What are the class variables?
      Class variables also known as static variables are declared with the static keyword in a class, but outside a method, constructor or a block. 
The class variable is shared between yhe other classes. In the above code int x = 7 is the class variable because the  Static keyword is 
referred for the class variables.

-What are instance variables?
      Instance variables are declared in a class, but outside a method. When space is allocated for an object in the heap, a slot for each instance 
variable value is created. It must be referenced by more than one method, block or a constructor.In the above code int y = 3 is declared as the 
instance variable.*/