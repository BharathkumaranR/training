SELECT student.roll_number
      ,student.name
      ,student.gender
      ,student.dob
      ,student.email
      ,student.phone
      ,student.address
      ,college.name
      ,department.dept_name
      ,employee.name
  FROM university 
      ,student 
 INNER JOIN college
    ON student.college_id = college.id 
 INNER JOIN college_department 
    ON college_department.college_id = college_department.college_id
 INNER JOIN department 
    ON college_department.udept_coed = department.dept_code
 INNER JOIN employee
    ON college.id = employee.college_id
 WHERE university.university_name = "Anna University" 
   AND college.city = "Coimbatore"
 GROUP BY roll_number