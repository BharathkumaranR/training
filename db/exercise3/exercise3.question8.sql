
SELECT university.university_name
      ,department.dept_name
      ,college.name
      ,designation.rank
      ,designation.name
  FROM designation
      ,college
      ,department
      ,university 
      ,employee 
      ,college_department
 WHERE employee.name is NULL
   AND college.univ_code = university.univ_code 
   AND university.univ_code = department.univ_code 
   AND college_department.college_id = college.id
   AND college_department.udept_code = department.dept_code
   AND employee.college_id = college.id 
   AND employee.cdept_id = college_department.cdept_id 
   AND employee.desig_id = designation.ids