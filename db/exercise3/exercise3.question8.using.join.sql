SELECT univ.university_name
      ,dept.dept_name
      ,coll.name AS college_name
      ,desig.rank
      ,desig.name AS desig_name
  FROM university univ
  JOIN  college clg
    ON univ.univ_code = clg.univ_code 
  JOIN college_department clg_dept
    ON clg.id = clg_dept.college_id
  JOIN employee emp
    ON clg_dept.cdept_id = emp.cdept_id 
  JOIN designation desig
    ON emp.desig_id = desig.id 
  JOIN department dept
    ON clg_dept.udept_code = dept.dept_code
 WHERE emp.name is NULL