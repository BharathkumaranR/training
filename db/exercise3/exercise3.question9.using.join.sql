SELECT univ.university_name
      ,clg.name
      ,sem_fee.semester
      ,SUM(amount) AS 'Collected_fees' 
      ,SUM(balance_amount) AS 'Uncollected_fees' 
      ,sem_fee.paid_year
  FROM university univ
  JOIN college clg
    ON univ.univ_code = coll.univ_code
  JOIN student stud
    ON clg.id = stud.college_id 
  JOIN semester_fee sem_fee
    ON stud.id = sem_fee.stud_id
 WHERE paid_year = 2020
 GROUP BY clg.name;
 
SELECT univ.univ_code
      ,univ.university_name
      ,sum(sem_fee.amount) AS Collected_Fees
      ,sem_fee.paid_year
  FROM university univ
  JOIN college clg
    ON univ.univ_code = clg.univ_code
  JOIN student stud
    ON coll.id = stud.college_id
  JOIN semester_fee sem_fee
    ON stud.id = sem_fee.stud_id
 WHERE paid_status = 'Paid'
   AND paid_year = '2020'
 GROUP BY univ.univ_code,univ.university_name