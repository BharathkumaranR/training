UPDATE semester_fee sem_fee
   SET paid_year = (2019) 
      ,paid_status = "Paid" 
WHERE (
	SELECT roll_number
      FROM student stud
     WHERE sem_fee.stud.id = stud.id
) 
IN ("19T12" , "19T13", "19T02", "19T03" , "19T04"  );