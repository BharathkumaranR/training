  ALTER TABLE `university management`.`semester_fee` 
CHANGE COLUMN `amount` `amount` DOUBLE NULL DEFAULT 35000 ,
CHANGE COLUMN `paid_year` `paid_year` YEAR NULL ,
CHANGE COLUMN `paid_status` `paid_status` VARCHAR(10) NULL DEFAULT 'UNPAID' ;