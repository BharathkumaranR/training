 SELECT university.`university_name`
	   ,college.`name`
       ,semester_fee.`semester`
       ,SUM(amount) AS 'collected_fees' 
       ,semester_fee.paid_year
   FROM semester_fee
       ,university
       ,student
       ,college
  WHERE semester_fee.stud_id = student.id
    AND student.college_id = college.id
    AND college.univ_code = university.univ_code
    AND university_name = 'Alagappa University'
	AND paid_year = '2020'
    AND semester = '2'
    AND paid_status = 'PAID';
 
SELECT university.`university_name`
      ,college.`name`
      ,semester_fee.`semester`
      ,SUM(balance_amount) AS 'uncollected_fees' 
  FROM semester_fee
      ,university
      ,student
      ,college
 WHERE semester_fee.stud_id = student.id
   AND student.college_id = college.id
   AND college.univ_code = university.univ_code
   AND university_name = 'Anna University'
   AND semester = '1';

SELECT university.`university_name`
      ,SUM(AMOUNT) AS 'collected_fees' 
      ,semester_fee.PAID_YEAR
  FROM semester_fee
      ,university
      ,student
      ,college
WHERE semester_fee.stud_id  = student.id
  AND student.college_id = college.id
  AND college.univ_code = university.univ_code
  AND university_name = 'Anna University'
  AND paid_status = 'PAID'
  AND paid_year = '2020'