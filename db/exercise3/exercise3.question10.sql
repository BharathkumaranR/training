   USE `university management`;
SELECT student.`id`
	  ,student.`roll_number`
      ,student.`name`
      ,student.`gender`
      ,college.`code`
      ,college.`name`
      ,semester_result.`grade`
      ,semester_result.`credits`
  FROM university
      ,college
      ,student
      ,semester_result
 WHERE university.`univ_code` = college.`univ_code`
   AND student.`college_id` = college.`id`
   AND semester_result.`stud_id` = student.`id`
   AND semester_result.`gpa `>8;
   
SELECT student.`id`
	  ,student.`roll_number`
      ,student.`name`
      ,student.`gender`
      ,college.`code`
      ,college.`name`
      ,semester_result.`grade`
      ,semester_result.`credits`
  FROM university
      ,college
      ,student
      ,semester_result
 WHERE university.`univ_code` = college.`univ_code`
   AND student.`college_id` = college.`id`
   AND semester_result.`stud_id` = student.`id`
   AND semester_result.`gpa`>5
 ORDER BY `gpa`;   
   