SELECT stud.id
      ,stud.roll_number
      ,stud.name AS student_name
      ,stud.gender
      ,clg.code
      ,clg.name AS college_name
      ,sem_res.grade
      ,sem_res.gpa
      ,sem_res.semester
  FROM student stud
  JOIN college clg
    ON stud.college_id = coll.id
  JOIN university univ
    ON clg.univ_code = univ.univ_code 
  JOIN semester_result sem_res
    ON stud.id = sem_res.stud_id
 WHERE sem_res.semester = 3
   AND sem_res.gpa > 5
 UNION 
SELECT stud.id
      ,stud.roll_number
      ,stud.name AS student_name
      ,stud.gender
      ,coll.code
      ,clg.name AS college_name
      ,sem_res.grade
      ,sem_res.gpa
      ,sem_res.semester
  FROM student stud
  JOIN college clg
    ON stud.college_id = coll.id
  JOIN university univ
    ON clg.univ_code = univ.univ_code 
  JOIN semester_result sem_res
    ON  stud.id = sem_res.stud_id
 WHERE  sem_res.semester = 6
   AND sem_res.gpa > 8 