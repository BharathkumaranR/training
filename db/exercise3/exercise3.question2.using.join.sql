SELECT stud.roll_number
	  ,stud.name
      ,stud.gender
      ,stud.dob
      ,stud.email
      ,stud.phone
      ,stud.address
      ,stud.academic_year AS 'batch'
      ,clg.name
      ,dept.dept_name
  FROM student stud
  JOIN college clg
    ON stud.college_id = clg.ID
  JOIN university univ
    ON clg.univ_code = univ.univ_code 
  JOIN college_department clg_dept
    ON stud.cdept_id = clg_dept.cdept_id
  JOIN department dept
    ON clg_dept.udept_code = dept.dept_code
 WHERE univ.university_name = "Anna University"
   AND col.city = "Coimbatore"
   AND stud.academic_year = '2022'