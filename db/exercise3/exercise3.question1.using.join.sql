
SELECT clg.code
      ,clg.name
      ,univ.university_name
      ,clg.city
      ,clg.state
      ,clg.year_opened
      ,dept.dept_name
      ,emp.name AS Hod_name
  FROM college clg
  JOIN university univ 
    ON clg.univ_code = univ.univ_code
  JOIN employee emp
    ON clg.ID = emp.college_id
  JOIN designation desig
    ON desig.id = emp.desig_id
  JOIN college_department clg_dept
    ON clg_dept.cdet_id = emp.cdept_id
  JOIN department dept 
    ON clg_dept.udept_code = dept.dept_code
 WHERE desig.name = 'Hod'
HAVING dept.dept_name = 'CSE' 
    OR dept.dept_name = 'IT'