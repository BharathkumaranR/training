 SELECT university.university_name
       ,student.id AS roll_number
       ,student.name AS student_name
       ,student.gender
       ,student.dob
       ,student.address
       ,college.name AS college_name
       ,department.dept_name
       ,semester_fee.amount
       ,semester_fee.paid_year
       ,semester_fee.paid_status
   FROM university 
       ,college 
       ,department  
       ,college_department 
       ,student 
       ,syllabus 
       ,semester_fee 
  WHERE college.univ_code = university.univ_code 
    AND university.univ_code = department.univ_code 
    AND college_department.college_id = college.id 
    AND college_department.udept_code = department.dept_code
    AND student.college_id = college.id
    AND student.cdept_id = college_department.cdept_id
    AND syllabus.cdept_id = college_department.cdept_id
    AND semester_fee.stud_id = student.id
    AND semester_fee.cdept_id = college_department.cdept_id
  ORDER BY roll_number ;