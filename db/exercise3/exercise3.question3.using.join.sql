
SELECT stud.roll_number
      ,stud.name
      ,stud.gender
      ,stud.dob
      ,stud.email
      ,stud.phone
      ,stud.address
      ,coll.id
      ,dept.dept_name
      ,emp.name
  FROM university univ 
       ,student stud
  JOIN college clg
    ON stud.college_id = clg.ID 
  JOIN college_department clg_dept 
    ON clg_dept.college_id = clg_dept.college_id
  JOIN department dept
    ON clg_dept.udept_code = dept.dept_code
  JOIN employee emp
    ON clg.id = emp.college_id
 WHERE univ.university_name = "Anna University" 
   AND clg.city = "Coimbatore"
 GROUP BY roll_number 