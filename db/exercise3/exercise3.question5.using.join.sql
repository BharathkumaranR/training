SELECT stud.id
      ,stud.roll_number
      ,stud.name
      ,stud.gender
      ,stud.phone AS 'Student Contact'
      ,coll.name AS 'College Name'
      ,sem_res.semester
      ,sem_res.credits
      ,sem_res.grade
      ,sem_res.gpa
 FROM student stud
 JOIN semester_result sem_res
   ON stud.id = sem_res.stud_id
 JOIN college clg
   ON stud.college_id = clg.id 
 JOIN university univ 
   ON clg.univ_code = univ.univ_code
ORDER BY clg.name
         ,sem_res.semester
LIMIT 10
OFFSET 0