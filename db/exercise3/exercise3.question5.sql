SELECT student.`id`
      ,student.`roll_number`
	  ,student.`name`
      ,student.`gender`
      ,student.`phone` AS 'Student Contact'
      ,college.`name` AS 'College Name'
      ,semester_result.`semester`
      ,semester_result.`credits`
      ,semester_result.`grade`
	  ,semester_result.`gpa`
  FROM student
      ,semester_result
      ,college
      ,university
WHERE university.`univ_code` = college.`univ_code`
  AND college.`id` = student.`college_id`
  AND student.`id` = semester_result.`stud_id`
ORDER BY college.`name`, semester_result.`semester`;