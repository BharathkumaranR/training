CREATE SCHEMA `employee management` ;
CREATE TABLE `employee management`.`employee_pf` (
             `emp_pf_id` INT NOT NULL
			,`pf_amount` INT NOT NULL
			,`emp_esi` INT NOT NULL
			,PRIMARY KEY (`emp_pf_id`));
  CREATE TABLE `employee management`.`profession_data` (
               `profession_id` INT NOT NULL
			  ,`profession_name` VARCHAR(45) NOT NULL
			  ,`emp_pf_id` INT NOT NULL
			  ,PRIMARY KEY (`profession_id`)
			  ,FOREIGN KEY (`emp_pf_id`)
				REFERENCES employee_pf(`emp_pf_id`));
   CREATE TABLE `employee management`.`employee_data` (
                `emp_id` INT NOT NULL
			   ,`emp_name` VARCHAR(45) NOT NULL
			   ,`emp_dob` VARCHAR(45) NOT NULL
			   ,`emp_joiningdate` VARCHAR(45) NOT NULL
			   ,`emp_salary` INT NOT NULL
			   ,`profession_id` INT NOT NULL
			   ,PRIMARY KEY (`emp_id`)
			   ,FOREIGN KEY (`profession_id`)
				 REFERENCES profession_data(`profession_id`));
   CREATE TABLE `employee management`.`employee_works` (
                `emp_job` INT NOT NULL
			   ,`emp_doorno` INT NOT NULL
			   ,`emp_city` VARCHAR(45) NOT NULL
			   ,PRIMARY KEY (`emp_job`));
  
ALTER TABLE `employee management`.`employee_works`
  RENAME TO `employee management`.`employee_tasks`;

DROP TABLE `employee management`.`employee_tasks`;