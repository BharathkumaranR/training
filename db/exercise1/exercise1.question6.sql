CREATE SCHEMA `employee management` ;

CREATE TABLE `employee management`.`employee_works` (
             `emp_job` INT NOT NULL
			 ,`emp_doorno` INT NOT NULL
			 ,`emp_city` VARCHAR(45) NOT NULL
			 ,PRIMARY KEY (`emp_job`));
  
ALTER TABLE `employee management`.`employee_tasks`
 ADD COLUMN `emp_streetno` INT NOT NULL AFTER `emp_doorno`;

UPDATE `employee management`.`employee_tasks`
   SET `emp_streetno` = '3'
 WHERE (`emp_job` = 'Cleaning');
UPDATE `employee management`.`employee_tasks` 
   SET `emp_streetno` = '6' 
 WHERE (`emp_job` = 'Lecture');
UPDATE `employee management`.`employee_tasks` 
   SET `emp_streetno` = '9' 
 WHERE (`emp_job` = 'Reasearch');
UPDATE `employee management`.`employee_tasks` 
   SET `emp_streetno` = '12' 
 WHERE (`emp_job` = 'Support');
UPDATE `employee management`.`employee_tasks` 
   SET `emp_streetno` = '15' 
 WHERE (`emp_job` = 'Teach');


