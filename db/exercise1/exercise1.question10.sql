CREATE SCHEMA `employee management` ;

CREATE TABLE `employee management`.`employee_data` (
             `emp_id` INT NOT NULL
			,`emp_name` VARCHAR(45) NOT NULL
			,`emp_dob` VARCHAR(45) NOT NULL
			,`emp_joiningdate` VARCHAR(45) NOT NULL
			,`emp_salary` INT NOT NULL
			,`profession_id` INT NOT NULL
			,PRIMARY KEY (`emp_id`)
			,FOREIGN KEY (`profession_id`)
			  REFERENCES profession_data(`profession_id`));

SELECT MIN(emp_salary)
  FROM `employee management`.employee_data;

SELECT MAX(emp_salary)
  FROM `employee management`.employee_data;

SELECT AVG(emp_salary)
  FROM `employee management`.employee_data;

SELECT COUNT(emp_id)
  FROM `employee management`.employee_data;

SELECT SUM(emp_salary)
  FROM `employee management`.employee_data;

SELECT *
  FROM `employee management`.employee_data
 ORDER BY emp_id ASC LIMIT 1;

SELECT *
  FROM `employee management`.employee_data
 ORDER BY emp_id DESC LIMIT 1;