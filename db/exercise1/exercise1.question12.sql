CREATE SCHEMA `employee management` ;


SELECT *
  FROM `employee management`.profession_data
 CROSS JOIN  `employee management`.employee_data;

SELECT *
  FROM `employee management`.profession_data
INNER JOIN  `employee management`.employee_data
   ON profession_data.profession_id = employee_data.emp_id;


SELECT *
  FROM `employee management`.profession_data
LEFT JOIN  `employee management`.employee_data
    ON profession_data.profession_id = employee_data.emp_id;

SELECT *
  FROM `employee management`.profession_data
RIGHT JOIN  `employee management`.employee_data
    ON profession_data.profession_id = employee_data.emp_id;