
CREATE SCHEMA `employee management` ;

CREATE TABLE `employee management`.`employee_works` (
             `emp_job` INT NOT NULL
			,`emp_doorno` INT NOT NULL
			,`emp_city` VARCHAR(45) NOT NULL
			,PRIMARY KEY (`emp_job`));
  
  
ALTER TABLE `employee management`.`employee_tasks`
 ADD COLUMN `emp_streetno` INT NOT NULL AFTER `emp_doorno`;

  ALTER TABLE `employee management`.`employee_works` 
CHANGE COLUMN `emp_job` `emp_job` VARCHAR(45) NOT NULL ;

ALTER TABLE `employee management`.`employee_tasks` 
DROP COLUMN `emp_streetno`;