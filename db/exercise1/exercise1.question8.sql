CREATE SCHEMA `employee management` ;


CREATE TABLE `employee management`.`employee_data` (
             `emp_id` INT NOT NULL
			,`emp_name` VARCHAR(45) NOT NULL
			,`emp_dob` VARCHAR(45) NOT NULL
			,`emp_joiningdate` VARCHAR(45) NOT NULL
			,`emp_salary` INT NOT NULL
			,`profession_id` INT NOT NULL
			,PRIMARY KEY (`emp_id`)
			,FOREIGN KEY (`profession_id`)
			  REFERENCES profession_data(`profession_id`));

SELECT emp_name
  FROM `employee management`.employee_data;


SELECT * FROM `employee management`.employee_data;