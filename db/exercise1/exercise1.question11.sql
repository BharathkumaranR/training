CREATE SCHEMA `employee management` ;
 CREATE TABLE `employee management`.`profession_data` (
               `profession_id` INT NOT NULL
			  ,`profession_name` VARCHAR(45) NOT NULL
			  ,`emp_pf_id` INT NOT NULl
              ,PRIMARY KEY (`profession_id`)
              ,FOREIGN KEY (`emp_pf_id`)
                REFERENCES employee_pf(`emp_pf_id`));

CREATE TABLE `employee management`.`employee_data` (
             `emp_id` INT NOT NULL
			,`emp_name` VARCHAR(45) NOT NULL
            ,`emp_dob` VARCHAR(45) NOT NULL
			,`emp_joiningdate` VARCHAR(45) NOT NULL
			,`emp_salary` INT NOT NULL
            ,`profession_id` INT NOT NULL
            ,PRIMARY KEY (`emp_id`)
            ,FOREIGN KEY (`profession_id`)
              REFERENCES profession_data(`profession_id`));

SELECT * 
  FROM  `employee management`.profession_data
 WHERE profession_id
IN (SELECT emp_id 
	  FROM  `employee management`.employee_data
	 WHERE emp_salary= "70000") ;