CREATE SCHEMA `employee management` ;

CREATE TABLE `employee management`.`employee_works` (
             `emp_job` INT NOT NULL
			,`emp_doorno` INT NOT NULL
			,`emp_city` VARCHAR(45) NOT NULL
			,PRIMARY KEY (`emp_job`));
  
INSERT INTO `employee management`.`employee_works` (
            `emp_job`
		   ,`emp_doorno`
		   ,`emp_city`) 
VALUES (
		'Lecture'
	   ,'13'
	   ,'Coimbatore');
TRUNCATE TABLE `employee management`.`employee_works`