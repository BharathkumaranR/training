CREATE SCHEMA `employee management` ;

CREATE TABLE `employee management`.`employee_works` (
             `emp_job` INT NOT NULL
			,`emp_doorno` INT NOT NULL
			,`emp_city` VARCHAR(45) NOT NULL
			,PRIMARY KEY (`emp_job`));
  
  
 ALTER TABLE `employee management`.`employee_data` 
ALTER COLUMN emp_name SET DEFAULT 'Bharath';

   ALTER TABLE `employee management`.`employee_pf` 
ADD CONSTRAINT CHK_emp_pf CHECK (pf_amount>=4000);

ALTER TABLE `employee management`.`employee_data`
 ADD UNIQUE (emp_name);

CREATE INDEX idx_emp_streetno
ON `employee management`.employee_tasks(emp_streetno);

     ALTER TABLE `employee management`.`profession_data`
 ADD FOREIGN KEY (`emp_pf_id`)
      REFERENCES employee_pf(`emp_pf_id`);