CREATE SCHEMA `employee management` ;

CREATE TABLE `employee management`.`employee_data` (
             `emp_id` INT NOT NULL
			,`emp_name` VARCHAR(45) NOT NULL
			,`emp_dob` VARCHAR(45) NOT NULL
			,`emp_joiningdate` VARCHAR(45) NOT NULL
			,`emp_salary` INT NOT NULL
			,`profession_id` INT NOT NULL
			,PRIMARY KEY (`emp_id`)
			,FOREIGN KEY (`profession_id`)
			  REFERENCES profession_data(`profession_id`));


SELECT *
  FROM `employee management`.employee_data
 WHERE emp_name="Kumar" AND  emp_salary="70000";

SELECT * 
  FROM `employee management`.employee_data
 WHERE emp_salary="50000"  or  emp_salary="20000";

SELECT * 
  FROM `employee management`.employee_data
 WHERE NOT emp_salary="80000";

SELECT *
  FROM `employee management`.employee_data
 WHERE emp_name 
  LIKE "b%";

SELECT * 
  FROM `employee management`.employee_data
 WHERE emp_name IN ('Kishor','Prasath');

SELECT emp_name 
  FROM `employee management`.employee_data
 WHERE emp_id = ANY (SELECT emp_id FROM `employee management`.employee_data WHERE emp_salary = "50000" );

SELECT *
  FROM `employee management`.employee_data
 WHERE emp_name 
  LIKE "Gna___";