CREATE SCHEMA `employee management` ;

SELECT employee_pf.pf_amount, profession_data.profession_name, employee_data.emp_name
FROM `employee management`.employee_pf
JOIN `employee management`.profession_data ON profession_data.emp_pf_id= employee_pf.emp_pf_id
JOIN `employee management`.employee_data ON employee_data.emp_id = profession_data.profession_id;

   CREATE TABLE `employee management`.`employee_data2` (
                `emp_id` INT NOT NULl
			   ,`emp_name` VARCHAR(45) NOT NULL
               ,`emp_dob` VARCHAR(45) NOT NULL
               ,`emp_joiningdate` VARCHAR(45) NOT NULL
               ,`emp_salary` INT NOT NULL
               ,`profession_id` INT NOT NULL
               ,PRIMARY KEY (`emp_id`)
               ,FOREIGN KEY (`profession_id`)
                 REFERENCES profession_data(`profession_id`));
   
INSERT INTO `employee management`.`employee_data2` (
            `emp_id`
		   ,`emp_name`
           ,`emp_dob`
           ,`emp_joiningdate`
		   ,`emp_salary`
           ,`profession_id`) 
VALUES ('7'
       ,'Ramesh'
       ,'01-05-1999'
       ,'21-03-2020'
       ,'70000'
       ,'4');
INSERT INTO `employee management`.`employee_data2` (
            `emp_id`
            ,`emp_name`
            ,`emp_dob`
            ,`emp_joiningdate`
            ,`emp_salary`
            ,`profession_id`)
VALUES ('8'
       ,'Suresh'
       ,'05-09-1997'
       ,'20-01-2018'
       ,'50000'
       ,'5');
INSERT INTO `employee management`.`employee_data2` (
            `emp_id`
            ,`emp_name`
            ,`emp_dob`
            ,`emp_joiningdate`
            ,`emp_salary`
            ,`profession_id`)
VALUES ('9'
       ,'Ganesh'
       ,'11-12-1995'
       ,'22-02-2017'
       ,'20000'
       ,'3');
INSERT INTO `employee management`.`employee_data2` (
            `emp_id`
            ,`emp_name`
            ,`emp_dob`
            ,`emp_joiningdate`
            ,`emp_salary`
            ,`profession_id`)
VALUES ('10'
       ,'Rajesh'
       ,'17-04-1995'
       ,'25-06-2017'
       ,'80000'
       ,'2');
INSERT INTO `employee management`.`employee_data2` (
            `emp_id`
            ,`emp_name`
            ,`emp_dob`
            ,`emp_joiningdate`
            ,`emp_salary`
            ,`profession_id`)
VALUES ('11'
	   ,'Rubesh'
       ,'31-10-1994'
       ,'05-10-2016'
       ,'70000'
       ,'4');
			
SELECT 'employee_data' , emp_id, emp_name, emp_dob, emp_joiningdate, emp_salary, profession_id
  FROM  `employee management`.employee_data
 UNION
SELECT 'employee_data2' , emp_id, emp_name, emp_dob, emp_joiningdate, emp_salary, profession_id
  FROM  `employee management`.employee_data2;


SELECT emp_name 
  FROM  `employee management`.employee_data  
  UNION ALL
SELECT emp_name 
  FROM  `employee management`.employee_data2
 ORDER BY emp_name
