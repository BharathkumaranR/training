 ALTER TABLE `employee management system`.`employee_table` 
  ADD COLUMN `manager_id` INT AFTER `area`;

UPDATE `employee management system`.`employee_table` 
   SET `manager_id` = '1' 
 WHERE (`emp_id` = '1');
UPDATE `employee management system`.`employee_table` 
   SET `manager_id` = '2' 
 WHERE (`emp_id` = '2');
UPDATE `employee management system`.`employee_table` 
   SET `manager_id` = '3' 
 WHERE (`emp_id` = '3');
UPDATE `employee management system`.`employee_table` 
   SET `manager_id` = '4' 
 WHERE (`emp_id` = '4');
UPDATE `employee management system`.`employee_table` 
   SET `manager_id` = '5' 
 WHERE (`emp_id` = '5');
UPDATE `employee management system`.`employee_table` 
   SET `manager_id` = '6' 
 WHERE (`emp_id` = '6');
UPDATE `employee management system`.`employee_table` 
   SET `manager_id` = '7' 
 WHERE (`emp_id` = '7');
UPDATE `employee management system`.`employee_table` 
   SET `manager_id` = '8' 
 WHERE (`emp_id` = '8');
UPDATE `employee management system`.`employee_table` 
   SET `manager_id` = '9' 
 WHERE (`emp_id` = '9');
UPDATE `employee management system`.`employee_table` 
   SET `manager_id` = '10' 
 WHERE (`emp_id` = '10');
UPDATE `employee management system`.`employee_table` 
   SET `manager_id` = '11' 
 WHERE (`emp_id` = '11');




SELECT employee_table.emp_id AS 'Employee ID'
	  ,employee_table.first_name AS 'Employee name'
	  ,manager.emp_id AS 'Manager ID'
	  ,manager.first_name AS 'Manager name'
  FROM employee_table employee_table, employee_table manager
 WHERE employee_table.manager_id = manager.emp_id;  