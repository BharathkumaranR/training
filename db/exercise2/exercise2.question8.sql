ALTER TABLE `employee management system`.`employee_table` 
ADD COLUMN `area` VARCHAR(45) NOT NULL AFTER `dept_no`;

UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Coimbatore' 
 WHERE (`emp_id` = '1');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Namakkal' 
 WHERE (`emp_id` = '2');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Bangalore' 
 WHERE (`emp_id` = '3');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Chennai' 
 WHERE (`emp_id` = '4');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Ariyalur' 
 WHERE (`emp_id` = '5');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Coimbatore' 
 WHERE (`emp_id` = '6');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Bangalore' 
 WHERE (`emp_id` = '7');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Ariyalur' 
 WHERE (`emp_id` = '8');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Chennai' 
 WHERE (`emp_id` = '9');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Palghat' 
 WHERE (`emp_id` = '10');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Dharmapuri' 
 WHERE (`emp_id` = '11');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Ranipettai' 
 WHERE (`emp_id` = '12');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Namakkal' 
 WHERE (`emp_id` = '13');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Palghat' 
 WHERE (`emp_id` = '15');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Ramanathapuram' 
 WHERE (`emp_id` = '14');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Karaikal' 
 WHERE (`emp_id` = '16');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Coimbatore' 
 WHERE (`emp_id` = '17');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Thirunelveli' 
 WHERE (`emp_id` = '18');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Thoothukudi' 
 WHERE (`emp_id` = '19');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Kanyakumari' 
 WHERE (`emp_id` = '20');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Tanjore' 
 WHERE (`emp_id` = '21');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Trichy' 
 WHERE (`emp_id` = '22');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Coimbatore' 
 WHERE (`emp_id` = '23');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Bangalore' 
 WHERE (`emp_id` = '24');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Chennai' 
 WHERE (`emp_id` = '25');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Ariyalur' 
 WHERE (`emp_id` = '26');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Karaikudi' 
 WHERE (`emp_id` = '27');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Chengalpattu' 
 WHERE (`emp_id` = '28');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Namakkal' 
 WHERE (`emp_id` = '29');
UPDATE `employee management system`.`employee_table` 
   SET `area` = 'Sivakasi' 
 WHERE (`emp_id` = '30');

SELECT employee_table.first_name
      ,employee_table.area
  FROM employee_table 
      ,department_table 
 WHERE employee_table.dept_no = department_table.dept_no 
   AND area = 'Chennai';


SELECT employee_table.first_name
      ,employee_table.area
  FROM employee_table 
      ,department_table 
 WHERE employee_table.dept_no = department_table.dept_no and dept_name = 'HR';

SELECT area, first_name
  FROM employee_table
 ORDER BY area;


SELECT employee_table.first_name 
      ,department_table.dept_name
  FROM employee_table 
      ,department_table 
 WHERE department_table.dept_no = 102
   AND employee_table.dept_no = department_table.dept_no
   AND department_table.dept_name 
    IN
      (SELECT department_table.dept_name
         FROM employee_table
             ,department_table 
  WHERE employee_table.dept_no = department_table.dept_no
    AND department_table.dept_no = 102);
