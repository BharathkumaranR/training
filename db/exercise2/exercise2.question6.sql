CREATE SCHEMA `employee management system` ;


CREATE TABLE `employee management system`.`department_table` (
             `dept_no` INT NOT NULL
			,`dept_name` VARCHAR(45) NOT NULL
			,PRIMARY KEY (`dept_no`));

CREATE TABLE `employee management system`.`employee_table` (
             `emp_id` INT NOT NULL
			,`first_name` VARCHAR(45) NOT NULL
			,`surname` VARCHAR(45) NOT NULL
			,`dob` DATE NOT NULL
			,`date_of_joining` DATE NOT NULL
			,`annual_salary` INT NOT NULL
			,`dept_no` INT NOT NULL
		    ,PRIMARY KEY (`emp_id`)
			,CONSTRAINT FK_dept FOREIGN KEY (dept_no)
			REFERENCES department_table(dept_no));

SELECT AVG(annual_salary) As Avg_salary
  FROM employee_table
 WHERE dept_no
IN 
   (SELECT dept_no 
      FROM department_table
     WHERE employee_table.dept_no = department_table.dept_no);