SELECT *
  FROM `employee management system`.department_table;
INSERT INTO `employee management system`.`department_table` (
			`dept_no`
		   ,`dept_name`) 
VALUES (
	   '101'
	  ,'ITDesk');
INSERT INTO `employee management system`.`department_table` (
			`dept_no`
		  ,`dept_name`)
VALUES ('102'
	   ,'Finance');
INSERT INTO `employee management system`.`department_table` (
			`dept_no`
		   ,`dept_name`)
VALUES (
       '103'
	  ,'Engineering');
INSERT INTO `employee management system`.`department_table` (
			`dept_no`
		   ,`dept_name`)
VALUES (
       '104'
	  ,'HR');
INSERT INTO `employee management system`.`department_table` (
			`dept_no` 
		   ,`dept_name`)
VALUES (
       '105'
	  ,'Recruitment');
INSERT INTO `employee management system`.`department_table` (
			`dept_no`
		   ,`dept_name`) 
VALUES (
       '106'
	  ,'Facility');
SELECT * 
  FROM `employee management system`.employee_table;	   
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
		   ,`surname`
		   ,`dob`
		   ,`date_of_joining`
		   ,`annual_salary`
		   ,`dept_no`)
VALUES (
       '1'
       ,'Bharath'
       ,'Kumaran'
       ,'1980-01-01'
       ,'2010-12-30'
       ,'150000'
       ,'101');
INSERT INTO `employee management system`.`employee_table` (
            `emp_id`
           ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`)
VALUES (
       '2'
       ,'Ajith'
       ,'Kumar'
       ,'1979-02-02'
       ,'2009-11-29'
       ,'140000'
       ,'102');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
		   ,`surname`
		   ,`dob`
		   ,`date_of_joining`
		   ,`annual_salary`
		   ,`dept_no`)
VALUES (
       '3'
       ,'Arvin '
       ,'Dass'
       ,'1978-03-03'
       ,'2008-10-28'
       ,'130000'
       ,'104');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
		   ,`surname`
		   ,`dob`
		   ,`date_of_joining`
		   ,`annual_salary`
		   ,`dept_no`) 
VALUES (
       '4'
       ,'Aravinth'
       ,'Krishnan'
       ,'1977-04-04'
       ,'2007-09-27'
       ,'120000'
       ,'103');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
           ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`) 
VALUES (
       '5'
       ,'Hari'
       ,'Prasath'
       ,'1976-05-05'
       ,'2006-08-26'
       ,'110000'
       ,'106');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
		   ,`surname`
		   ,`dob`
		   ,`date_of_joining`
		   ,`annual_salary`
		   ,`dept_no`) 
VALUES (
       '6'
       ,'Arun '
       ,'Selvan'
       ,'1975-06-06'
       ,'2005-07-25'
       ,'150000'
       ,'101');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
		   ,`surname`
		   ,`dob`
		   ,`date_of_joining`
		   ,`annual_salary`
		   ,`dept_no`) 
VALUES (
       '7'
      ,'Mohammed '
      ,'Asif'
      ,'1974-07-07'
      ,'2004-06-24'
      ,'130000'
      ,'104');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
		   ,`surname`
		   ,`dob`
		   ,`date_of_joining`
		   ,`annual_salary`
		   ,`dept_no`) 
VALUES (
       '8'
      ,'Deepak'
      ,'Karthick'
      ,'1973-08-08'
      ,'2003-05-23'
      ,'110000'
	  ,'106');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`) 
VALUES (
       '9'
	  ,'Kevin'
      ,'Jones'
      ,'1972-09-09'
      ,'2002-04-22'
      ,'120000'
      ,'103');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`) 
VALUES (
       '10'
	  ,'Mesach'
      ,'Sharan'
      ,'1971-10-10'
      ,'2001-03-21'
      ,'100000'
      ,'105');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`) 
VALUES (
       '11'
	  ,'Niranjan'
      ,'Sundhar'
      ,'1970-11-11'
      ,'2000-02-20'
      ,'110000'
      ,'106');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`) 
VALUES (
       '12'
	  ,'Pradosh'
      ,'Raman'
      ,'1969-12-12'
      ,'1999-01-19'
      ,'100000'
      ,'105');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`) 
VALUES (
       '13'
	  ,'Pravin'
      ,'Bosco'
      ,'1968-01-13'
      ,'1998-12-18'
      ,'140000'
      ,'102');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`) 
VALUES (
       '14'
	  ,'Ronan'
      ,'Joshua'
      ,'1967-02-14'
      ,'1997-11-17'
	  ,'130000'
      ,'104');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`)
VALUES (
       '15'
	  ,'Nakul'
      ,'Kishore'
      ,'1966-03-15'
      ,'1996-10-16'
      ,'100000'
      ,'105');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`) 
VALUES (
       '16'
	  ,'Bala'
	  ,'Vasan'
      ,'1965-04-15'
      ,'1995-09-15'
      ,'120000'
      ,'103');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`) 
VALUES (
       '17'
	  ,'Gautham'
      ,'Gambir'
      ,'1964-05-16'
      ,'1994-08-14'
      ,'100000'
      ,'105');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`) 
VALUES (
       '18'
	  ,'Ravi'
      ,'Sasthri'
      ,'1963-06-17'
      ,'1993-07-13'
      ,'140000'
      ,'102');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`) 
VALUES (
       '19'
	  ,'Pranav'
      ,'Singh'
      ,'1962-07-18'
      ,'1992-08-12'
      ,'130000'
      ,'104');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`) 
VALUES (
       '20'
	  ,'Ishwar'
      ,'Pandey'
      ,'1961-08-19'
      ,'1991-06-11'
      ,'120000'
      ,'103');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
	       ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`) 
VALUES (
       '21'
	  ,'Yeswanth'
      ,'Yogaraj'
      ,'1960-09-20'
      ,'1990-05-10'
      ,'100000'
      ,'105');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`) 
VALUES (
       '22'
	  ,'Rithic'
      ,'Chandran'
      ,'1959-10-21'
      ,'1989-04-09'
      ,'140000'
      ,'102');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`) 
VALUES (
       '23'
	  ,'Immaneul'
      ,'Irvin'
      ,'1958-11-22'
      ,'1988-03-08'
      ,'150000'
      ,'101');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`) 
VALUES (
       '24'
	  ,'Victor'
      ,'Paul'
      ,'1957-12-23'
      ,'1987-02-07'
      ,'130000'
      ,'104');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`) 
VALUES (
	   '25'
	  ,'Harish'
      ,'Ragavendar'
      ,'1956-01-24'
      ,'1986-01-06'
      ,'120000'
      ,'103');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
           ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`) 
VALUES (
       '26'
	  ,'Inian'
      ,'Palanisamy'
      ,'1955-02-25'
      ,'1985-12-05'
      ,'110000'
      ,'106');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`) 
VALUES (
       '27'
	  ,'Ashwin'
      ,'Ravi'
      ,'1954-03-26'
      ,'1984-11-04'
      ,'150000'
      ,'101');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`)
VALUES (
       '28'
	  ,'Nisanth'
      ,'Ranton'
      ,'1953-03-27'
      ,'1983-10-03'
      ,'110000'
      ,'106');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`) 
VALUES (
       '29'
	  ,'Vishnu'
      ,'Prakash'
      ,'1952-03-28'
      ,'9182-09-02'
      ,'140000'
      ,'102');
INSERT INTO `employee management system`.`employee_table` (
			`emp_id`
		   ,`first_name`
           ,`surname`
           ,`dob`
           ,`date_of_joining`
           ,`annual_salary`
           ,`dept_no`) 
VALUES (
       '30'
	  ,'Sandeep'
      ,'Sharma'
      ,'1951-06-29'
      ,'1981-08-01'
      ,'150000'
      ,'101');